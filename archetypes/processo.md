---
title: { { replace .Name "-" " " | title } }
date: { { .Date } }
draft: false
---

![{{ replace .Name "-" " " | title }}](/govbr-ds-wiki/gestao/processos/imagens/)

## Descrição

## Processos pai

- [](/govbr-ds-wiki/gestao/processos/)

## Sub-processos

- [](/govbr-ds-wiki/gestao/processos/)

## Tarefas

- [](/govbr-ds-wiki/gestao/tarefas/)
