# GOVBR-DS - Wiki (Hugo)

Esse é um repositório de conhecimento para os projetos [GOVBR-DS](http://gov.br/ds 'GOVBR-DS').

## Objetivo

Construir repositório de conhecimento para facilitar a construção, manutenção e criação do [GOVBR-DS](http://gov.br/ds 'GOVBR-DS').

## Tecnologias

Esse projeto é desenvolvido usando:

- [Hugo](https://gohugo.io/ 'Hugo')
- [Hugo ReLearn Theme](https://mcshelby.github.io/hugo-theme-relearn/basics/customization/ 'Hugo ReLearn Theme')
- [Node.js](https://nodejs.org/ 'Node.js')

## Documentos

Todos os nossos documentos ficam na pasta *content*. Observe a documentação do [Hugo](https://gohugo.io/ 'Hugo') para aprender os detalhes de como criar novas páginas.

## Arquétipos

O [Hugo](https://gohugo.io/ 'Hugo') usa arquétipos para definir modelos que auxiliam na criação rápida de conteúdo. Eles se encontram na pasta *archetypes*.

O parâmetro que define o tipo é o `--kind nome_do_arquetipo`.

Para usar um arquétipo execute o comando:

```bash
hugo new --kind arquétipo pasta/nome_arquivo.md
```

OBS: No caso do tipo *default* não é necessário definir um arquétipo.

## Markdown

Para mais detalhes sobre como o Hugo interpreta o markdown acesse <https://learn.netlify.app/en/cont/markdown/>.

## Instalar

- Consulte a [documentação oficial](https://gohugo.io/getting-started/installing/) para entender como instalar o Hugo.

### Problema com instalação

Alguns comandos na documentação oficial estão instalando versões antigas do Hugo.

Para testar a versão instalada na sua máquina execute o comando `hugo version`.

Caso seja uma versão anterior a `0.93.2`, consulte o [repositório oficial do Hugo](https://github.com/gohugoio/hugo/releases) e instale uma versão mais nova.

## Instalando as dependências

Certifique-se de que o [Node.js](https://nodejs.org/ 'Node.js') está instalado e execute o comando `npm install` na raiz do projeto para instalar algumas dependências.

## Instalando no Windows

- Baixe a [última release](https://github.com/gohugoio/hugo/releases/) do pacote (prefira a versão com nome "extended"). Ex.: `hugo_extended_0.97.3_Windows-64bit.zip`;
- Extraia os arquivos. Ex.: `C:\Hugo\bin`;
- Adicione um novo valor a Variável de ambiente `%PATH%`:
  - Painel de Controle > Sistema e Segurança > Editar as variáveis de ambiente do sistema;
  - Avançado > Variáveis de Ambiente...;
  - Selecione a variável `PATH` e escolha **EDITAR...**;
  - Escolha **NOVO** e adicione o caminho onde extraiu os arquivos do HUGO. Ex.: `C:\Hugo\bin`;
  - Escolha **OK**, depois em **OK** e **OK** novamente.

## Rodar o ambiente local

Para testar a Wiki no ambiente local execute o comando:

```bash
hugo server -D
```

Caso o comando não seja encontrado, pode ser necessário incluir o executável no **path** do sistema operacional.

## Como contribuir?

Antes de abrir um Merge Request tenha em mente algumas informações:

- Esse é um projeto opensource e contribuições são bem-vindas.
- Para facilitar a aprovação da sua contribuição, escolha um título curto, simples e explicativo para o MR, e siga os padrões da nossa [wiki](https://gov.br/ds/wiki/ 'Wiki').
- Quer contribuir com o projeto? Confira o nosso guia [como contribuir](./CONTRIBUTING.md 'Como contribuir?').

## Precisa de ajuda?

> Por favor **não** crie issues para fazer perguntas...

Use nossos canais abaixo para obter tirar suas dúvidas:

- Site do GOVBR-DS <http://gov.br/ds>

- Usando nosso canal no discord <https://discord.gg/NkaVZERAT7>

### Commits

Nesse projeto usamos um padrão para branches e commits. Por favor observe a documentação na nossa [wiki](https://gov.br/ds/wiki/ 'Wiki') para aprender sobre os nossos padrões.

## Licença

Nesse projeto usamos a [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/ 'CC0 1.0 Universal').
