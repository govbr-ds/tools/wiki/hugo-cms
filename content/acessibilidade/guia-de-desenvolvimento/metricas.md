---
Title: Métricas
description: Fase de métricas do desenvolvimento
weight: 7
a11y: true
---

Nesta fase, sugerimos a inserção de algumas métricas de acessibilidade.

| **Link para a fase original do processo de desenvolvimento do PDG - Padrão Digital de Governo** |
| ----------------------------------------------------------------------------------------------- |
| ● [Fase original de Métricas](/gestao/processos/metricas/)                                      |

Sugestão de Métricas

```markdown
● Quantidade de bugs x Nível de Conformidade
Analisar a quantidade de melhorias abertas de acordo com os níveis de severidade (A, AA, AAA)
Indicador: por sprint ou por produto desenvolvido

● Quantidade de bugs de tipo de deficiência x componente ○ Analisar a quantidade de bugs abertos por componente e
pelo tipo de deficiência
Indicador: por sprint ou por componente desenvolvido

● Quantidade de sugestões de melhorias de acessibilidade x solicitante
Analisar as sugestões de melhoria feitas pelo solicitante. Aqui é possível verificar a área mais impactada e
entender as dores e motivações das pessoas.
Indicador: por release ou por produto desenvolvido

● Quantidade de bugs de UI x Severidade
Analisar a quantidade de bugs abertos pela severidade da issue
Indicador: por sprint
```

A severidade de um bug reflete o grau ou a intensidade de um defeito em particular, para impactar negativamente o software ou seu funcionamento.
Com base na métrica de severidade, um defeito pode ser categorizado em:

**0 – Very low** : Não afeta a usabilidade do sistema, não causa erros, pode ser algo divergente ao protótipo, erros de digitação, melhorias de UI/UX, etc.

**1 – Low**: Pode ser que afete a usabilidade, mas não impede que o sistema seja utilizado, causa pequenos erros que podem ser facilmente contornados.

**2 – Medium**: Erro que impede que alguma funcionalidade do sistema seja utilizada em um cenário específico, retorna erros ao inserir dados incomuns/inesperados ou acessar o sistema por um caminho incomum/inesperado.

**3 – High**: impede que alguma funcionalidade do sistema seja utilizada como deveria e retorna erros.

**4 – Critical**: impede o uso do sistema, de uma ou mais funcionalidades, retorna erros, afeta a segurança do sistema e deve ser corrigido imediatamente.

**[Referências](../referencias)**
