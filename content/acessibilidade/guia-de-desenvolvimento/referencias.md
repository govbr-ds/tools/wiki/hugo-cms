---
Title: Referências
description: Referências do guia de desenvolvimento acessível
weight: 9
a11y: true
---

1. <https://caroli.org/mapa-da-alteridade/>
2. <https://www.oreilly.com/library/view/strategic-writing-for/9781492049388/>
3. <https://www.w3.org/WAI/RD/2012/easy-to-read/paper11/#tronbacke2010>
4. <https://qatainarareis.medium.com/teste-de-acessibilidade-b71085facb5c>
5. <https://guia-wcag.com/>
6. <https://wave.webaim.org/>
7. <https://www.w3.org/TR/WCAG21/>
8. <https://measuringu.com/sus/>
9. <https://webaim.org/resources/designers/#infographic>
