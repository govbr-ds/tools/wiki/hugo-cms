---
Title: Descoberta
description: Fase de descoberta do desenvolvimento
weight: 2
a11y: true
---

Fase em que são investigadas as informações, oportunidades e soluções que serão valiosas para os usuários com deficiência.

| **Link para a fase original do processo de desenvolvimento do PDG - Padrão Digital de Governo** |
| ----------------------------------------------------------------------------------------------- |
| ● [Fase original de Descoberta](/gestao/processos/descoberta/)                                  |

### Definição do Problema

Para cada novo componente e/ou produto digital é necessário entender as barreiras tecnológicas de cada tipo de deficiência. Abaixo estão descritos alguns exemplos de desafios, relacionados com os critérios de sucesso dasdiretrizes internacionais de acessibilidade, WCAG 2.1 (Web Content Accessibility Guidelines) :

Pessoa com deficiência visual
| **Desafios** | **Critérios de Sucesso WCAG 2.1** |
|---------------------------------------|-----------------------------------|
| Imagens e gráficos podem não ser percebidos | 1.1.1 - Conteúdo não textual, 2.4.1 - Ignorar blocos |
| As relações visuais e a estrutura pode não ser pecebidas | 1.3.1 - Informações e relacionamentos, 2.4.1 - Ignorar blocos |  
| O conteúdo é frequentemente acessado linearmente | 1.3.2 Sequência com significado |
| Os usuários geralmente navegam de *link* em *link* ou visualizam uma lista de links | 2.4.4 Finalidade do Link(no contexto), 2.4.9 Finalidade do Link(Apenas Link) |
| Os usuários geralmente não usam um mouse | 2.1.3 Teclado (Sem Exceção), 2.1.2 Sem Bloqueio do Teclado, 2.1.1 Teclado|
| As cores não são perceptíveis | 1.4.1 Utilização de Cores |
| O conteúdo visual em multimídia pode não ser perceptível | 1.2.3 Audiodescrição ou Mídia Alternativa (Pré-gravada) |

Pessoa com deficiência auditiva
| **Desafios** | **Critérios de Sucesso WCAG 2.1** |
|---------------------------------------|-----------------------------------|
| Os usuários podem não conseguir ouvir o conteúdo de áudio | 1.2.1 Apenas Áudio ou Apenas Vídeo (Pré-gravado), 1.2.2 Legendas (Pré-gravadas), 1.2.4 Legendas (Ao Vivo), 1.2.6 Língua de Sinais (Pré-gravada), 1.2.8 Mídia Alternativa (Pré-gravada) |
| Os usuários podem ter dificuldade em filtrar ruídos | 1.4.2 Controle de Áudio, 1.4.7 Áudio de fundo baixo ou sem Áudio de fundo |

Pessoa com deficiência motora
| **Desafios** | **Critérios de Sucesso WCAG 2.1** |
|------------|--------------|
| Os usuários podem não conseguir usar o mouse | 2.1.1 Teclado, 2.1.2 Sem Bloqueio do Teclado, 2.1.3 Teclado (Sem Exceção), 2.1.4 Atalhos de teclado por caracter |
| Os usuários podem ficar cansados ao navegar por vários itens | 2.4.1 Ignorar Blocos |

Pessoa com deficiência intelectual
| **Desafios** | **Critérios de Sucesso WCAG 2.1** |
|------------|--------------|
| Os usuários podem ficar confusos com layouts complexos ou esquemas de navegação inconsistentes| 1.3.1 Informações e Relações, 1.3.2 Sequência com Significado, 1.3.3 Características Sensoriais, 1.3.4 Orientação,1.3.5 Identificar o Objetivo de Entrada, 1.3.6 Identificar o Objetivo |
| Os usuários podem ter dificuldade em se concentrar ou compreender seções extensas de texto | 1.3.1 Informações e Relações, 1.4.8 Apresentação Visual, 2.4.6 Cabeçalhos e Rótulos |
| Os usuários podem ter dificuldade para interagir com alguns tipos de conteúdo ou entrada | 1.4.1 Utilização de Cores|

➔ Exemplo:
Neste Guia, será demonstrado um exemplo de cada fase, utilizando o mesmo
componente: Vídeo. Como o Design System atual não possui este
componente verificamos a necessidade de implementá-lo, para que seja
re-usado em qualquer ambiente e seja acessível a qualquer pessoa,
independente da sua necessidade específica e contexto social.

### Mapa de Alteridade

O Mapa da Alteridade é um canvas que auxilia a identificar e propor soluções para
as barreiras de acessibilidade que os usuários enfrentam ao interagir com um
produto ou serviço. Inspirado no Mapa de Empatia, amplamente utilizado por
equipes de design, o Mapa da Alteridade se diferencia ao direcionar o foco para os
sentidos humanos dos usuários: a audição, a visão, a fala e o tato. Ao
compreendermos como as pessoas com deficiência utilizam essas habilidades,
somos capazes de aprimorar a usabilidade das soluções e proporcionar uma
experiência mais inclusiva e funcional.

Com esse canvas, é possível identificar as barreiras específicas e propor soluções
personalizadas, garantindo que todos os usuários tenham uma experiência
satisfatória.Durante processos de Imersão e Ideação, o Mapa da Alteridade ajuda a
identificar as barreiras de acessibilidade enfrentadas pelos usuários, ao mesmo
tempo em que sugere soluções inclusivas. Isso possibilita uma compreensão mais
abrangente das experiências e perspectivas das pessoas com deficiência desde as
fases iniciais do projeto.

Neste mapa são detalhados também os recursos assistivos utilizados e barreiras de
acessibilidade, como estratégia para destacar a importância da inclusão para os
stakeholders e promover uma mudança de mentalidade em relação à criação de
produtos e serviços acessíveis. Com o Mapa da Alteridade, as equipes poderão
compreender e valorizar a diversidade, promovendo soluções que integrem a
acessibilidade na experiência do usuário e promovam a inclusão. Além disso,
estarão preparadas para oferecer produtos e serviços que atendam às demandas
futuras, pois a inclusão representa uma oportunidade de inovação e crescimento no
mercado.

![Alteridade 1](/imagens/Austeridade11.png)

![Alteridade 2](/imagens/Austeridade22.png)

➔ Exemplo:
Mapa de Alteridade para usuário com deficiência visual, que irá utilizar o
componente de Vídeo.

![Alteridade 3](/imagens/Austeridade33.png)

Este Canva de exemplo está disponível para [Download](https://www.canva.com/design/DAFo1hYCGCM/r18riuzGLh2bpVQCZUdOcQ/edit)

### Personas

As personas são personagens fictícios que ajudam a compreender as necessidades
e desejos de um grupo específico de usuários. Para criar personas inclusivas e
entender os problemas de acessibilidade relacionados à usabilidade de produtos e
serviços digitais, sugerimos algumas etapas:

```markdown
● Identificar as barreiras de acessibilidade existentes: pesquisar as diretrizes de acessibilidade para soluções digitais, como as Diretrizes de Acessibilidade para Conteúdo Web (WCAG), e identificar as barreiras que os usuários com deficiência podem enfrentar ao utilizar o produto ou serviço, tal como descrito anteriormente.

● Reconhecer características distintas dos usuários: identificar como as características podem afetar a acessibilidade e procurar por dados comportamentais relevantes para cada grupo de usuários.

● Representar diferentes grupos de usuário: com base nas informações coletadas, criar personas que representem cada grupo de usuários, suas necessidades e desejos específicos.

● Priorizar as necessidades de acessibilidade: identificar as necessidades de acessibilidade mais importantes para cada persona, com base nas informações coletadas durante a pesquisa. Isso permitirá que as melhorias de acessibilidade mais críticas sejam priorizadas para o produto/serviço desenvolvido.

● Usar as personas como referência: utilizar as personas inclusivas como referência para projetar a experiência e certificar-se de testar as soluções criadas com usuários reais, que representem as personas criadas. Assim, é
possível entender melhor os problemas e projetar soluções mais acessíveis e inclusivas para um público mais amplo.
```

➔ Exemplo
Como exemplo para problemas gerais de acessibilidade, não só do componente de
vídeo, criamos algumas personas por tipo de deficiências:

![Persona 1](/imagens/Persona1.png)

![Persona 2](/imagens/Persona2.png)

![Persona 3](/imagens/Persona3.png)

![Persona 4](/imagens/Persona4.png)

**[Referências](../referencias)**
