---
Title: Execução
description: Fase de execução do desenvolvimento
weight: 5
a11y: true
---

Nesta fase são implementados os artefatos e abaixo serão descritas três etapas, em complemento ao que já é executado atualmente, levando em conta a priorização das diretrizes de acessibilidade.

| **Link para a fase original do processo de desenvolvimento do PDG - Padrão Digital de Governo** |
| ----------------------------------------------------------------------------------------------- |
| ● [Fase original de Execução](/gestao/processos/execucao/)                                      |

### Design

Para a avaliação evolutiva da acessibilidade, sugerimos a construção de protótipos de alta fidelidade para cada componente/produto digital que será desenvolvido. Dentre outras vantagens, este tipo de protótipo possui:

```markdown
● Resposta mais próxima da experiência proposta pela solução final, o que permite obter feedbacks mais precisos durante os testes;

● Melhor entendimento, clareza e assimilação dos elementos e informações da interface;

● Possibilidade de testes de fluxo de tarefas mais complexos;

● Oportunidade de validar componentes específicos da interface e itens de acessibilidade;

● A possibilidade de observação de novas métricas ao se executar os testes de usabilidade (como média de cliques, níveis de confiança, tempo médio, quantidade de erros, taxas de conclusão e outros).
```

Nesta etapa de Design, também sugerimos a utilização dos princípios de UX Writing e Easy to Read, para promover a leiturabilidade dos conteúdos. É considerado um texto com boa leiturabilidade aquele que exige do leitor as
habilidades de leitura de um estudante do 9º ano escolar brasileiro ou menos.
Apesar disso, todos os leitores se beneficiam, especialmente pessoas disléxicas, pessoas com deficiência visual ou com baixo grau de instrução.

O motivo para seguir estes princípios é fazer com que o conteúdo possua características desejáveis para um texto UX: **significativo, conciso, dialógico e claro**. É possível seguindo as dicas:

**Significativo**: Crie o esboço do seu conteúdo certificando-se que ele é significativo para o negócio e para o usuário, transmitindo tudo o que ambos precisam.

**Conciso**: Experimente criar diversas versões para a mesma ideia, mudando as palavras e a organização delas. A meta aqui é reduzir a quantidade de texto.

**Dialógico**: Com algumas versões do conteúdo em mãos, é hora de ler em voz alta, verificar se soa bem ou não, se soa repetitivo e se atende ao objetivo. Aqui, considere também a leitura das tecnologias assistivas, que sinalizam, por exemplo, os elementos da interface antes do conteúdo em si.
Se, por exemplo, você apresenta um link com o texto “Assine o serviço”, o leitor de tela lê “link: assine o serviço”. Portanto, é recomendável testar oconteúdo com o leitor de telas, ferramenta assistiva, utilizada por pessoas com deficiência visual.

**Claro**: Por fim, é hora de rever o objetivo do texto e o contexto do usuário.
Tente, aqui, mostrar o conteúdo a outras pessoas da equipe para coletar feedbacks do quão claro está o seu texto.

Se depois de tudo isso, o conteúdo ainda for complexo ou não for possível
sua tradução para uma linguagem mais simples, crie um conteúdo
suplementar que esclareça o conteúdo principal. Ele pode ser:

```markdown
● Uma versão em áudio.

● Uma ilustração.

● Um resumo do conteúdo, que pode ser um parágrafo único ou tópicos com os principais pontos do texto. Lembre-se de usar frases mais curtas, palavras mais simples e outras dicas para escrever conteúdo simples.
```

O checklist do UX Writing e Easy do Read podem ser encontrados no Anexo I
e o checklist de design no Anexo II.

### Desenvolvimento

O desenvolvimento deve seguir uma estrutura sólida, de acordo com as normas de acessibilidade para que o código possa ser legível por máquina.
As normas de codificação são os códigos de construção do ambiente digital.
Elas fornecem as especificações testadas e aprovadas que permitem que os sites funcionem em diferentes versões de navegadores e diferentes dispositivos.
Sugerimos também a utilização de tecnologias Web padrão, como as descritas na tabela abaixo.

| **Padrões**                                            | **Descrição**                                                                                                                                                                                                         |
| ------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| HTML (Hypertext Markup Language)                       | Linguagem para descrever a estrutura de uma página, incluindo informação semântica, para incluir ligações e formulários interactivos e para incorporar elementos multimédia, como imagens e vídeo. <www.w3.org/html/> |
| CSS (Cascading Style Sheets)                           | Linguagem para descrever os aspectos de apresentação de uma página, incluindo cor, tipo e disposição. <www.w3.org/css/>                                                                                               |
| JavaScript (officially known as, ECMAScript)           | Linguagem de scripting para proporcionar interação e conteúdo dinâmico. <www.ecma-international.org/>                                                                                                                 |
| WAI-ARIA (Accessible Rich Internet Applications Suite) | Framework para adicionar atributos a documentos Web de modo a tornar os elementos acionáveis acessíveis a pessoas que utilizam tecnologias de acessibilidade. <www.w3.org/WAI/intro/aria>                             |

Para além da codificação de acordo com as normas, é importante selecionar as tecnologias certas a utilizar. Um sinal seguro de integridade estrutural é a capacidade de funcionar em diferentes contextos e em diferentes dispositivos.

Nesta etapa também estão incluídas as tarefas de bugfix, que devem ter como prioridade o concerto dos bugs abertos, de acordo com as métricas de acessibilidade.

O checklist de desenvolvimento pode ser encontrado no Anexo III e as ferramentas que podem auxiliar esta etapa no Anexo VI.

### Testes

Para a etapa dos testes sugerimos que sejam feitos testes automáticos, com o apoio das ferramentas descritas no Anexo VI. Após o resultado dos testes automáticos e da realização do bugfix, se necessário, então são iniciadas as próximas etapas, com suas respectivas tarefas:

Testes com especialistas
Uma vez que a estratégia de testes foi criada (seção 3.3.1), podem ser definidos também, no planejamento dos testes, a análise de riscos, as métricas que serão coletadas, e feito o acompanhamento das sprints. Além do plano, as seguintes tarefas devem ser executadas nesta fase:

```markdown
● Projeto de Testes:
Criação dos cenários de testes
Escrita dos charters de testes

● Execução:
Execução dos testes
Submissão das solicitações de mudança, na ferramenta de gerenciamento de defeitos
Re-testes

● Análise dos Resultados:
Priorização das falhas
Estruturação das solicitações de mudanças
Entrega e apresentação dos relatórios de resultados
```

Criamos um template de diagnóstico de avaliação, com foco na acessibilidade, que pode ser encontrado no Anexo VIII.

Testes com usuários com deficiência

Para esta etapa, sugerimos as seguintes tarefas:

```markdown
● Selecionar os participantes

● Criar o roteiro

● Preparar o setup do ambiente

● Acompanhar a execução das tarefas (sem intervenções)
Neste acompanhamento é importante que o profissional esteja atento às dificuldades que o usuário está encontrando. Sugerimos algumas dicas para os testes presenciais:
Apresente-se - inclua sua descrição para pessoas com deficiência visual, no caso de pessoas com deficiência auditiva convide um intérprete de LIBRAS.
Descreva o ambiente do participante: cadeiras, mesas, posição da câmera etc
Avise quando alguém entra ou sai da sala, pois os sons gerados podem desconcentrar a pessoa e atrapalhar o
andamento dos testes.
Linguagem corporal, como gestos ou sorrisos simpáticos, que estamos tão acostumados a usar, não funcionam. É necessário aprender a transmitir tudo verbalmente.
Explique todo o processo e sempre fale o que está ocorrendo e o que está fazendo.

● Fazer uma entrevista final Após a realização dos testes, sugerimos a aplicação de um questionário, utilizando a escala SUS (System Usability Scale), adaptada à acessibilidade, com a adição de algumas perguntas finais, que podem ser encontradas no Anexo V. Os critérios que o SUS ajuda a avaliar a:
Efetividade (os usuários conseguem completar seus objetivos?)
Eficiência (quanto esforço e recursos são necessários para isso?)
Satisfação (a experiência foi satisfatória?)
```

**[Referências](../referencias)**
