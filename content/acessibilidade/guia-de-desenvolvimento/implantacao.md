---
Title: Implantação
description: Fase de implantação do desenvolvimento
weight: 6
a11y: true
---

Nesta fase, as tarefas relativas à acessibilidade são os re-testes com pessoas com
deficiência, que seguem a mesma estrutura da fase anterior (execução - testes com usuários com deficiência).

| **Link para a fase original do processo de desenvolvimento do PDG - Padrão Digital de Governo** |
| ----------------------------------------------------------------------------------------------- |
| ● [Fase original de Implantação](/gestao/processos/implantacao/)                                |

**[Referências](../referencias)**
