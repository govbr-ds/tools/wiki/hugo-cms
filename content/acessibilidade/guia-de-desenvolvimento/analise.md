---
Title: Análise
description: Fase de análise do desenvolvimento
weight: 3
a11y: true
---

Esta é a fase de análise da demanda e definição do objetivo dos produtos que serão
desenvolvidos. Abaixo estão destacadas as atividades relativas à acessibilidade.

| **Link para a fase original do processo de desenvolvimento do PDG - Padrão Digital de Governo** |
| ----------------------------------------------------------------------------------------------- |
| ● [Fase original de Análise](/gestao/processos/analise/)                                        |

### Especificações

No momento da especificação, é preciso levar em consideração as
funcionalidades e características do produto/componente, tendo em conta:

```markdown
● as diretrizes de acessibilidade que melhor se adequam ao cenário especificado;

● a prioridade dos requisitos.
```

### Identificação das Diretrizes de Acessibilidade

Após a especificação do componente/produto, é preciso entender quais são as
barreiras que as pessoas com deficiência podem enfrentar e definir quais
diretrizes de acessibilidade conseguem colmatar as dificuldades de acesso,
atendendo o máximo de cenários possível.
Ao listar as diretrizes, verificar a viabilidade técnica e analisar como elas podem
ser adaptadas para o desenvolvimento em questão.
No Anexo IV podem ser encontradas todas as diretrizes da WCAG 2.1, bem
como seus respectivos critérios de sucesso.

### Priorização de Requisitos

O objetivo desta tarefa é que haja uma priorização dos requisitos, de acordo
com as necessidades do Design System/Produto desenvolvido e com as
diretrizes de acessibilidade.
Um dos métodos recomendados é o MoSCoW, muito utilizado em projetos
ágeis, para priorizar as demandas e valorizar o tempo das equipes. É
representado pelas siglas MSCW, que significam: Must, Should, Could e Won’t.
As 4 tarefas do método MoSCow
Elas determinam a ordem de importância das atividades, sendo decrescente:
da mais relevante para a menos relevante:

```markdown
● Must Have (você tem que fazer);

● Should Have (você deve fazer);

● Could Have (você pode fazer);

● Won’t Have (você não vai fazer – por agora).
```

Assim, a disposição das ações será por importância em que Must Have são
as mais essenciais no momento e as Won’t Have poderão ser feitas
posteriormente.

1 – Must have (tem que fazer)

```markdown
Tarefas essenciais, que devem ser cumpridas, prioritariamente, para que um projeto ou solução seja bem sucedida. Logo, são consideradas uma obrigação para as equipes. Neste caso, a implementação das diretrizes de acessibilidade, que possuem nível de conformidade A e algumas do nível AA, devem fazer parte do escopo Must Have.
```

2 – Should have (deve fazer)

```markdown
São as tarefas importantes, mas não vitais para determinado projeto. Isso porque é possível priorizá-las em segundo momento, após cumprir as obrigatoriedades. Ao serem feitas, tais atividades irão gerar um valor significativo ao projeto. A implementação das diretrizes de acessibilidade, que possuem nível de conformidade AA e algumas do nível AAA, podem fazer parte do escopo Should Have.
```

3 – Could have (pode fazer)

```markdown
Como um acessório, tais tarefas desta seção não são necessárias para o funcionamento do projeto. Mas se houver tempo, após realização das demais, podem ser incluídas nas demandas do desenvolvimento. A implementação das diretrizes de acessibilidade, que possuem nível de conformidade AAA, podem fazer parte do escopo Could Have.
E se, ainda houver mais itens a serem adicionados como prioritários, as tarefas daqui poderão ser retiradas sem prejudicar o resultado.
```

4 – Won’t have (não vou fazer – por agora)

```markdown
As atividades dessa categoria são aquelas que podem ser desprezadas e/ou excluídas facilmente, logo não é necessário gerar expectativa sobre a sua realização. Aqui não são inseridas diretrizes de acessibilidade, visto que os critérios de sucesso devem ser implementados para que o produto seja, realmente, inclusivo.
```

➔ Exemplo: Para o exemplo do componente de vídeo, foram listadas as diretrizes
consideradas críticas e importantes para a implementação acessível das
funcionalidades.

### Diretrizes relacionadas

Nível A (Prioridade - Crítico M )

```markdown
● Os botões Play, Pause e Stop e o controle do volume devem ter ser funcionais e ter uma descrição textual associada: - 1.1.1 Conteúdo Não Textual - 1.4.2 Controle de Áudio - 2.2.2 Colocar em Pausa, Parar, Ocultar

● Os vídeos devem possuir legenda pré-definida e quando o usuário acessá-los, o modo Legenda deve estar ativado: - 1.2.2 Legendas (Pré-gravadas)

● Os vídeos devem possuir Audiodescrição pré-definida ou deve haver uma mídia alternativa ao vídeo, como um áudio ou uma transcrição. Deve também estar disponível um modo de ativação e desativação da audiodescrição: - 1.2.3 Audiodescrição ou Mídia Alternativa (Pré-gravada)

● Os controles do vídeo devem possuir contraste acessível: - 1.4.1 Utilização de Cores

● Os botões Play, Pause e Stop e o controle do volume devem ser acessíveis pelo teclado: - 2.1.1 Teclado

● Quando acessado pelo teclado, a ordem de navegação deve estar no mesmo direcionamento da visão (da direita para a esquerda, de cima para baixo): - 2.4.3 Ordem de Foco
```

Nivel AA (Prioridade - Importante S )

```markdown
● Para os vídeos de transmissão ao vivo, é necessário desenvolver ou utilizar um recurso de legenda online para que o usuário escolha ativá-lo ou não: - 1.2.4 Legendas (Ao Vivo)

● Os vídeos devem possuir Audiodescrição pré-definida, com o recurso de ativação e desativação da audiodescrição disponível: - 1.2.5 Audiodescrição (Pré-gravada)

● Quando acessado por dispositivos móveis, o vídeo deve ter o recurso da mudança de orientação (retrato ou paisagem), sem perder o conteúdo: - 1.3.4 Orientação

● Quando acessado pelo teclado, o foco do elemento selecionado deve estar visível: - 2.4.7 Foco Visível
```

O exemplo do quadro completo é mostrado na imagem abaixo.

![Exemplo das severidades dos erros referentes aos critérios de sucesso](/imagens/criticidade.png)

E a aplicação dos requisitos e diretrizes selecionadas neste exemplo é mostrado na próxima imagem.

![Explicações das severidades dos erros referentes aos critérios de sucesso](/imagens/criticidade2.png)

**[Referências](../referencias)**
