---
Title: Planejamento
description: Fase de planejamento do desenvolvimento
weight: 4
a11y: true
---

Nesta fase são planejados todos os detalhes das releases e sprints e no presente Guia, estão listadas as tarefas específicas de testes, com foco na qualidade e na acessibilidade.

| **Link para a fase original do processo de desenvolvimento do PDG - Padrão Digital de Governo** |
| ----------------------------------------------------------------------------------------------- |
| ● [Fase original de Planejamento](/gestao/processos/planejamento/)                              |

### Estratégia – Teste

Na estratégia de testes são selecionadas as abordagens, os tipos e as técnicas de testes que serão utilizados no projeto e devem ser indicadas quais recomendações de acessibilidade serão priorizadas. Como tarefas principais, listamos:

● Determinar os ciclos de testes e o conjunto de requisitos, que envolvem acessibilidade, selecionando o que deverá ser avaliado em cada ciclo.

● Especificar os critérios de conclusão dos ciclos de teste, informando que, depois de implementados os requisitos de acessibilidade e executados os testes, não poderá ser encontrado nenhum erro crítico, onde funcionalidades são inacessíveis pelas ferramentas assistivas.

● Estabelecer os marcos e o cronograma das atividades do projeto.

● Identificar os possíveis riscos de não planejar e implementar algumas diretrizes de acessibilidade.

### Ciclos

Cada Ciclo é definido pelo tipo de Teste, Escopo e número previsto de rodadas necessárias até que o ciclo atenda aos critérios de conclusão e êxito definidos abaixo.
Cada Ciclo poderá ser executado com objetivos diferentes, em momentos diferentes do desenvolvimento do software. Os objetivos podem ser: **validação interna, aceitação, regressão ou sanidade.**

| Nível                    | Quantidade prevista de rodadas | Escopo do teste  |
| ------------------------ | :----------------------------: | ---------------- |
| Testes de acessibilidade |              <3>               | <componente /US> |
| Teste de integração      |              <3>               | <componente /US> |
| Testes com usuários      |              <2>               | <componente /US> |
| < Teste de Sistema >     |              <1>               | <componente /US> |

Critérios

Esta seção lista os critérios de entrada, de bloqueio/suspensão e critérios de conclusão e êxito para cada um dos Ciclos definidos na seção anterior.

Critério de Entrada

Descreve como o sistema deve “estar” para que cada Ciclo de Teste possa ser iniciado, isto é, um conjunto mínimo de qualificações, como nos exemplos listados abaixo.

```markdown
● <Para iniciar o Ciclo 1, o escopo do teste deve ter sido concluído, com os critérios de sucesso da acessibilidade
definidos... >;

● <O Ciclo 2 poderá ser iniciado apenas após a conclusão da implementação de todas as diretrizes de acessibilidade do nível
A ... >;
```

Critério de Bloqueio / Suspensão

Descreve os critérios que fazem com que um ciclo de teste seja bloqueado/suspenso. Caso algum desses critérios seja atendido, o Ciclo de Teste ao qual este se refere não poderá ser executado ou concluído, como nos exemplos listados abaixo:

```markdown
● <O Ciclo 1 será bloqueado caso algum dos scripts de execução não tenha sido implementado corretamente... >;

● <A execução do Ciclo 2 deverá ser suspensa se os critérios de acessibilidade do nível A não forem atendidos corretamente... >;
```

Critério de Conclusão e Êxito

Descreve como o sistema deve “estar” para que um determinado Ciclo de Teste possa ser concluído. Descreva também, os critérios de êxito para cada Ciclo. Os seguintes critérios deverão ser atendidos para a conclusão e êxito de cada Ciclo de Teste:

```markdown
● <O Ciclo 1 será concluído após a execução de 100% dos scripts de teste, e terá êxito se 100% dos testes que contêm diretrizes de acessibilidade passarem. >;

● <O Ciclo 2 será concluído se 90% dos componentes, definidos no escopo, forem testados... >;
```

**[Referências](../referencias)**
