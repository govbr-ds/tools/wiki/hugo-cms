---
Title: 'Guia de Desenvolvimento Acessível'
pre: '<i class="fas fa-universal-access"></i>'
a11y: false
---

## Introdução

Levando em consideração o processo de desenvolvimento do PDG - Padrão Digital de Governo, este guia irá detalhar cada fase do PDG na perspectiva de acessibilidade, inserindo as etapas para a construção de produtos digitais inclusivos.<br/>Este documento seguiu uma abordagem do design inclusivo, estabelecendo a acessibilidade como prioridade e visando a sua implementação em todo o processo do desenvolvimento e levando em consideração:

```markdown
O Co-design, incorporando as perspectivas dos usuários e tendo em conta suas diversas perspectivas ao longo do processo de descoberta e melhorias;

E a Alteridade, incentivando a consideração das diferenças entre os usuários, como suas necessidades e habilidades únicas, bem como sua diversidade cultural, a fim de evitar estereótipos e preconceitos.
```

| **Links para as fases do desenvolvimento acessível inseridas no PDG - Padrão Digital de Governo** |
| ------------------------------------------------------------------------------------------------- |
| ● [descoberta](../guia-de-desenvolvimento/descoberta)                                             |
| ● [análise](../guia-de-desenvolvimento/analise)                                                   |
| ● [planejamento](../guia-de-desenvolvimento/planejamento)                                         |
| ● [execução](../guia-de-desenvolvimento/execucao)                                                 |
| ● [implantação](../guia-de-desenvolvimento/implantacao)                                           |
| ● [métricas](../guia-de-desenvolvimento/metricas)                                                 |
