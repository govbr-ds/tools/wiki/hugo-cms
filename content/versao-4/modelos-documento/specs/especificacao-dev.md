---
version: x.y.z
date: 09/01/2025
keywords: componente, anatomia, tom e voz, detalhamento, tipos, comportamentos, melhores práticas, especificação
---

# [Nome do Componente]

> Todo componente precisa começar com o prefixo "br", exemplos: br-icon, br-tag.

O componente `[Nome do Componente]` é utilizado para [breve descrição do objetivo ou funcionalidade principal do componente - deve estar de acordo com "Objetivo" da issue de pesquisa do componente].

Elemento HTML:

```html
<br-component-name></br-component-name>
```

## Anatomia

> Manter os nomes ou termos usados nas especificações de design.

| Anatomia   | Estrutura HTML         | Descrição                                                                               |
| ---------- | ---------------------- | --------------------------------------------------------------------------------------- |
| Contêiner  | Tag `host` do elemento | O contêiner é implementado por meio de CSS. No HTML, corresponde à tag raiz do elemento |
| [Elemento] | [Tag HTML]             | [Descrição funcional do elemento]                                                       |

## Propriedades

| Atributo HTML   | Propriedade JS | Restrição       | Descrição             | Tipo   | Valores Aceitos   | Padrão    |
| --------------- | -------------- | --------------- | --------------------- | ------ | ----------------- | --------- |
| `prop-required` | `propRequired` | **OBRIGATÓRIO** | [Descrição detalhada] | `tipo` | [Valores aceitos] | `[valor]` |
| `prop-optional` | `propOptional` | *OPCIONAL*      | [Descrição detalhada] | `tipo` | [Valores aceitos] | `[valor]` |

**Propriedades JS** podem ser acessadas por meio do objeto HTMLElement que representa o elemento.

### Flexibilidade na Composição do Componente

O componente permite [detalhes sobre como o componente pode ser combinado com outros elementos ou adaptado].

## Slots

[Descrição dos slots disponíveis no componente. Caso não possua, mencionar explicitamente.]

| Slot     | Descrição                         |
| -------- | --------------------------------- |
| `[nome]` | [Descrição do slot, se aplicável] |

## Estados

> Caso o componente não possua estados, especificar: "O componente não possui estado próprio."

| Estado   | Descrição                                                          |
| -------- | ------------------------------------------------------------------ |
| `[nome]` | [Descrição do estado e seu impacto no comportamento do componente] |

## Eventos

| Evento             | Descrição             | Detalhes do Payload      |
| ------------------ | --------------------- | ------------------------ |
| `[nome-do-evento]` | [Descrição do evento] | [Detalhes, se aplicável] |

## Acessibilidade

> Linkar para os critérios de sucesso para o componente.

---

## Dependências (internas e externas)

| Dependência   | Uso                |
| ------------- | ------------------ |
| [Dependência] | [Descrição do uso] |

## Breaking Changes

[Detalhamento de alterações importantes que podem afetar a compatibilidade. Caso não se aplique, mencionar explicitamente.]

---
