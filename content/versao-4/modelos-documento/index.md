---
title: 'Modelos de Documento'
weight: 3
pre: '<i class="far fa-copy"></i> '
---

Os **modelos de documentos** são estruturas pré-construídas, respeitando as orientações e regras adotadas pelo Design System que servem de base para a construção de novos documentos.

## Fase de Benchmarking (Pesquisa)

- [Benchmarking de Componente (Pesquisa)  | Formato MD](https://gitlab.com/govbr-ds/govbr-ds-gitlab-configs/-/blob/main/.gitlab/issue_templates/Benchmarking%20de%20Componente.md)
- [Critérios de Sucesso de Acessibilidade  | Formato MD](https://gitlab.com/govbr-ds/govbr-ds-gitlab-configs/-/blob/main/.gitlab/issue_templates/Crit%C3%A9rios%20WCAG.md)
- [Especificação de Desenvolvimento  | Formato MD](https://gitlab.com/govbr-ds/govbr-ds-gitlab-configs/-/blob/main/.gitlab/issue_templates/Especifica%C3%A7%C3%A3o%20de%20Componente.md)

## Diretrizes

Pacote com estrutura e arquivos para criação da diretrizes de Fundamentos, Padrões e Componentes.

- {{% download "specs/fundamento.zip" "Pacote de Fundamento  | Formato ZIP" %}}
- {{% download "specs/padrao.zip" "Pacote de Padrão  | Formato ZIP" %}}
- {{% download "specs/componente.zip" "Pacote de Componente  | Formato ZIP" %}}
