---
title: 'Versão 4'
chapter: true
weight: 0
pre: '<i class="fas fa-fire-alt"></i> '
---

Seção explicando os processos, ferramentas e fluxos relacionados à versão 4 do Design System.
