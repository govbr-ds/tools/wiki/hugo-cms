---
title: Protótipo do site
---

## Como executar o projeto localmente

```bash
git clone git@gitlab.com:govbr-ds/experimentos/prototipo-site-v4.git

npm install

git submodule init && git submodule update

npm run start
```

## Visualizar documentos de branch específica localmente

Os documentos das diretrizes ficam no submódulo `public/docs/diretrizes`.

```bash
cd public/docs/diretrizes

git checkout nome-da-branch

cd ../../..

npm run start
```

## Visualizar as diretrizes com uma branch especifica no site gitpages

Dentro do repositorio do site

1. No menu lateral esquerdo, selecione **Build** e depois **Pipelines**. Em seguida, clique no botão **Run Pipeline**. Para evitar problemas de cache, certifique-se de clicar em Clear Runner Caches.

2. Adicione a variável **DIRETRIZ_BRANCH** no campo **Input Variable Key** e insira o nome da branch no campo **Input Variable Value**. Lembre-se de que a branch deve ser criada a partir da branch "next" no repositório <https://gitlab.com/govbr-ds/govbr-ds>.

<!-- Item que podem entrar no documento: 

## Configurar o menu
## Configurar rotas
## Incluir páginas em "Introdução"

 -->
