---
title: Protótipo dos tokens
---

## Como executar o projeto

```bash
git clone git@gitlab.com:govbr-ds/experimentos/mvp-tokens.git

npm install

npm run start
```

## Atualizar o ambiente gitpages

```bash
npm run build

git push
```

## Adicionar nova página

Criar uma nova view em `src/views/` usando o modelo abaixo:

```pug
extend ../default.pug

append vars
  - title = "Título da página"
  - path = "/nome-do-arquivo.html"

block page
  p lorem ipsum 
```

Incluir no menu de navegação no arquivo `src/default.pug`:

```pug
+menu
  +menu-link("Página inicial", "/index.html")
  +menu-link("Título da página", "/nome-do-arquivo.html")
```
