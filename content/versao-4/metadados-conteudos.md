---
title: Metadados
hidden: true
---


Este documento demonstra a utilização dos metadados consumidos no site da versão 4 do *Design System*. Eles são usados na construção das rotas, abas de conteúdo e ordenação de menu dos documentos.

## Formatação dos metadados

Os metadados seguem o padrão *frontmatter* e devem ser inseridos sempre no inicio do arquivo *markdown*, segue o exemplo abaixo.

```yaml
---
title: Códigos e Utilitários de Cor
tab_title: Códigos e Utilitários de cor
tab_order: 3
path: visao-geral
type: tab_content
keywords: Fundamento;Cor;Design System; Visão Geral
---
```

## Entendendo o menu do site

O menu do site reflete a organização das diretrizes de design. Para facilitar o entendimento, serão definidos os seguintes termos:

- Tópico: É a pasta onde estão os itens de menu. Normalmente é usado como agrupador de itens e pode ter um ícone ilustrativo;
- Subtópico: são os itens de menu do tópico;
- Aba: são as abas de conteúdo de cada subtópico.

O menu do site é multinível. O primeiro nível exibe um ícone e o nome do agrupador dos tópicos. O segundo nível são os links dos subtópicos. Consulte a imagem abaixo para mais detalhes:

![menu](/imagens/menu.png)

## Como utilizar os metadados

Os metadados serão extraidos dos arquivos .md localizados no Repositório do Design System [DS-GOV](https://gitlab.com/govbr-ds/govbr-ds/-/tree/next), portanto deve-se respeitar a organização e a estrutura de
arquivos estipulados pelo reposítorio.

### Metadados para Tópicos

Para utilizar os metadados no tópicos é necessário criar um arquivo `menuMetadata.md` na raiz. Siga a estrutura de diretórios do repositório de conteúdo, conforme ilustrado abaixo.

```plaintext
- diretrizes/
  - fundamentos/
    - cor/
    - elevacao/
    - estado/
    - ...
    - menuMetadata.md
```

Após a criação deste arquivo, o seu conteúdo deve conter um metadado com as seguintes propiedades:

1. **menuGroupName** : Responsável por agrupar os subtópicos no menu, o valor inserido nessa propriedade aparecerá como título no primeiro nível do menu, representando um tópico.
2. **path** : Determina o URL da rota que será criada no site, de acordo com a hierarquia de diretórios estabelecida pelo repositório do DS-GOV.
3. **menu_title** : Define a criação de um item de segundo ou maior nivel do menu e seu nome de exibição
4. **icon** : Adiciona um ícone junto ao título da seção que agrupa os itens do menu. O valor deve ser uma classe do Font Awesome.
5. **menuPosition** : Especifica a posição da seção agrupadora dentro do menu.
6. **description** : Especifica o texto a ser exibido na página de introdução do tópico.

De modo que o metadado fique ilustrado como mostra abaixo:

 ``` yaml
---
menuGroupName: Fundamentos Visuais
path: /fundamentos
menu_title: Introdução
icon: fas fa-palette
menuPosition: 2
description: Fundamentos visuais são os elementos básicos de design desenvolvidos para o Padrão Digital de Governo. Eles estabelecem uma linguagem visual consistente e coerente para todo design system. Os fundamentos visuais abordam temas importantes como cores, ilustrações, movimentos, tipografias, espaçamentos, sistemas de grids, ícones, entre outros.Todos esses elementos são cuidadosamente projetados de forma independente e global e se adequam ao estilo visual do Design  System. Além disso, suas diretrizes foram claramente estabelecidas, garantindo que todos tenham uma compreensão compartilhada de como cada fundamento deve ser aplicado. O objetivo desta abordagem é oferecer um Design System robusto e consistente a fim de criar experiências únicas e coesas a todos os usuários.
---

```

### Metadados para Subtópicos

Em cada subtópico deve haver um arquivo `intro.md`, no qual devem ser incluídos os seguintes metadados:

1. **menu_title** : Define a criação de um item de segundo nivel do menu e seu nome de exibição
2. **menu_order** : Define a posição e o nome de exibição de um item no segundo nível do menu.
3. **breadcrumb** : Metadado opcional que define o titulo do breadcrumb que será exibido no site.
4. **path** : Determina o URL da rota que será criada no site, de acordo com a hierarquia de diretórios estabelecida pelo repositório do DS-GOV

```yaml
---
menu_title: Fundamento Cor
menu_order: 1
breadcrumb: Fundamento Cor
path: /fundamentos/cor
---
```

### Metadados para Abas

Por fim, acesse os arquivos restantes dentro do diretório dos subtopicos e insira os metadados com as seguintes propriedades:

1. **title** : Metadado utilizado nas pagínas que não  possuem o marcador de titulo para do markdown. é  necessário que esteja incluido somente nesses casos, pois caso não estejam o documento não irá passar  pela regra de lint do projeto.
2. **tab_title** : Define o nome de exibição da tab criada dentro do site
3. **tab_order** : Define a ordem de exibição da tab dentro do site
4. **default** :  Metadado define qual tab estará ativa quando a pagina for acessada no site.

Deste modo o metadado deverá ficar como é mostrado abaixo:

```yaml
---
title: Visão Geral da Cor
tab_title: Visão Geral de Cor
tab_order: 4
default: true
---
```

## Atenção

Caso as propiedades de ordenção de item do primeiro nivel do menu ( menuPosition) , item do segundo nivel do menu(menu_order) e exibição da tab(tab_order) não seja inseridos no metadado , o site irá exibir os items em ordem alfabética
