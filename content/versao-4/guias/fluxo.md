---
title: Fluxo dos componentes
description: Explicação sobre nosso fluxo de trabalho dentro dos componentes
---

```mermaid
graph TD

    subgraph Artefatos
        dsg@{ shape: docs, label: "Design"}
        a11y@{ shape: docs, label: "Acessibilidade"}
        dev@{ shape: docs, label: "Desenvolvimento"}

        dsg --> dsg2@{ shape: subproc, label: "Fluxo DSG" }
        a11y --> a11y2@{ shape: subproc, label: "Fluxo A11Y" }
        dev --> dev2@{ shape: subproc, label: "Fluxo DEV" }
    end

    comp((Componente)) -- 2 a 3 dias --> pesq[Benchmarking]
    pesq --> Artefatos
    Artefatos --> hom[Homologação]
    hom -.-> |ajustes| Artefatos
    hom --> prod((Publicação))

    click comp href "#componente"
    click pesq href "#pesquisa"
    click dsg href "#dsg"
    click a11y href "#a11y"
    click dev href "#dev"
    click hom href "#homologacao"
    click prod href "#publicacao"

```

## Componente {#componente}

O componente e os responsáveis são definidos no **planejamento da sprint**. O ideal é que cada responsável já dêem início às pesquisas sobre o ponto de vista de design, acessibilidade e desenvolvimento.

Os responsáveis deverão criar as issues:

1. Pesquisa;
1. Diretriz;
1. Critérios de sucesso;
1. Especificação;
1. Atualizar uikit.

## Benchmarking (Pesquisa) {#pesquisa}

Nesta etapa, os responsáveis irão preencher o template [Benchmarking de Componente](https://gitlab.com/govbr-ds/govbr-ds-gitlab-configs/-/blob/main/.gitlab/issue_templates/Benchmarking%20de%20Componente.md). Esse documento serve para:

1. Informar os responsáveis;
1. Categorizar;
1. Definir o objetivo;
1. Listar as decisões gerais.

## Artefatos de design {#dsg}

A *issue* **Diretriz** deve apontar para o *Merge Request* contendo os seguintes artefatos:

1. projects/diretrizes/componentes/componente/intro.md
2. projects/diretrizes/componentes/componente/visao-geral.md
3. projects/diretrizes/componentes/componente/token.md
4. projects/diretrizes/componentes/componente/saiba-mais.md (opcional)
5. projects/diretrizes/componentes/componente/src/componente.fig (arquivo de handoff)
6. projects/tokens/src/tokens.json (tokens do componente)

A *issue* **Atualizar uikit** deve apontar para o *Merge Request* da atualização do *uikit*.

## Artefatos de acessibilidade {#a11y}

A *issue* **Acessibilidade** deve usar o modelo [Critérios de Sucesso de Acessibilidade](https://gitlab.com/govbr-ds/tools/govbr-ds-wiki/-/blob/main/content/versao-4/modelos-documento/specs/criterios-sucesso.md).

Informar as especificações de aspectos e detalhes de como aplicar roles ou atributos aria.

## Artefatos de desenvolvimento {#dev}

A *issue* **especificação** deve usar o modelo [Especificação de Desenvolvimento](https://gitlab.com/govbr-ds/tools/govbr-ds-wiki/-/blob/main/content/versao-4/modelos-documento/specs/especificacao-dev.md).

Após a especificação do componente, o desenvolvedor responsável deve iniciar o [Fluxo de trabalho do Desenvolvimento](../../../desenvolvimento/web-components/fluxo/).

## Homologação

Nesta etapa, o componente deve ser validado pelos responsáveis. Planejar a publicação e divulgação.

## Publicação

É a última etapa do fluxo. O componente deve estar apto para ser usado tanto no uikit, quanto no pacote npm do projeto.

A documentação deve estar publicada nos sites de design e desenvolvimento.
