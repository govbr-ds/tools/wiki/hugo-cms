---
title: Handoff de componentes
description: Explicação sobre handoff dentro dos componentes
---

## Antes de começar

- O arquivo de *handoff* **não substitui a leitura da diretriz** e serve para melhorar a comunicação e visualização dos estilos aplicados no componente;
- Leia o [resultado do teste de Handoff](https://gitlab.com/govbr-ds/govbr-ds-backlogs/-/issues/781) e os comentários para entender motivações e vantagens no [Fluxo dos componentes](../fluxo);
- Para visualizar os tokens no *Figma* é necessário habilitar o *plugin* [**Tokens Studio for Figma**](https://www.figma.com/community/plugin/843461159747178978/tokens-studio-for-figma);
- Acesse [Design Token](https://next-ds.estaleiro.serpro.gov.br/fundamentos/design-token) no site do Design System caso tenha dúvidas sobre os tokens;
- Ao inspecionar um elemento no Figma, o token mostrado **pode pertencer a outro componente**. Isso acontece quando há reuso de componentes, portanto sempre verifique a anatomia na diretriz;
- A opção ***Deep Inspect*** (do plugin) permite visualizar tanto os tokens do objeto selecionado quanto os tokens pertencentes aos “elementos filho” desse elemento;
- Nem todas as propriedades visuais são aplicadas usando token. No figma, existe a configuração "***Auto layout***" (*flexbox*, no CSS), onde o alinhamento aplicado precisa ser conferido conforme a figura a seguir:
  ![Auto layout no Figma](figma-auto-layout.png)

## Passo a passo

Usaremos o componente [**Form-Label**](https://next-ds.estaleiro.serpro.gov.br/componentes/form-label) neste passo a passo.

1. Acesse o conteúdo da aba **Saiba Mais** na diretriz;
2. Baixe o arquivo `form-label.fig` dentro da seção **Especificação**;
3. Importe o arquivo no *Figma*;
4. No *Figma*, abra a página **Diretriz**:
   ![Diretriz do componente no Figma](figma-diretriz.jpg)
5. Abra o plugin **Tokens Studio for Figma** e selecione a aba **Inspect**. Depois disso, elecione o objeto que deseja ser inspecionado para visualizar os tokens:
   {{% notice note %}}
   Confirme se a opção "***Deep Inspect***" está ativada na aba de inspeção para visualizar todos os tokens em uso.
   {{% /notice %}}
   ![Inspecionar os tokens](plugin-inspect.jpg)
6. Recomendamos manter a **mesma estrutura** (não precisa manter os mesmos nomes) usada no *Figma* para facilitar a aplicação dos estilos em CSS, Sass, Less, etc:
   ![Elementos do componente](anatomia.jpg)
7. Para visualizar somente os tokens aplicado no objeto selecionado, desmarque a opção "***Deep Inspect***":
   ![Inspecionar individualmente](element-inspect.jpg)
