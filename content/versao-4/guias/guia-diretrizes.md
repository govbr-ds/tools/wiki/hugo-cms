---
title: Guia de Diretrizes
description: Aspectos a serem seguidos durante a criação e atualização das diretrizes de design.
date: 23/05/2024
keywords: diretriz, estrutura padrão, modelo, markdown, padrões de design.
weight: 0
---

## Roteiro de criação

- Para criar introdução:
  - (1 frase no máximo 3 linhas) Explicação do que se trata o tema e o que ele deve encontrar na documentação;
  - (1 frase no máximo 3 linhas) Como ele pode me ajudar;
- Caixa alta segue o padrão da lingua portuguesa. Basicamente, só terá letra maiúscula nome próprio (nome de elementos que foram dados pelo time). Os nomes dos fundamentos, padrões e componentes letra minuscula;
- Quando se referir a um conteúdo de uma outra aba, utilize a palavra "seção";
- Nome dos elementos, propriedade ... não precisam estar em itálico. Ex: O  Design System segue os conceitos..... Todo *Design System* deve ter....

nome-do-arquivo?nome-da-aba#nome-da-ancora

## Conteúdos especiais

Card Assunto Relacionados:

```markdown
> $link-externo / $link / $hashtag [titulo](www.google.com)
> conteúdo / descrição
```

Blockquote:

```markdown
> $nao-esqueca / $moral-da-historia / $porque-e-importante / $va-mais-longe / $assim-que-se-faz / $faca / $nao-faca  / $recomendado  / $nao-recomendado
> conteúdo / descrição
```

imagem símbolo ("sem fundo cinza"):

```markdown
![Texto para leitor de telas.](imagens/image.png "$not")
```

Imagem Horizontal:

```markdown
![Texto para leitor de telas.](imagens/image.png "$lateral descrição do title")
*Legenda da imagem.*
```

Imagem Vertical:

```markdown
![Texto para leitor de telas.](imagens/image.png "descrição do title")
*Legenda da imagem.*
```

Vídeos:

```markdown
![Texto para leitor de telas.](imagens/meu-video.mp4 '$video $lateral $controls $loop')
*Legenda da imagem.*
```

Faça e Não Faça:

```markdown
![Texto para leitor de telas.](imagens/image.png "$faca")
![Texto para leitor de telas.](imagens/image.png "$nao-faca")
*Legenda da imagem.*
```

Recomendado e Não Recomendado:

```markdown
![Texto para leitor de telas.](imagens/image.png "$recomendado")
![Texto para leitor de telas.](imagens/image.png "$nao-recomendado")
*Legenda da imagem.*
```

## Conteúdos das diretrizes

As abas das diretrizes são vinculadas aos nomes dos arquivos.

| Nome da aba           | Nome do arquivo        |
| --------------------- | ---------------------- |
| Visão Geral           | intro.md               |
| Códigos e Utilitários | codigos-utilitarios.md |
| Tokens                | tokens.md              |
| Saiba Mais            | saiba-mais.md          |
