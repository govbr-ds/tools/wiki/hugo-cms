---
title: Tokens
description: Este documento lista os design tokens utilizados no Design System, organizados por fundamentos
date: 25/10/2021
keywords: tokens, fundamentos, tipografia, iconografia, size, escala, cores, luminância, hexadecimal, espaçamento, alinhamento, layout, ajuste, superfície, borda, cantos, opacidade, overlay, elevação, movimento, especificação
---

## Tipografia

### Família

| Family  |     Family Token     |
| ------- | :------------------: |
| Rawline | `--font-family-base` |

### Font-Weight (peso)

| Font-Weight | Style       |        Weight Token         |
| ----------- | ----------- | :-------------------------: |
| 100         | Thin        |    `--font-weight-thin`     |
| 200         | Extra-Light | `--font-weight-extra-light` |
| 300         | Light       |    `--font-weight-light`    |
| 400         | Regular     |   `--font-weight-regular`   |
| 500         | Medium      |   `--font-weight-medium`    |
| 600         | Semi-Bold   |  `--font-weight-semi-bold`  |
| 700         | Bold        |    `--font-weight-bold`     |
| 800         | Extra-bold  | `--font-weight-extra-bold`  |
| 900         | Black       |    `--font-weight-black`    |

### Line-height (entrelinha)

| Line-Height (em) |     Line-Height Token      |
| ---------------- | :------------------------: |
| 1.15             |  `--font-lineheight-low`   |
| 1.45             | `--font-lineheight-medium` |
| 1.85             |  `--font-lineheight-high`  |

### Escala

| em    | px    |         Scale Token         |
| ----- | ----- | :-------------------------: |
| 3.583 | 50.16 |  `--font-size-scale-up-07`  |
| 2.986 | 41.8  |  `--font-size-scale-up-06`  |
| 2.488 | 34.84 |  `--font-size-scale-up-05`  |
| 2.074 | 29.03 |  `--font-size-scale-up-04`  |
| 1.728 | 24.19 |  `--font-size-scale-up-03`  |
| 1.44  | 20.16 |  `--font-size-scale-up-02`  |
| 1.2   | 16.8  |  `--font-size-scale-up-01`  |
| 1     | 14    |  `--font-size-scale-base`   |
| 0.833 | 11.67 | `--font-size-scale-down-01` |
| 0.694 | 9.72  | `--font-size-scale-down-02` |
| 0.579 | 8.10  | `--font-size-scale-down-03` |

---

## Iconografia

### Size

| em   | px  | Class (fontawesome) |     Size Token     |
| ---- | --- | :-----------------: | :----------------: |
| 1    | 16  |        `fas`        | `--icon-size-base` |
| 0.5  | 8   |       `fa-xs`       |  `--icon-size-xs`  |
| 0.75 | 12  |       `fa-sm`       |  `--icon-size-sm`  |
| 1.25 | 20  |       `fa-lg`       |  `--icon-size-lg`  |
| 2    | 32  |       `fa-2x`       |  `--icon-size-2x`  |
| 3    | 48  |       `fa-3x`       |  `--icon-size-3x`  |
| 4    | 64  |       `fa-4x`       |  `--icon-size-4x`  |
| 5    | 80  |       `fa-5x`       |  `--icon-size-5x`  |
| 6    | 96  |       `fa-6x`       |  `--icon-size-6x`  |
| 7    | 112 |       `fa-7x`       |  `--icon-size-7x`  |
| 8    | 128 |       `fa-8x`       |  `--icon-size-8x`  |
| 9    | 144 |       `fa-9x`       |  `--icon-size-9x`  |
| 10   | 160 |      `fa-10x`       | `--icon-size-10x`  |

---

## Cores

### Red

| Nome da Cor | Luminância |       HSL / Hexadecimal        |   Token    |
| :---------: | :--------: | :----------------------------: | :--------: |
|  `Red 90`   |  `0.008`   | `hsl(0, 10%, 10%)` / `#1b1616` | `--red-90` |
|  `Red 80`   |  `0.027`   | `hsl(5, 23%, 20%)` / `#3e2927` | `--red-80` |
|  `Red 70`   |  `0.059`   | `hsl(2, 39%, 31%)` / `#6f3331` | `--red-70` |
|  `Red 60`   |  `0.106`   | `hsl(0, 49%, 43%)` / `#a23737` | `--red-60` |
|  `Red 50`   |  `0.177`   | `hsl(2, 68%, 52%)` / `#d83933` | `--red-50` |
|  `Red 40`   |  `0.282`   | `hsl(4, 76%, 64%)` / `#e9695f` | `--red-40` |
|  `Red 30`   |  `0.416`   | `hsl(4, 80%, 75%)` / `#f2938c` | `--red-30` |
|  `Red 20`   |  `0.584`   | `hsl(9, 81%, 83%)` / `#F7BBB1` | `--red-20` |
|  `Red 10`   |  `0.790`   | `hsl(7, 65%, 92%)` / `#F8E1DE` | `--red-10` |
|   `Red 5`   |  `0.874`   | `hsl(0, 48%, 95%)` / `#F9EEEE` | `--red-5`  |

### Red Vivid

|  Nome da Cor   | Luminância |        HSL / Hexadecimal        |      Token       |
| :------------: | :--------: | :-----------------------------: | :--------------: |
| `Red Vivid 80` |  `0.027`   | `hsl(0, 69%, 21%)` / `#5C1111`  | `--red-vivid-80` |
| `Red Vivid 70` |  `0.057`   | `hsl(3, 96%, 28%)` / `#8B0A03`  | `--red-vivid-70` |
| `Red Vivid 60` |  `0.100`   | `hsl(0, 91%, 37%)` / `#B50909`  | `--red-vivid-60` |
| `Red Vivid 50` |  `0.178`   | `hsl(7, 94%, 46%)` / `#E52207`  | `--red-vivid-50` |
| `Red Vivid 40` |  `0.282`   | `hsl(6, 96%, 63%)` / `#FB5A47`  | `--red-vivid-40` |
| `Red Vivid 30` |  `0.417`   | `hsl(8, 100%, 74%)` / `#FF8D7B` | `--red-vivid-30` |
| `Red Vivid 20` |  `0.582`   | `hsl(8, 95%, 84%)` / `#FDB8AE`  | `--red-vivid-20` |
| `Red Vivid 10` |  `0.793`   | `hsl(9, 89%, 93%)` / `#FDE0DB`  | `--red-vivid-10` |
| `Red Vivid 5`  |  `0.917`   | `hsl(5, 100%, 97%)` / `#FFF3F2` | `--red-vivid-5`  |

### Red Cool

|  Nome da Cor  | Luminância |        HSL / Hexadecimal         |      Token      |
| :-----------: | :--------: | :------------------------------: | :-------------: |
| `Red Cool 90` |  `0.008`   | `hsl(347, 18%, 10%)` / `#1E1517` | `--red-cool-90` |
| `Red Cool 80` |  `0.027`   | `hsl(350, 23%, 20%)` / `#40282C` | `--red-cool-80` |
| `Red Cool 70` |  `0.059`   | `hsl(349, 32%, 31%)` / `#68363F` | `--red-cool-70` |
| `Red Cool 60` |  `0.107`   | `hsl(349, 47%, 42%)` / `#9E394B` | `--red-cool-60` |
| `Red Cool 50` |  `0.176`   | `hsl(349, 58%, 53%)` / `#CD425B` | `--red-cool-50` |
| `Red Cool 40` |  `0.280`   | `hsl(349, 66%, 65%)` / `#E16B80` | `--red-cool-40` |
| `Red Cool 30` |  `0.417`   | `hsl(350, 53%, 74%)` / `#E09AA6` | `--red-cool-30` |
| `Red Cool 20` |  `0.587`   | `hsl(350, 55%, 84%)` / `#ECBEC6` | `--red-cool-20` |
| `Red Cool 10` |  `0.785`   | `hsl(350, 43%, 92%)` / `#F3E1E4` | `--red-cool-10` |
| `Red Cool 5`  |  `0.880`   | `hsl(347, 39%, 95%)` / `#F8EFF1` | `--red-cool-5`  |

### Red Cool Vivid

|     Nome da Cor     | Luminância |         HSL / Hexadecimal         |         Token         |
| :-----------------: | :--------: | :-------------------------------: | :-------------------: |
| `Red Cool Vivid 80` |  `0.026`   | `hsl(351, 48%, 21%)` / `#4F1C24`  | `--red-cool-vivid-80` |
| `Red Cool Vivid 70` |  `0.060`   | `hsl(349, 60%, 32%)` / `#822133`  | `--red-cool-vivid-70` |
| `Red Cool Vivid 60` |  `0.106`   | `hsl(349, 72%, 41%)` / `#B21D38`  | `--red-cool-vivid-60` |
| `Red Cool Vivid 50` |  `0.177`   | `hsl(350, 79%, 50%)` / `#E41D3D`  | `--red-cool-vivid-50` |
| `Red Cool Vivid 40` |  `0.284`   | `hsl(349, 87%, 66%)` / `#F45D79`  | `--red-cool-vivid-40` |
| `Red Cool Vivid 30` |  `0.418`   | `hsl(349, 97%, 77%)` / `#FD8BA0`  | `--red-cool-vivid-30` |
| `Red Cool Vivid 20` |  `0.586`   | `hsl(349, 82%, 85%)` / `#F8B9C5`  | `--red-cool-vivid-20` |
| `Red Cool Vivid 10` |  `0.782`   | `hsl(353, 64%, 92%)` / `#F8DFE2`  | `--red-cool-vivid-10` |
| `Red Cool Vivid 5`  |  `0.913`   | `hsl(346, 100%, 97%)` / `#FFF2F5` | `--red-cool-vivid-5`  |

### Red Warm

|  Nome da Cor  | Luminância |        HSL / Hexadecimal        |      Token      |
| :-----------: | :--------: | :-----------------------------: | :-------------: |
| `Red Warm 90` |  `0.011`   | `hsl(34, 13%, 11%)` / `#1F1C18` | `--red-warm-90` |
| `Red Warm 80` |  `0.027`   | `hsl(24, 11%, 18%)` / `#332D29` | `--red-warm-80` |
| `Red Warm 70` |  `0.059`   | `hsl(26, 21%, 27%)` / `#524236` | `--red-warm-70` |
| `Red Warm 60` |  `0.106`   | `hsl(19, 38%, 36%)` / `#805039` | `--red-warm-60` |
| `Red Warm 50` |  `0.176`   | `hsl(15, 63%, 47%)` / `#C3512C` | `--red-warm-50` |
| `Red Warm 40` |  `0.282`   | `hsl(17, 58%, 58%)` / `#D27A56` | `--red-warm-40` |
| `Red Warm 30` |  `0.419`   | `hsl(20, 57%, 68%)` / `#DCA081` | `--red-warm-30` |
| `Red Warm 20` |  `0.583`   | `hsl(22, 64%, 79%)` / `#ECC0A7` | `--red-warm-20` |
| `Red Warm 10` |  `0.792`   | `hsl(19, 53%, 91%)` / `#F4E3DB` | `--red-warm-10` |
| `Red Warm 5`  |  `0.872`   | `hsl(25, 40%, 94%)` / `#F6EFEA` | `--red-warm-5`  |

### Red Warm Vivid

|     Nome da Cor     | Luminância |        HSL / Hexadecimal         |         Token         |
| :-----------------: | :--------: | :------------------------------: | :-------------------: |
| `Red Warm Vivid 80` |  `0.027`   | `hsl(23, 35%, 18%)` / `#3E2A1E`  | `--red-warm-vivid-80` |
| `Red Warm Vivid 70` |  `0.051`   | `hsl(26, 74%, 22%)` / `#63340F`  | `--red-warm-vivid-70` |
| `Red Warm Vivid 60` |  `0.104`   | `hsl(19, 81%, 34%)` / `#9C3D10`  | `--red-warm-vivid-60` |
| `Red Warm Vivid 50` |  `0.181`   | `hsl(17, 92%, 44%)` / `#D54309`  | `--red-warm-vivid-50` |
| `Red Warm Vivid 40` |  `0.264`   | `hsl(17, 86%, 54%)` / `#EF5E25`  | `--red-warm-vivid-40` |
| `Red Warm Vivid 30` |  `0.406`   | `hsl(18, 85%, 68%)` / `#F39268`  | `--red-warm-vivid-30` |
| `Red Warm Vivid 20` |  `0.583`   | `hsl(22, 83%, 79%)` / `#F6BD9C`  | `--red-warm-vivid-20` |
| `Red Warm Vivid 10` |  `0.792`   | `hsl(19, 87%, 91%)` / `#FCE1D4`  | `--red-warm-vivid-10` |
| `Red Warm Vivid 5`  |  `0.927`   | `hsl(25, 100%, 97%)` / `#FFF5EE` | `--red-warm-vivid-5`  |

### Orange

| Nome da Cor | Luminância |        HSL / Hexadecimal        |     Token     |
| :---------: | :--------: | :-----------------------------: | :-----------: |
| `Orange 90` |  `0.008`   | `hsl(17, 15%, 9%)` / `#1B1614`  | `--orange-90` |
| `Orange 80` |  `0.027`   | `hsl(30, 13%, 18%)` / `#332D27` | `--orange-80` |
| `Orange 70` |  `0.059`   | `hsl(26, 21%, 27%)` / `#524236` | `--orange-70` |
| `Orange 60` |  `0.107`   | `hsl(23, 30%, 36%)` / `#775540` | `--orange-60` |
| `Orange 50` |  `0.176`   | `hsl(26, 48%, 43%)` / `#A26739` | `--orange-50` |
| `Orange 40` |  `0.283`   | `hsl(23, 71%, 53%)` / `#DD7533` | `--orange-40` |
| `Orange 30` |  `0.418`   | `hsl(23, 83%, 66%)` / `#F09860` | `--orange-30` |
| `Orange 20` |  `0.583`   | `hsl(28, 80%, 76%)` / `#F3BF90` | `--orange-20` |
| `Orange 10` |  `0.791`   | `hsl(32, 54%, 89%)` / `#F2E4D4` | `--orange-10` |
| `Orange 5`  |  `0.872`   | `hsl(28, 42%, 94%)` / `#F6EFE9` | `--orange-5`  |

### Orange Vivid

|    Nome da Cor    | Luminância |        HSL / Hexadecimal         |        Token        |
| :---------------: | :--------: | :------------------------------: | :-----------------: |
| `Orange Vivid 80` |  `0.020`   | `hsl(28, 47%, 14%)` / `#352313`  | `--orange-vivid-80` |
| `Orange Vivid 70` |  `0.051`   | `hsl(26, 61%, 23%)` / `#5F3617`  | `--orange-vivid-70` |
| `Orange Vivid 60` |  `0.101`   | `hsl(23, 67%, 33%)` / `#8C471C`  | `--orange-vivid-60` |
| `Orange Vivid 50` |  `0.178`   | `hsl(27, 100%, 38%)` / `#C05600` | `--orange-vivid-50` |
| `Orange Vivid 40` |  `0.282`   | `hsl(27, 89%, 48%)` / `#E66F0E`  | `--orange-vivid-40` |
| `Orange Vivid 30` |  `0.400`   | `hsl(33, 100%, 50%)` / `#FF8C00` | `--orange-vivid-30` |
| `Orange Vivid 20` |  `0.585`   | `hsl(30, 100%, 74%)` / `#FFBC78` | `--orange-vivid-20` |
| `Orange Vivid 10` |  `0.791`   | `hsl(32, 90%, 88%)` / `#FCE2C5`  | `--orange-vivid-10` |
| `Orange Vivid 5`  |  `0.919`   | `hsl(39, 85%, 95%)` / `#FDF5E6`  | `--orange-vivid-5`  |

### Orange Warm

|   Nome da Cor    | Luminância |        HSL / Hexadecimal        |       Token        |
| :--------------: | :--------: | :-----------------------------: | :----------------: |
| `Orange Warm 90` |  `0.008`   | `hsl(9, 14%, 10%)` / `#1C1615`  | `--orange-warm-90` |
| `Orange Warm 80` |  `0.027`   | `hsl(10, 24%, 19%)` / `#3D2925` | `--orange-warm-80` |
| `Orange Warm 70` |  `0.059`   | `hsl(10, 33%, 29%)` / `#633A32` | `--orange-warm-70` |
| `Orange Warm 60` |  `0.107`   | `hsl(12, 47%, 39%)` / `#914734` | `--orange-warm-60` |
| `Orange Warm 50` |  `0.177`   | `hsl(19, 66%, 45%)` / `#BD5727` | `--orange-warm-50` |
| `Orange Warm 40` |  `0.281`   | `hsl(18, 73%, 57%)` / `#E17141` | `--orange-warm-40` |
| `Orange Warm 30` |  `0.419`   | `hsl(18, 85%, 69%)` / `#F3966D` | `--orange-warm-30` |
| `Orange Warm 20` |  `0.583`   | `hsl(18, 84%, 80%)` / `#F7BCA2` | `--orange-warm-20` |
| `Orange Warm 10` |  `0.783`   | `hsl(22, 84%, 90%)` / `#FBE0D0` | `--orange-warm-10` |
| `Orange Warm 5`  |  `0.871`   | `hsl(26, 68%, 94%)` / `#FAEEE5` | `--orange-warm-5`  |

### Orange Warm Vivid

|      Nome da Cor       | Luminância |        HSL / Hexadecimal         |          Token           |
| :--------------------: | :--------: | :------------------------------: | :----------------------: |
| `Orange Warm Vivid 80` |  `0.022`   | `hsl(11, 36%, 18%)` / `#3D231D`  | `--orange-warm-vivid-80` |
| `Orange Warm Vivid 70` |  `0.052`   | `hsl(10, 74%, 27%)` / `#782312`  | `--orange-warm-vivid-70` |
| `Orange Warm Vivid 60` |  `0.102`   | `hsl(12, 83%, 36%)` / `#A72F10`  | `--orange-warm-vivid-60` |
| `Orange Warm Vivid 50` |  `0.180`   | `hsl(21, 100%, 41%)` / `#CF4900` | `--orange-warm-vivid-50` |
| `Orange Warm Vivid 40` |  `0.282`   | `hsl(19, 100%, 52%)` / `#FF580A` | `--orange-warm-vivid-40` |
| `Orange Warm Vivid 30` |  `0.417`   | `hsl(15, 96%, 71%)` / `#FC906D`  | `--orange-warm-vivid-30` |
| `Orange Warm Vivid 20` |  `0.584`   | `hsl(14, 91%, 82%)` / `#FBBAA7`  | `--orange-warm-vivid-20` |
| `Orange Warm Vivid 10` |  `0.802`   | `hsl(22, 100%, 91%)` / `#FFE2D1` | `--orange-warm-vivid-10` |
| `Orange Warm Vivid 5`  |  `0.913`   | `hsl(26, 100%, 96%)` / `#FFF3EA` | `--orange-warm-vivid-5`  |

### Gold

| Nome da Cor | Luminância |        HSL / Hexadecimal        |    Token    |
| :---------: | :--------: | :-----------------------------: | :---------: |
|  `Gold 90`  |  `0.008`   | `hsl(36, 11%, 9%)` / `#191714`  | `--gold-90` |
|  `Gold 80`  |  `0.026`   | `hsl(35, 14%, 17%)` / `#322D26` | `--gold-80` |
|  `Gold 70`  |  `0.059`   | `hsl(34, 16%, 26%)` / `#4D4438` | `--gold-70` |
|  `Gold 60`  |  `0.107`   | `hsl(30, 20%, 35%)` / `#6B5947` | `--gold-60` |
|  `Gold 50`  |  `0.179`   | `hsl(31, 29%, 43%)` / `#8E704F` | `--gold-50` |
|  `Gold 40`  |  `0.282`   | `hsl(32, 31%, 54%)` / `#AD8B65` | `--gold-40` |
|  `Gold 30`  |  `0.419`   | `hsl(36, 40%, 63%)` / `#C7A97B` | `--gold-30` |
|  `Gold 20`  |  `0.582`   | `hsl(39, 51%, 74%)` / `#DEC69A` | `--gold-20` |
|  `Gold 10`  |  `0.791`   | `hsl(40, 56%, 87%)` / `#F1E5CD` | `--gold-10` |
|  `Gold 5`   |  `0.874`   | `hsl(40, 43%, 93%)` / `#F5F0E6` | `--gold-5`  |

### Gold Vivid

|   Nome da Cor   | Luminância |        HSL / Hexadecimal         |       Token       |
| :-------------: | :--------: | :------------------------------: | :---------------: |
| `Gold Vivid 80` |  `0.027`   | `hsl(35, 48%, 16%)` / `#3B2B15`  | `--gold-vivid-80` |
| `Gold Vivid 70` |  `0.060`   | `hsl(40, 80%, 20%)` / `#5C410A`  | `--gold-vivid-70` |
| `Gold Vivid 60` |  `0.113`   | `hsl(39, 65%, 29%)` / `#7A591A`  | `--gold-vivid-60` |
| `Gold Vivid 50` |  `0.178`   | `hsl(36, 45%, 40%)` / `#936F38`  | `--gold-vivid-50` |
| `Gold Vivid 40` |  `0.282`   | `hsl(40, 88%, 40%)` / `#C2850C`  | `--gold-vivid-40` |
| `Gold Vivid 30` |  `0.417`   | `hsl(42, 100%, 45%)` / `#E5A000` | `--gold-vivid-30` |
| `Gold Vivid 20` |  `0.582`   | `hsl(41, 100%, 59%)` / `#FFBE2E` | `--gold-vivid-20` |
| `Gold Vivid 10` |  `0.784`   | `hsl(44, 100%, 79%)` / `#FFE396` | `--gold-vivid-10` |
| `Gold Vivid 5`  |  `0.875`   | `hsl(44, 96%, 89%)` / `#FEF0C8`  | `--gold-vivid-5`  |

### Yellow

| Nome da Cor | Luminância |        HSL / Hexadecimal        |     Token     |
| :---------: | :--------: | :-----------------------------: | :-----------: |
| `Yellow 90` |  `0.008`   | `hsl(20, 13%, 9%)` / `#1A1614`  | `--yellow-90` |
| `Yellow 80` |  `0.027`   | `hsl(30, 13%, 18%)` / `#332D27` | `--yellow-80` |
| `Yellow 70` |  `0.059`   | `hsl(34, 23%, 25%)` / `#504332` | `--yellow-70` |
| `Yellow 60` |  `0.107`   | `hsl(40, 30%, 32%)` / `#6B5A39` | `--yellow-60` |
| `Yellow 50` |  `0.177`   | `hsl(43, 43%, 38%)` / `#8A7237` | `--yellow-50` |
| `Yellow 40` |  `0.284`   | `hsl(44, 40%, 47%)` / `#A88F48` | `--yellow-40` |
| `Yellow 30` |  `0.420`   | `hsl(46, 54%, 54%)` / `#C9AB48` | `--yellow-30` |
| `Yellow 20` |  `0.581`   | `hsl(48, 75%, 60%)` / `#E6C74C` | `--yellow-20` |
| `Yellow 10` |  `0.791`   | `hsl(47, 78%, 82%)` / `#F5E6AF` | `--yellow-10` |
| `Yellow 5`  |  `0.890`   | `hsl(50, 80%, 90%)` / `#FAF3D1` | `--yellow-5`  |

### Yellow Vivid

|    Nome da Cor    | Luminância |        HSL / Hexadecimal         |        Token        |
| :---------------: | :--------: | :------------------------------: | :-----------------: |
| `Yellow Vivid 80` |  `0.031`   | `hsl(29, 45%, 18%)` / `#422D19`  | `--yellow-vivid-80` |
| `Yellow Vivid 70` |  `0.069`   | `hsl(46, 82%, 20%)` / `#5C4809`  | `--yellow-vivid-70` |
| `Yellow Vivid 60` |  `0.123`   | `hsl(46, 68%, 28%)` / `#776017`  | `--yellow-vivid-60` |
| `Yellow Vivid 50` |  `0.181`   | `hsl(46, 100%, 29%)` / `#947100` | `--yellow-vivid-50` |
| `Yellow Vivid 40` |  `0.283`   | `hsl(47, 100%, 35%)` / `#B38C00` | `--yellow-vivid-40` |
| `Yellow Vivid 30` |  `0.441`   | `hsl(46, 99%, 44%)` / `#DDAA01`  | `--yellow-vivid-30` |
| `Yellow Vivid 20` |  `0.649`   | `hsl(48, 100%, 51%)` / `#FFCD07` | `--yellow-vivid-20` |
| `Yellow Vivid 10` |  `0.793`   | `hsl(48, 98%, 76%)` / `#FEE685`  | `--yellow-vivid-10` |
| `Yellow Vivid 5`  |  `0.904`   | `hsl(50, 100%, 88%)` / `#FFF5C2` | `--yellow-vivid-5`  |

### Green

| Nome da Cor | Luminância |        HSL / Hexadecimal        |    Token     |
| :---------: | :--------: | :-----------------------------: | :----------: |
| `Green 90`  |  `0.008`   |  `hsl(90, 9%, 9%)` / `#161814`  | `--green-90` |
| `Green 80`  |  `0.026`   | `hsl(88, 19%, 16%)` / `#293021` | `--green-80` |
| `Green 70`  |  `0.060`   | `hsl(85, 29%, 23%)` / `#3C4A29` | `--green-70` |
| `Green 60`  |  `0.107`   | `hsl(83, 47%, 27%)` / `#4C6424` | `--green-60` |
| `Green 50`  |  `0.179`   | `hsl(85, 41%, 35%)` / `#607F35` | `--green-50` |
| `Green 40`  |  `0.283`   | `hsl(83, 33%, 46%)` / `#7D9B4E` | `--green-40` |
| `Green 30`  |  `0.416`   | `hsl(83, 33%, 46%)` / `#7D9B4E` | `--green-30` |
| `Green 20`  |  `0.583`   | `hsl(85, 41%, 70%)` / `#B8D293` | `--green-20` |
| `Green 10`  |  `0.789`   | `hsl(83, 41%, 86%)` / `#DFEACD` | `--green-10` |
|  `Green 5`  |  `0.874`   | `hsl(86, 51%, 91%)` / `#EAF4DD` | `--green-5`  |

### Green Vivid

|   Nome da Cor    | Luminância |        HSL / Hexadecimal         |       Token        |
| :--------------: | :--------: | :------------------------------: | :----------------: |
| `Green Vivid 80` |  `0.028`   | `hsl(89, 46%, 14%)` / `#243413`  | `--green-vivid-80` |
| `Green Vivid 70` |  `0.055`   | `hsl(86, 74%, 17%)` / `#2F4A0B`  | `--green-vivid-70` |
| `Green Vivid 60` |  `0.120`   | `hsl(82, 93%, 22%)` / `#466C04`  | `--green-vivid-60` |
| `Green Vivid 50` |  `0.178`   | `hsl(82, 100%, 25%)` / `#538200` | `--green-vivid-50` |
| `Green Vivid 40` |  `0.284`   | `hsl(84, 58%, 39%)` / `#719F2A`  | `--green-vivid-40` |
| `Green Vivid 30` |  `0.362`   | `hsl(84, 54%, 45%)` / `#7FB135`  | `--green-vivid-30` |
| `Green Vivid 20` |  `0.520`   | `hsl(82, 62%, 51%)` / `#98D035`  | `--green-vivid-20` |
| `Green Vivid 10` |  `0.751`   | `hsl(87, 73%, 75%)` / `#C5EE93`  | `--green-vivid-10` |
| `Green Vivid 5`  |  `0.872`   | `hsl(94, 81%, 88%)` / `#DDF9C7`  | `--green-vivid-5`  |

### Green Cool

|   Nome da Cor   | Luminância |        HSL / Hexadecimal         |       Token       |
| :-------------: | :--------: | :------------------------------: | :---------------: |
| `Green Cool 90` |  `0.012`   | `hsl(120, 9%, 11%)` / `#1A1F1A`  | `--green-cool-90` |
| `Green Cool 80` |  `0.028`   | `hsl(133, 10%, 17%)` / `#28312A` | `--green-cool-80` |
| `Green Cool 70` |  `0.058`   | `hsl(133, 14%, 25%)` / `#37493B` | `--green-cool-70` |
| `Green Cool 60` |  `0.107`   | `hsl(118, 20%, 33%)` / `#446443` | `--green-cool-60` |
| `Green Cool 50` |  `0.176`   | `hsl(129, 25%, 40%)` / `#4d8055` | `--green-cool-50` |
| `Green Cool 40` |  `0.281`   | `hsl(130, 26%, 50%)` / `#5E9F69` | `--green-cool-40` |
| `Green Cool 30` |  `0.417`   | `hsl(129, 27%, 63%)` / `#86B98E` | `--green-cool-30` |
| `Green Cool 20` |  `0.583`   | `hsl(131, 23%, 76%)` / `#B4D0B9` | `--green-cool-20` |
| `Green Cool 10` |  `0.797`   | `hsl(131, 29%, 89%)` / `#DBEBDE` | `--green-cool-10` |
| `Green Cool 5`  |  `0.879`   | `hsl(120, 23%, 94%)` / `#ECF3EC` | `--green-cool-5`  |

### Green Cool Vivid

|      Nome da Cor      | Luminância |         HSL / Hexadecimal         |          Token          |
| :-------------------: | :--------: | :-------------------------------: | :---------------------: |
| `Green Cool Vivid 80` |  `0.024`   | `hsl(133, 32%, 15%)` / `#19311E`  | `--green-cool-vivid-80` |
| `Green Cool Vivid 70` |  `0.054`   | `hsl(133, 57%, 19%)` / `#154C21`  | `--green-cool-vivid-70` |
| `Green Cool Vivid 60` |  `0.115`   | `hsl(118, 56%, 28%)` / `#216E1F`  | `--green-cool-vivid-60` |
| `Green Cool Vivid 50` |  `0.178`   | `hsl(126, 72%, 31%)` / `#168821`  | `--green-cool-vivid-50` |
| `Green Cool Vivid 40` |  `0.284`   | `hsl(130, 100%, 33%)` / `#00A91C` | `--green-cool-vivid-40` |
| `Green Cool Vivid 30` |  `0.418`   | `hsl(127, 72%, 46%)` / `#21C834`  | `--green-cool-vivid-30` |
| `Green Cool Vivid 20` |  `0.587`   | `hsl(126, 65%, 66%)` / `#70E17B`  | `--green-cool-vivid-20` |
| `Green Cool Vivid 10` |  `0.790`   | `hsl(126, 76%, 84%)` / `#B7F5BD`  | `--green-cool-vivid-10` |
| `Green Cool Vivid 5`  |  `0.870`   | `hsl(114, 50%, 92%)` / `#E3F5E1`  | `--green-cool-vivid-5`  |

### Green Warm

|   Nome da Cor   | Luminância |        HSL / Hexadecimal        |       Token       |
| :-------------: | :--------: | :-----------------------------: | :---------------: |
| `Green Warm 90` |  `0.008`   | `hsl(60, 12%, 8%)` / `#171712`  | `--green-warm-90` |
| `Green Warm 80` |  `0.027`   | `hsl(69, 18%, 16%)` / `#2D2F21` | `--green-warm-80` |
| `Green Warm 70` |  `0.059`   | `hsl(65, 20%, 23%)` / `#45472F` | `--green-warm-70` |
| `Green Warm 60` |  `0.106`   | `hsl(68, 26%, 30%)` / `#5A5F38` | `--green-warm-60` |
| `Green Warm 50` |  `0.176`   | `hsl(72, 30%, 37%)` / `#6F7A41` | `--green-warm-50` |
| `Green Warm 40` |  `0.283`   | `hsl(71, 34%, 45%)` / `#8A984B` | `--green-warm-40` |
| `Green Warm 30` |  `0.418`   | `hsl(70, 39%, 53%)` / `#A6B557` | `--green-warm-30` |
| `Green Warm 20` |  `0.597`   | `hsl(64, 49%, 65%)` / `#CBD17A` | `--green-warm-20` |
| `Green Warm 10` |  `0.792`   | `hsl(64, 55%, 82%)` / `#E7EAB7` | `--green-warm-10` |
| `Green Warm 5`  |  `0.883`   | `hsl(66, 57%, 90%)` / `#F1F4D7` | `--green-warm-5`  |

### Green Warm Vivid

|      Nome da Cor      | Luminância |        HSL / Hexadecimal         |          Token          |
| :-------------------: | :--------: | :------------------------------: | :---------------------: |
| `Green Warm Vivid 80` |  `0.036`   | `hsl(60, 67%, 13%)` / `#38380B`  | `--green-warm-vivid-80` |
| `Green Warm Vivid 70` |  `0.069`   | `hsl(63, 66%, 18%)` / `#4B4E10`  | `--green-warm-vivid-70` |
| `Green Warm Vivid 60` |  `0.117`   | `hsl(69, 69%, 24%)` / `#5A6613`  | `--green-warm-vivid-60` |
| `Green Warm Vivid 50` |  `0.177`   | `hsl(69, 100%, 25%)` / `#6A7D00` | `--green-warm-vivid-50` |
| `Green Warm Vivid 40` |  `0.283`   | `hsl(74, 69%, 36%)` / `#7E9C1D`  | `--green-warm-vivid-40` |
| `Green Warm Vivid 30` |  `0.418`   | `hsl(69, 61%, 45%)` / `#A3B72C`  | `--green-warm-vivid-30` |
| `Green Warm Vivid 20` |  `0.584`   | `hsl(64, 91%, 43%)` / `#C5D30A`  | `--green-warm-vivid-20` |
| `Green Warm Vivid 10` |  `0.819`   | `hsl(64, 90%, 58%)` / `#E7F434`  | `--green-warm-vivid-10` |
| `Green Warm Vivid 5`  |  `0.922`   | `hsl(66, 88%, 87%)` / `#F5FBC1`  | `--green-warm-vivid-5`  |

### Mint

| Nome da Cor | Luminância |        HSL / Hexadecimal         |    Token    |
| :---------: | :--------: | :------------------------------: | :---------: |
|  `Mint 90`  |  `0.008`   | `hsl(143, 33%, 8%)` / `#0D1A12`  | `--mint-90` |
|  `Mint 80`  |  `0.027`   | `hsl(145, 34%, 15%)` / `#193324` | `--mint-80` |
|  `Mint 70`  |  `0.060`   | `hsl(146, 42%, 22%)` / `#204E34` | `--mint-70` |
|  `Mint 60`  |  `0.107`   | `hsl(148, 44%, 28%)` / `#286846` | `--mint-60` |
|  `Mint 50`  |  `0.177`   | `hsl(160, 48%, 35%)` / `#2E8367` | `--mint-50` |
|  `Mint 40`  |  `0.284`   | `hsl(160, 52%, 42%)` / `#34A37E` | `--mint-40` |
|  `Mint 30`  |  `0.416`   | `hsl(155, 44%, 55%)` / `#5ABF95` | `--mint-30` |
|  `Mint 20`  |  `0.593`   | `hsl(155, 48%, 71%)` / `#92D9BB` | `--mint-20` |
|  `Mint 10`  |  `0.793`   | `hsl(161, 56%, 86%)` / `#C7EFE2` | `--mint-10` |
|  `Mint 5`   |  `0.870`   | `hsl(160, 60%, 91%)` / `#DBF6ED` | `--mint-5`  |

### Mint Vivid

|   Nome da Cor   | Luminância |         HSL / Hexadecimal         |       Token       |
| :-------------: | :--------: | :-------------------------------: | :---------------: |
| `Mint Vivid 80` |  `0.027`   | `hsl(145, 61%, 13%)` / `#0D351E`  | `--mint-vivid-80` |
| `Mint Vivid 70` |  `0.056`   | `hsl(146, 73%, 18%)` / `#0C4E29`  | `--mint-vivid-70` |
| `Mint Vivid 60` |  `0.107`   | `hsl(156, 68%, 25%)` / `#146947`  | `--mint-vivid-60` |
| `Mint Vivid 50` |  `0.177`   | `hsl(160, 100%, 26%)` / `#008659` | `--mint-vivid-50` |
| `Mint Vivid 40` |  `0.291`   | `hsl(160, 100%, 33%)` / `#00A871` | `--mint-vivid-40` |
| `Mint Vivid 30` |  `0.416`   | `hsl(160, 96%, 39%)` / `#04C585`  | `--mint-vivid-30` |
| `Mint Vivid 20` |  `0.633`   | `hsl(161, 90%, 49%)` / `#0CEDA6`  | `--mint-vivid-20` |
| `Mint Vivid 10` |  `0.791`   | `hsl(160, 95%, 75%)` / `#83FCD4`  | `--mint-vivid-10` |
| `Mint Vivid 5`  |  `0.874`   | `hsl(161, 86%, 89%)` / `#C9FBEB`  | `--mint-vivid-5`  |

### Mint Cool

|  Nome da Cor   | Luminância |        HSL / Hexadecimal         |      Token       |
| :------------: | :--------: | :------------------------------: | :--------------: |
| `Mint Cool 90` |  `0.008`   | `hsl(180, 17%, 8%)` / `#111818`  | `--mint-cool-90` |
| `Mint Cool 80` |  `0.027`   | `hsl(180, 21%, 16%)` / `#203131` | `--mint-cool-80` |
| `Mint Cool 70` |  `0.059`   | `hsl(169, 28%, 23%)` / `#2A4B45` | `--mint-cool-70` |
| `Mint Cool 60` |  `0.108`   | `hsl(177, 29%, 30%)` / `#376462` | `--mint-cool-60` |
| `Mint Cool 50` |  `0.180`   | `hsl(178, 33%, 38%)` / `#40807E` | `--mint-cool-50` |
| `Mint Cool 40` |  `0.284`   | `hsl(176, 33%, 46%)` / `#4F9E99` | `--mint-cool-40` |
| `Mint Cool 30` |  `0.417`   | `hsl(174, 35%, 58%)` / `#6FBAB3` | `--mint-cool-30` |
| `Mint Cool 20` |  `0.585`   | `hsl(175, 40%, 72%)` / `#9BD4CF` | `--mint-cool-20` |
| `Mint Cool 10` |  `0.788`   | `hsl(176, 55%, 85%)` / `#C4EEEB` | `--mint-cool-10` |
| `Mint Cool 5`  |  `0.890`   | `hsl(177, 59%, 92%)` / `#E0F7F6` | `--mint-cool-5`  |

### Mint Cool Vivid

|     Nome da Cor      | Luminância |         HSL / Hexadecimal         |         Token          |
| :------------------: | :--------: | :-------------------------------: | :--------------------: |
| `Mint Cool Vivid 80` |  `0.025`   | `hsl(180, 46%, 13%)` / `#123131`  | `--mint-cool-vivid-80` |
| `Mint Cool Vivid 70` |  `0.054`   | `hsl(169, 74%, 17%)` / `#0B4B3F`  | `--mint-cool-vivid-70` |
| `Mint Cool Vivid 60` |  `0.100`   | `hsl(177, 74%, 23%)` / `#0F6460`  | `--mint-cool-vivid-60` |
| `Mint Cool Vivid 50` |  `0.180`   | `hsl(178, 100%, 26%)` / `#008480` | `--mint-cool-vivid-50` |
| `Mint Cool Vivid 40` |  `0.283`   | `hsl(171, 50%, 42%)` / `#36A191`  | `--mint-cool-vivid-40` |
| `Mint Cool Vivid 30` |  `0.419`   | `hsl(173, 74%, 44%)` / `#1DC2AE`  | `--mint-cool-vivid-30` |
| `Mint Cool Vivid 20` |  `0.589`   | `hsl(174, 72%, 56%)` / `#40E0D0`  | `--mint-cool-vivid-20` |
| `Mint Cool Vivid 10` |  `0.788`   | `hsl(168, 94%, 74%)` / `#7EFBE1`  | `--mint-cool-vivid-10` |
| `Mint Cool Vivid 5`  |  `0.896`   | `hsl(167, 83%, 91%)` / `#D5FBF3`  | `--mint-cool-vivid-5`  |

### Cyan

| Nome da Cor | Luminância |        HSL / Hexadecimal         |    Token    |
| :---------: | :--------: | :------------------------------: | :---------: |
|  `Cyan 90`  |  `0.008`   | `hsl(188, 19%, 8%)` / `#111819`  | `--cyan-90` |
|  `Cyan 80`  |  `0.027`   | `hsl(186, 23%, 16%)` / `#203133` | `--cyan-80` |
|  `Cyan 70`  |  `0.059`   | `hsl(187, 28%, 24%)` / `#2C4A4E` | `--cyan-70` |
|  `Cyan 60`  |  `0.107`   | `hsl(188, 44%, 30%)` / `#2A646D` | `--cyan-60` |
|  `Cyan 50`  |  `0.176`   | `hsl(189, 74%, 33%)` / `#168092` | `--cyan-50` |
|  `Cyan 40`  |  `0.283`   | `hsl(189, 43%, 47%)` / `#449DAC` | `--cyan-40` |
|  `Cyan 30`  |  `0.446`   | `hsl(189, 56%, 59%)` / `#5DC0D1` | `--cyan-30` |
|  `Cyan 20`  |  `0.649`   | `hsl(189, 66%, 76%)` / `#99DEEA` | `--cyan-20` |
|  `Cyan 10`  |  `0.792`   | `hsl(189, 59%, 87%)` / `#CCECF2` | `--cyan-10` |
|  `Cyan 5`   |  `0.896`   | `hsl(187, 55%, 94%)` / `#E7F6F8` | `--cyan-5`  |

### Cyan Vivid

|   Nome da Cor   | Luminância |         HSL / Hexadecimal         |       Token       |
| :-------------: | :--------: | :-------------------------------: | :---------------: |
| `Cyan Vivid 80` |  `0.036`   | `hsl(189, 77%, 15%)` / `#093B44`  | `--cyan-vivid-80` |
| `Cyan Vivid 70` |  `0.064`   | `hsl(190, 74%, 21%)` / `#0E4F5C`  | `--cyan-vivid-70` |
| `Cyan Vivid 60` |  `0.113`   | `hsl(190, 100%, 25%)` / `#00687D` | `--cyan-vivid-60` |
| `Cyan Vivid 50` |  `0.182`   | `hsl(192, 100%, 32%)` / `#0081A1` | `--cyan-vivid-50` |
| `Cyan Vivid 40` |  `0.283`   | `hsl(191, 100%, 38%)` / `#009EC1` | `--cyan-vivid-40` |
| `Cyan Vivid 30` |  `0.419`   | `hsl(190, 100%, 45%)` / `#00BDE3` | `--cyan-vivid-30` |
| `Cyan Vivid 20` |  `0.583`   | `hsl(189, 86%, 64%)` / `#52DAF2`  | `--cyan-vivid-20` |
| `Cyan Vivid 10` |  `0.790`   | `hsl(189, 100%, 83%)` / `#A8F2FF` | `--cyan-vivid-10` |
| `Cyan Vivid 5`  |  `0.922`   | `hsl(192, 100%, 95%)` / `#E5FAFF` | `--cyan-vivid-5`  |

### Blue

| Nome da Cor | Luminância |        HSL / Hexadecimal         |    Token    |
| :---------: | :--------: | :------------------------------: | :---------: |
|  `Blue 90`  |  `0.008`   | `hsl(205, 26%, 9%)` / `#11181D`  | `--blue-90` |
|  `Blue 80`  |  `0.027`   | `hsl(207, 33%, 18%)` / `#1F303E` | `--blue-80` |
|  `Blue 70`  |  `0.059`   | `hsl(207, 43%, 27%)` / `#274863` | `--blue-70` |
|  `Blue 60`  |  `0.107`   | `hsl(207, 52%, 36%)` / `#2C608A` | `--blue-60` |
|  `Blue 50`  |  `0.177`   | `hsl(208, 70%, 45%)` / `#2378C3` | `--blue-50` |
|  `Blue 40`  |  `0.283`   | `hsl(207, 59%, 56%)` / `#4F97D1` | `--blue-40` |
|  `Blue 30`  |  `0.416`   | `hsl(207, 71%, 68%)` / `#73B3E7` | `--blue-30` |
|  `Blue 20`  |  `0.582`   | `hsl(208, 63%, 80%)` / `#AACDEC` | `--blue-20` |
|  `Blue 10`  |  `0.797`   | `hsl(216, 80%, 92%)` / `#DBE8FB` | `--blue-10` |
|  `Blue 5`   |  `0.912`   | `hsl(205, 60%, 96%)` / `#EFF6FB` | `--blue-5`  |

### Blue Vivid

|   Nome da Cor   | Luminância |         HSL / Hexadecimal         |       Token       |
| :-------------: | :--------: | :-------------------------------: | :---------------: |
| `Blue Vivid 80` |  `0.027`   | `hsl(210, 64%, 19%)` / `#112F4E`  | `--blue-vivid-80` |
| `Blue Vivid 70` |  `0.059`   | `hsl(207, 83%, 26%)` / `#0B4778`  | `--blue-vivid-70` |
| `Blue Vivid 60` |  `0.106`   | `hsl(205, 100%, 32%)` / `#005EA2` | `--blue-vivid-60` |
| `Blue Vivid 50` |  `0.178`   | `hsl(207, 100%, 42%)` / `#0076D6` | `--blue-vivid-50` |
| `Blue Vivid 40` |  `0.278`   | `hsl(210, 100%, 57%)` / `#2491FF` | `--blue-vivid-40` |
| `Blue Vivid 30` |  `0.419`   | `hsl(207, 100%, 67%)` / `#58B4FF` | `--blue-vivid-30` |
| `Blue Vivid 20` |  `0.613`   | `hsl(208, 100%, 82%)` / `#A1D3FF` | `--blue-vivid-20` |
| `Blue Vivid 10` |  `0.781`   | `hsl(209, 100%, 91%)` / `#CFE8FF` | `--blue-vivid-10` |
| `Blue Vivid 5`  |  `0.896`   | `hsl(206, 100%, 95%)` / `#E8F5FF` | `--blue-vivid-5`  |

### Blue Cool

|  Nome da Cor   | Luminância |        HSL / Hexadecimal         |      Token       |
| :------------: | :--------: | :------------------------------: | :--------------: |
| `Blue Cool 90` |  `0.008`   | `hsl(194, 30%, 8%)` / `#0F191C`  | `--blue-cool-90` |
| `Blue Cool 80` |  `0.028`   | `hsl(195, 51%, 16%)` / `#14333D` | `--blue-cool-80` |
| `Blue Cool 70` |  `0.059`   | `hsl(196, 44%, 24%)` / `#224A58` | `--blue-cool-70` |
| `Blue Cool 60` |  `0.106`   | `hsl(197, 44%, 32%)` / `#2E6276` | `--blue-cool-60` |
| `Blue Cool 50` |  `0.177`   | `hsl(196, 44%, 41%)` / `#3A7D95` | `--blue-cool-50` |
| `Blue Cool 40` |  `0.285`   | `hsl(198, 32%, 54%)` / `#6499AF` | `--blue-cool-40` |
| `Blue Cool 30` |  `0.416`   | `hsl(198, 40%, 65%)` / `#82B4C9` | `--blue-cool-30` |
| `Blue Cool 20` |  `0.586`   | `hsl(197, 40%, 77%)` / `#ADCFDC` | `--blue-cool-20` |
| `Blue Cool 10` |  `0.793`   | `hsl(195, 37%, 89%)` / `#DAE9EE` | `--blue-cool-10` |
| `Blue Cool 5`  |  `0.870`   | `hsl(193, 41%, 93%)` / `#E7F2F5` | `--blue-cool-5`  |

### Blue Cool Vivid

|     Nome da Cor      | Luminância |         HSL / Hexadecimal         |         Token          |
| :------------------: | :--------: | :-------------------------------: | :--------------------: |
| `Blue Cool Vivid 80` |  `0.022`   | `hsl(197, 100%, 12%)` / `#002D3F` | `--blue-cool-vivid-80` |
| `Blue Cool Vivid 70` |  `0.060`   | `hsl(198, 87%, 22%)` / `#074B69`  | `--blue-cool-vivid-70` |
| `Blue Cool Vivid 60` |  `0.110`   | `hsl(198, 91%, 29%)` / `#07648D`  | `--blue-cool-vivid-60` |
| `Blue Cool Vivid 50` |  `0.176`   | `hsl(194, 85%, 34%)` / `#0D7EA2`  | `--blue-cool-vivid-50` |
| `Blue Cool Vivid 40` |  `0.299`   | `hsl(196, 67%, 48%)` / `#28A0CB`  | `--blue-cool-vivid-40` |
| `Blue Cool Vivid 30` |  `0.420`   | `hsl(197, 67%, 61%)` / `#59B9DE`  | `--blue-cool-vivid-30` |
| `Blue Cool Vivid 20` |  `0.596`   | `hsl(196, 66%, 75%)` / `#97D4EA`  | `--blue-cool-vivid-20` |
| `Blue Cool Vivid 10` |  `0.779`   | `hsl(196, 85%, 87%)` / `#C3EBFA`  | `--blue-cool-vivid-10` |
| `Blue Cool Vivid 5`  |  `0.868`   | `hsl(193, 62%, 93%)` / `#E1F3F8`  | `--blue-cool-vivid-5`  |

### Blue Warm

|  Nome da Cor   | Luminância |        HSL / Hexadecimal         |      Token       |
| :------------: | :--------: | :------------------------------: | :--------------: |
| `Blue Warm 90` |  `0.008`   | `hsl(220, 24%, 10%)` / `#13171F` | `--blue-warm-90` |
| `Blue Warm 80` |  `0.027`   | `hsl(216, 25%, 19%)` / `#252F3E` | `--blue-warm-80` |
| `Blue Warm 70` |  `0.059`   | `hsl(216, 38%, 30%)` / `#2F4668` | `--blue-warm-70` |
| `Blue Warm 60` |  `0.107`   | `hsl(215, 49%, 40%)` / `#345D96` | `--blue-warm-60` |
| `Blue Warm 50` |  `0.179`   | `hsl(215, 42%, 50%)` / `#4A77B4` | `--blue-warm-50` |
| `Blue Warm 40` |  `0.282`   | `hsl(217, 43%, 61%)` / `#7292C7` | `--blue-warm-40` |
| `Blue Warm 30` |  `0.419`   | `hsl(216, 39%, 71%)` / `#98AFD2` | `--blue-warm-30` |
| `Blue Warm 20` |  `0.649`   | `hsl(216, 49%, 85%)` / `#C5D4EB` | `--blue-warm-20` |
| `Blue Warm 10` |  `0.795`   | `hsl(218, 36%, 91%)` / `#E1E7F1` | `--blue-warm-10` |
| `Blue Warm 5`  |  `0.874`   | `hsl(213, 41%, 95%)` / `#ECF1F7` | `--blue-warm-5`  |

### Blue Warm Vivid

|     Nome da Cor      | Luminância |         HSL / Hexadecimal         |         Token          |
| :------------------: | :--------: | :-------------------------------: | :--------------------: |
| `Blue Warm Vivid 90` |  `0.013`   | `hsl(217, 81%, 14%)` / `#071D41`  | `--blue-warm-vivid-90` |
| `Blue Warm Vivid 80` |  `0.035`   | `hsl(217, 80%, 24%)` / `#0C326F`  | `--blue-warm-vivid-80` |
| `Blue Warm Vivid 70` |  `0.093`   | `hsl(217, 81%, 39%)` / `#1351B4`  | `--blue-warm-vivid-70` |
| `Blue Warm Vivid 60` |  `0.119`   | `hsl(217, 81%, 44%)` / `#155BCB`  | `--blue-warm-vivid-60` |
| `Blue Warm Vivid 50` |  `0.178`   | `hsl(217, 81%, 53%)` / `#2670E8`  | `--blue-warm-vivid-50` |
| `Blue Warm Vivid 40` |  `0.287`   | `hsl(217, 80%, 64%)` / `#5992ED`  | `--blue-warm-vivid-40` |
| `Blue Warm Vivid 30` |  `0.419`   | `hsl(218, 95%, 75%)` / `#81AEFC`  | `--blue-warm-vivid-30` |
| `Blue Warm Vivid 20` |  `0.597`   | `hsl(217, 100%, 84%)` / `#ADCDFF` | `--blue-warm-vivid-20` |
| `Blue Warm Vivid 10` |  `0.772`   | `hsl(216, 100%, 92%)` / `#D4E5FF` | `--blue-warm-vivid-10` |
| `Blue Warm Vivid 5`  |  `0.905`   | `hsl(213, 100%, 96%)` / `#EDF5FF` | `--blue-warm-vivid-5`  |

### Indigo

| Nome da Cor | Luminância |        HSL / Hexadecimal         |     Token     |
| :---------: | :--------: | :------------------------------: | :-----------: |
| `Indigo 90` |  `0.008`   | `hsl(233, 17%, 10%)` / `#16171F` | `--indigo-90` |
| `Indigo 80` |  `0.026`   | `hsl(237, 20%, 21%)` / `#2B2C40` | `--indigo-80` |
| `Indigo 70` |  `0.059`   | `hsl(237, 32%, 35%)` / `#3D4076` | `--indigo-70` |
| `Indigo 60` |  `0.107`   | `hsl(237, 39%, 49%)` / `#4D52AF` | `--indigo-60` |
| `Indigo 50` |  `0.177`   | `hsl(237, 47%, 59%)` / `#676CC8` | `--indigo-50` |
| `Indigo 40` |  `0.282`   | `hsl(239, 54%, 70%)` / `#8889DB` | `--indigo-40` |
| `Indigo 30` |  `0.420`   | `hsl(237, 64%, 78%)` / `#A5A8EB` | `--indigo-30` |
| `Indigo 20` |  `0.582`   | `hsl(240, 66%, 86%)` / `#C5C5F3` | `--indigo-20` |
| `Indigo 10` |  `0.790`   | `hsl(243, 69%, 94%)` / `#E5E4FA` | `--indigo-10` |
| `Indigo 5`  |  `0.868`   | `hsl(240, 39%, 95%)` / `#EFEFF8` | `--indigo-5`  |

### Indigo Vivid

|    Nome da Cor    | Luminância |         HSL / Hexadecimal         |        Token        |
| :---------------: | :--------: | :-------------------------------: | :-----------------: |
| `Indigo Vivid 80` |  `0.024`   | `hsl(237, 50%, 26%)` / `#212463`  | `--indigo-vivid-80` |
| `Indigo Vivid 70` |  `0.057`   | `hsl(240, 52%, 42%)` / `#3333A3`  | `--indigo-vivid-70` |
| `Indigo Vivid 60` |  `0.111`   | `hsl(237, 51%, 53%)` / `#4A50C4`  | `--indigo-vivid-60` |
| `Indigo Vivid 50` |  `0.181`   | `hsl(237, 59%, 62%)` / `#656BD7`  | `--indigo-vivid-50` |
| `Indigo Vivid 40` |  `0.298`   | `hsl(237, 100%, 75%)` / `#8289FF` | `--indigo-vivid-40` |
| `Indigo Vivid 30` |  `0.423`   | `hsl(237, 90%, 81%)` / `#A3A7FA`  | `--indigo-vivid-30` |
| `Indigo Vivid 20` |  `0.641`   | `hsl(238, 100%, 90%)` / `#CCCEFF` | `--indigo-vivid-20` |
| `Indigo Vivid 10` |  `0.763`   | `hsl(240, 100%, 94%)` / `#E0E0FF` | `--indigo-vivid-10` |
| `Indigo Vivid 5`  |  `0.880`   | `hsl(240, 100%, 97%)` / `#F0F0FF` | `--indigo-vivid-5`  |

### Indigo Cool

|   Nome da Cor    | Luminância |        HSL / Hexadecimal         |       Token        |
| :--------------: | :--------: | :------------------------------: | :----------------: |
| `Indigo Cool 90` |  `0.008`   | `hsl(235, 24%, 11%)` / `#151622` | `--indigo-cool-90` |
| `Indigo Cool 80` |  `0.027`   | `hsl(230, 23%, 21%)` / `#292D42` | `--indigo-cool-80` |
| `Indigo Cool 70` |  `0.059`   | `hsl(229, 36%, 34%)` / `#374274` | `--indigo-cool-70` |
| `Indigo Cool 60` |  `0.106`   | `hsl(226, 45%, 45%)` / `#3F57A6` | `--indigo-cool-60` |
| `Indigo Cool 50` |  `0.177`   | `hsl(224, 65%, 57%)` / `#496FD8` | `--indigo-cool-50` |
| `Indigo Cool 40` |  `0.282`   | `hsl(223, 73%, 66%)` / `#6B8EE8` | `--indigo-cool-40` |
| `Indigo Cool 30` |  `0.417`   | `hsl(226, 72%, 76%)` / `#96ABEE` | `--indigo-cool-30` |
| `Indigo Cool 20` |  `0.584`   | `hsl(227, 74%, 85%)` / `#BBC8F5` | `--indigo-cool-20` |
| `Indigo Cool 10` |  `0.794`   | `hsl(227, 67%, 93%)` / `#E1E6F9` | `--indigo-cool-10` |
| `Indigo Cool 5`  |  `0.873`   | `hsl(229, 48%, 95%)` / `#EEF0F9` | `--indigo-cool-5`  |

### Indigo Cool Vivid

|      Nome da Cor       | Luminância |         HSL / Hexadecimal         |          Token           |
| :--------------------: | :--------: | :-------------------------------: | :----------------------: |
| `Indigo Cool Vivid 80` |  `0.036`   | `hsl(231, 66%, 31%)` / `#1B2B85`  | `--indigo-cool-vivid-80` |
| `Indigo Cool Vivid 70` |  `0.061`   | `hsl(235, 70%, 44%)` / `#222FBF`  | `--indigo-cool-vivid-70` |
| `Indigo Cool Vivid 60` |  `0.124`   | `hsl(235, 83%, 59%)` / `#3E4DED`  | `--indigo-cool-vivid-60` |
| `Indigo Cool Vivid 50` |  `0.181`   | `hsl(230, 100%, 64%)` / `#4866FF` | `--indigo-cool-vivid-50` |
| `Indigo Cool Vivid 40` |  `0.284`   | `hsl(222, 87%, 67%)` / `#628EF4`  | `--indigo-cool-vivid-40` |
| `Indigo Cool Vivid 30` |  `0.434`   | `hsl(226, 100%, 79%)` / `#94ADFF` | `--indigo-cool-vivid-30` |
| `Indigo Cool Vivid 20` |  `0.587`   | `hsl(226, 100%, 86%)` / `#B8C8FF` | `--indigo-cool-vivid-20` |
| `Indigo Cool Vivid 10` |  `0.787`   | `hsl(227, 100%, 94%)` / `#DEE5FF` | `--indigo-cool-vivid-10` |
| `Indigo Cool Vivid 5`  |  `0.875`   | `hsl(230, 100%, 96%)` / `#EDF0FF` | `--indigo-cool-vivid-5`  |

### Indigo Warm

|   Nome da Cor    | Luminância |        HSL / Hexadecimal         |       Token        |
| :--------------: | :--------: | :------------------------------: | :----------------: |
| `Indigo Warm 90` |  `0.008`   | `hsl(257, 14%, 10%)` / `#18161D` | `--indigo-warm-90` |
| `Indigo Warm 80` |  `0.027`   | `hsl(246, 19%, 21%)` / `#2E2C40` | `--indigo-warm-80` |
| `Indigo Warm 70` |  `0.059`   | `hsl(249, 34%, 36%)` / `#453C7B` | `--indigo-warm-70` |
| `Indigo Warm 60` |  `0.107`   | `hsl(250, 32%, 47%)` / `#5E519E` | `--indigo-warm-60` |
| `Indigo Warm 50` |  `0.177`   | `hsl(249, 54%, 61%)` / `#7665D1` | `--indigo-warm-50` |
| `Indigo Warm 40` |  `0.283`   | `hsl(248, 51%, 69%)` / `#9287D8` | `--indigo-warm-40` |
| `Indigo Warm 30` |  `0.418`   | `hsl(249, 59%, 78%)` / `#AFA5E8` | `--indigo-warm-30` |
| `Indigo Warm 20` |  `0.585`   | `hsl(249, 64%, 86%)` / `#CBC4F2` | `--indigo-warm-20` |
| `Indigo Warm 10` |  `0.788`   | `hsl(250, 70%, 94%)` / `#E7E3FA` | `--indigo-warm-10` |
| `Indigo Warm 5`  |  `0.871`   | `hsl(255, 33%, 95%)` / `#F1EFF7` | `--indigo-warm-5`  |

### Indigo Warm Vivid

|      Nome da Cor       | Luminância |         HSL / Hexadecimal         |          Token           |
| :--------------------: | :--------: | :-------------------------------: | :----------------------: |
| `Indigo Warm Vivid 80` |  `0.021`   | `hsl(247, 49%, 24%)` / `#261F5B`  | `--indigo-warm-vivid-80` |
| `Indigo Warm Vivid 70` |  `0.052`   | `hsl(249, 56%, 39%)` / `#3D2C9D`  | `--indigo-warm-vivid-70` |
| `Indigo Warm Vivid 60` |  `0.106`   | `hsl(250, 62%, 54%)` / `#5942D2`  | `--indigo-warm-vivid-60` |
| `Indigo Warm Vivid 50` |  `0.177`   | `hsl(249, 76%, 64%)` / `#745FE9`  | `--indigo-warm-vivid-50` |
| `Indigo Warm Vivid 40` |  `0.283`   | `hsl(252, 94%, 74%)` / `#967EFB`  | `--indigo-warm-vivid-40` |
| `Indigo Warm Vivid 30` |  `0.419`   | `hsl(254, 100%, 81%)` / `#B69FFF` | `--indigo-warm-vivid-30` |
| `Indigo Warm Vivid 20` |  `0.598`   | `hsl(252, 93%, 88%)` / `#CFC4FD`  | `--indigo-warm-vivid-20` |
| `Indigo Warm Vivid 10` |  `0.759`   | `hsl(251, 100%, 94%)` / `#E4DEFF` | `--indigo-warm-vivid-10` |
| `Indigo Warm Vivid 5`  |  `0.901`   | `hsl(254, 100%, 97%)` / `#F5F2FF` | `--indigo-warm-vivid-5`  |

### Violet

| Nome da Cor | Luminância |        HSL / Hexadecimal         |     Token     |
| :---------: | :--------: | :------------------------------: | :-----------: |
| `Violet 90` |  `0.008`   | `hsl(257, 14%, 10%)` / `#18161D` | `--violet-90` |
| `Violet 80` |  `0.027`   | `hsl(258, 19%, 21%)` / `#312B3F` | `--violet-80` |
| `Violet 70` |  `0.058`   | `hsl(260, 27%, 33%)` / `#4C3D69` | `--violet-70` |
| `Violet 60` |  `0.107`   | `hsl(260, 28%, 44%)` / `#665190` | `--violet-60` |
| `Violet 50` |  `0.178`   | `hsl(260, 33%, 55%)` / `#8168B3` | `--violet-50` |
| `Violet 40` |  `0.283`   | `hsl(259, 46%, 67%)` / `#9D84D2` | `--violet-40` |
| `Violet 30` |  `0.415`   | `hsl(260, 54%, 76%)` / `#B8A2E3` | `--violet-30` |
| `Violet 20` |  `0.583`   | `hsl(261, 46%, 84%)` / `#D0C3E9` | `--violet-20` |
| `Violet 10` |  `0.794`   | `hsl(262, 65%, 93%)` / `#EBE3F9` | `--violet-10` |
| `Violet 5`  |  `0.889`   | `hsl(263, 40%, 96%)` / `#F4F1F9` | `--violet-5`  |

### Violet Vivid

|    Nome da Cor    | Luminância |         HSL / Hexadecimal         |        Token        |
| :---------------: | :--------: | :-------------------------------: | :-----------------: |
| `Violet Vivid 80` |  `0.027`   | `hsl(264, 48%, 25%)` / `#39215E`  | `--violet-vivid-80` |
| `Violet Vivid 70` |  `0.053`   | `hsl(266, 57%, 36%)` / `#54278F`  | `--violet-vivid-70` |
| `Violet Vivid 60` |  `0.107`   | `hsl(269, 51%, 48%)` / `#783CB9`  | `--violet-vivid-60` |
| `Violet Vivid 50` |  `0.178`   | `hsl(268, 66%, 60%)` / `#9355DC`  | `--violet-vivid-50` |
| `Violet Vivid 40` |  `0.284`   | `hsl(268, 72%, 69%)` / `#AD79E9`  | `--violet-vivid-40` |
| `Violet Vivid 30` |  `0.417`   | `hsl(269, 66%, 77%)` / `#C39DEB`  | `--violet-vivid-30` |
| `Violet Vivid 20` |  `0.586`   | `hsl(261, 100%, 87%)` / `#D5BFFF` | `--violet-vivid-20` |
| `Violet Vivid 10` |  `0.801`   | `hsl(261, 100%, 95%)` / `#EDE3FF` | `--violet-vivid-10` |
| `Violet Vivid 5`  |  `0.904`   | `hsl(263, 100%, 97%)` / `#F7F2FF` | `--violet-vivid-5`  |

### Violet Warm

|   Nome da Cor    | Luminância |        HSL / Hexadecimal         |       Token        |
| :--------------: | :--------: | :------------------------------: | :----------------: |
| `Violet Warm 90` |  `0.008`   | `hsl(300, 13%, 9%)` / `#1B151B`  | `--violet-warm-90` |
| `Violet Warm 80` |  `0.026`   | `hsl(308, 15%, 19%)` / `#382936` | `--violet-warm-80` |
| `Violet Warm 70` |  `0.059`   | `hsl(303, 23%, 29%)` / `#5C395A` | `--violet-warm-70` |
| `Violet Warm 60` |  `0.106`   | `hsl(304, 33%, 39%)` / `#864381` | `--violet-warm-60` |
| `Violet Warm 50` |  `0.178`   | `hsl(293, 47%, 52%)` / `#B04ABD` | `--violet-warm-50` |
| `Violet Warm 40` |  `0.284`   | `hsl(293, 42%, 63%)` / `#BF77C8` | `--violet-warm-40` |
| `Violet Warm 30` |  `0.417`   | `hsl(294, 44%, 73%)` / `#D29AD8` | `--violet-warm-30` |
| `Violet Warm 20` |  `0.585`   | `hsl(297, 41%, 82%)` / `#E2BEE4` | `--violet-warm-20` |
| `Violet Warm 10` |  `0.791`   | `hsl(295, 64%, 92%)` / `#F6DFF8` | `--violet-warm-10` |
| `Violet Warm 5`  |  `0.891`   | `hsl(293, 43%, 96%)` / `#F8F0F9` | `--violet-warm-5`  |

### Violet Warm Vivid

|      Nome da Cor       | Luminância |         HSL / Hexadecimal         |          Token           |
| :--------------------: | :--------: | :-------------------------------: | :----------------------: |
| `Violet Warm Vivid 80` |  `0.022`   | `hsl(308, 57%, 18%)` / `#481441`  | `--violet-warm-vivid-80` |
| `Violet Warm Vivid 70` |  `0.055`   | `hsl(304, 58%, 28%)` / `#711E6C`  | `--violet-warm-vivid-70` |
| `Violet Warm Vivid 60` |  `0.105`   | `hsl(304, 48%, 39%)` / `#93348C`  | `--violet-warm-vivid-60` |
| `Violet Warm Vivid 50` |  `0.177`   | `hsl(293, 63%, 51%)` / `#BE32D0`  | `--violet-warm-vivid-50` |
| `Violet Warm Vivid 40` |  `0.283`   | `hsl(291, 82%, 65%)` / `#D85BEF`  | `--violet-warm-vivid-40` |
| `Violet Warm Vivid 30` |  `0.416`   | `hsl(292, 100%, 76%)` / `#EE83FF` | `--violet-warm-vivid-30` |
| `Violet Warm Vivid 20` |  `0.582`   | `hsl(291, 100%, 85%)` / `#F4B2FF` | `--violet-warm-vivid-20` |
| `Violet Warm Vivid 10` |  `0.789`   | `hsl(293, 100%, 93%)` / `#FBDCFF` | `--violet-warm-vivid-10` |
| `Violet Warm Vivid 5`  |  `0.917`   | `hsl(295, 100%, 97%)` / `#FEF2FF` | `--violet-warm-vivid-5`  |

### Magenta

| Nome da Cor  | Luminância |        HSL / Hexadecimal         |     Token      |
| :----------: | :--------: | :------------------------------: | :------------: |
| `Magenta 90` |  `0.008`   | `hsl(348, 10%, 10%)` / `#1B1617` | `--magenta-90` |
| `Magenta 80` |  `0.027`   | `hsl(336, 24%, 20%)` / `#402731` | `--magenta-80` |
| `Magenta 70` |  `0.059`   | `hsl(334, 31%, 31%)` / `#66364B` | `--magenta-70` |
| `Magenta 60` |  `0.107`   | `hsl(332, 34%, 41%)` / `#8B4566` | `--magenta-60` |
| `Magenta 50` |  `0.177`   | `hsl(332, 55%, 52%)` / `#C84281` | `--magenta-50` |
| `Magenta 40` |  `0.284`   | `hsl(333, 66%, 65%)` / `#E0699F` | `--magenta-40` |
| `Magenta 30` |  `0.419`   | `hsl(338, 64%, 75%)` / `#E895B3` | `--magenta-30` |
| `Magenta 20` |  `0.584`   | `hsl(341, 64%, 84%)` / `#F0BBCC` | `--magenta-20` |
| `Magenta 10` |  `0.792`   | `hsl(340, 54%, 92%)` / `#F6E1E8` | `--magenta-10` |
| `Magenta 5`  |  `0.888`   | `hsl(347, 43%, 96%)` / `#F9F0F2` | `--magenta-5`  |

### Magenta Vivid

|    Nome da Cor     | Luminância |         HSL / Hexadecimal         |        Token         |
| :----------------: | :--------: | :-------------------------------: | :------------------: |
| `Magenta Vivid 80` |  `0.024`   | `hsl(335, 55%, 20%)` / `#4F172E`  | `--magenta-vivid-80` |
| `Magenta Vivid 70` |  `0.050`   | `hsl(334, 58%, 29%)` / `#731F44`  | `--magenta-vivid-70` |
| `Magenta Vivid 60` |  `0.106`   | `hsl(330, 68%, 40%)` / `#AB2165`  | `--magenta-vivid-60` |
| `Magenta Vivid 50` |  `0.177`   | `hsl(333, 68%, 51%)` / `#D72D79`  | `--magenta-vivid-50` |
| `Magenta Vivid 40` |  `0.272`   | `hsl(333, 98%, 63%)` / `#FD4496`  | `--magenta-vivid-40` |
| `Magenta Vivid 30` |  `0.418`   | `hsl(339, 100%, 76%)` / `#FF87B2` | `--magenta-vivid-30` |
| `Magenta Vivid 20` |  `0.584`   | `hsl(338, 100%, 85%)` / `#FFB4CF` | `--magenta-vivid-20` |
| `Magenta Vivid 10` |  `0.789`   | `hsl(337, 100%, 93%)` / `#FFDDEA` | `--magenta-vivid-10` |
| `Magenta Vivid 5`  |  `0.913`   | `hsl(346, 100%, 97%)` / `#FFF2F5` | `--magenta-vivid-5`  |

### Gray

| Nome da Cor | Luminância |       HSL / Hexadecimal       |    Token    |
| :---------: | :--------: | :---------------------------: | :---------: |
|  `Gray 90`  |  `0.010`   | `hsl(0, 0%, 11%)` / `#1B1B1B` | `--gray-90` |
|  `Gray 80`  |  `0.033`   | `hsl(0, 0%, 20%)` / `#333333` | `--gray-80` |
|  `Gray 70`  |  `0.090`   | `hsl(0, 0%, 33%)` / `#555555` | `--gray-70` |
|  `Gray 60`  |  `0.124`   | `hsl(0, 0%, 39%)` / `#636363` | `--gray-60` |
|  `Gray 50`  |  `0.177`   | `hsl(0, 0%, 46%)` / `#757575` | `--gray-50` |
|  `Gray 40`  |  `0.246`   | `hsl(0, 0%, 53%)` / `#888888` | `--gray-40` |
|  `Gray 30`  |  `0.417`   | `hsl(0, 0%, 68%)` / `#ADADAD` | `--gray-30` |
|  `Gray 20`  |  `0.603`   | `hsl(0, 0%, 80%)` / `#CCCCCC` | `--gray-20` |
|  `Gray 10`  |  `0.791`   | `hsl(0, 0%, 90%)` / `#E6E6E6` | `--gray-10` |
|  `Gray 5`   |  `0.871`   | `hsl(0, 0%, 94%)` / `#F0F0F0` | `--gray-5`  |
|  `Gray 4`   |  `0.896`   | `hsl(0, 0%, 95%)` / `#F3F3F3` | `--gray-4`  |
|  `Gray 3`   |  `0.921`   | `hsl(0, 0%, 96%)` / `#F6F6F6` | `--gray-3`  |
|  `Gray 2`   |  `0.938`   | `hsl(0, 0%, 97%)` / `#F8F8F8` | `--gray-2`  |
|  `Gray 1`   |  `0.973`   | `hsl(0, 0%, 99%)` / `#FCFCFC` | `--gray-1`  |

### Gray Cool

|  Nome da Cor   | Luminância |        HSL / Hexadecimal         |      Token       |
| :------------: | :--------: | :------------------------------: | :--------------: |
| `Gray Cool 90` |  `0.012`   | `hsl(220, 5%, 12%)` / `#1C1D1F`  | `--gray-cool-90` |
| `Gray Cool 80` |  `0.027`   | `hsl(210, 2%, 18%)` / `#2D2E2F`  | `--gray-cool-80` |
| `Gray Cool 70` |  `0.058`   | `hsl(216, 14%, 28%)` / `#3D4551` | `--gray-cool-70` |
| `Gray Cool 60` |  `0.105`   | `hsl(216, 8%, 37%)` / `#565C65`  | `--gray-cool-60` |
| `Gray Cool 50` |  `0.178`   | `hsl(207, 4%, 46%)` / `#71767A`  | `--gray-cool-50` |
| `Gray Cool 40` |  `0.284`   | `hsl(210, 5%, 57%)` / `#8D9297`  | `--gray-cool-40` |
| `Gray Cool 30` |  `0.418`   | `hsl(202, 5%, 68%)` / `#A9AEB1`  | `--gray-cool-30` |
| `Gray Cool 20` |  `0.587`   | `hsl(210, 8%, 79%)` / `#C6CACE`  | `--gray-cool-20` |
| `Gray Cool 10` |  `0.750`   | `hsl(200, 5%, 88%)` / `#DFE1E2`  | `--gray-cool-10` |
| `Gray Cool 5`  |  `0.860`   | `hsl(200, 9%, 94%)` / `#EDEFF0`  | `--gray-cool-5`  |
| `Gray Cool 4`  |  `0.894`   | `hsl(216, 22%, 95%)` / `#F1F3F6` | `--gray-cool-4`  |
| `Gray Cool 3`  |  `0.920`   | `hsl(210, 11%, 96%)` / `#F5F6F7` | `--gray-cool-3`  |
| `Gray Cool 2`  |  `0.944`   | `hsl(200, 23%, 97%)` / `#F7F9FA` | `--gray-cool-2`  |
| `Gray Cool 1`  |  `0.972`   | `hsl(210, 33%, 99%)` / `#FBFCFD` | `--gray-cool-1`  |

### Gray Warm

|  Nome da Cor   | Luminância |        HSL / Hexadecimal        |      Token       |
| :------------: | :--------: | :-----------------------------: | :--------------: |
| `Gray Warm 90` |  `0.008`   |  `hsl(60, 2%, 9%)` / `#171716`  | `--gray-warm-90` |
| `Gray Warm 80` |  `0.027`   | `hsl(60, 5%, 17%)` / `#2E2E2A`  | `--gray-warm-80` |
| `Gray Warm 70` |  `0.058`   | `hsl(60, 4%, 26%)` / `#454540`  | `--gray-warm-70` |
| `Gray Warm 60` |  `0.107`   | `hsl(60, 6%, 34%)` / `#5D5D52`  | `--gray-warm-60` |
| `Gray Warm 50` |  `0.178`   | `hsl(60, 5%, 44%)` / `#76766A`  | `--gray-warm-50` |
| `Gray Warm 40` |  `0.283`   | `hsl(60, 6%, 55%)` / `#929285`  | `--gray-warm-40` |
| `Gray Warm 30` |  `0.419`   | `hsl(55, 8%, 66%)` / `#AFAEA2`  | `--gray-warm-30` |
| `Gray Warm 20` |  `0.581`   | `hsl(54, 9%, 77%)` / `#CAC9C0`  | `--gray-warm-20` |
| `Gray Warm 10` |  `0.789`   | `hsl(60, 7%, 89%)` / `#E6E6E2`  | `--gray-warm-10` |
| `Gray Warm 5`  |  `0.869`   | `hsl(60, 12%, 93%)` / `#F0F0EC` | `--gray-warm-5`  |
| `Gray Warm 4`  |  `0.910`   | `hsl(60, 20%, 95%)` / `#F5F5F0` | `--gray-warm-4`  |
| `Gray Warm 3`  |  `0.919`   | `hsl(60, 18%, 96%)` / `#F6F6F2` | `--gray-warm-3`  |
| `Gray Warm 2`  |  `0.946`   | `hsl(60, 14%, 97%)` / `#F9F9F7` | `--gray-warm-2`  |
| `Gray Warm 1`  |  `0.972`   | `hsl(60, 14%, 99%)` / `#FCFCFB` | `--gray-warm-1`  |

### Pure

| Nome da Cor | Luminância |       HSL / Hexadecimal        |    Token     |
| :---------: | :--------: | :----------------------------: | :----------: |
| `Pure 100`  |  `0.000`   |  `hsl(0, 0%, 0%)` / `#000000`  | `--pure-100` |
|  `Pure 0`   |  `1.000`   | `hsl(0, 0%, 100%)` / `#FFFFFF` |  `--pure-0`  |

---

## Espaçamento

### Alinhamento

#### Vertical

- `top`
- `center`
- `bottom`

#### Horizontal

- `left`
- `center`
- `right`

### Escala

| em  | px  |    Spacing Scale Token    |
| --- | --- | :-----------------------: |
| 0   | 0   | `--spacing-scale-default` |

#### Layout

| em  | px  |  Spacing Scale Token   |
| --- | --- | :--------------------: |
| 1   | 8   | `--spacing-scale-base` |
| 2   | 16  |  `--spacing-scale-2x`  |
| 3   | 24  |  `--spacing-scale-3x`  |
| 4   | 32  |  `--spacing-scale-4x`  |
| 5   | 40  |  `--spacing-scale-5x`  |
| 6   | 48  |  `--spacing-scale-6x`  |
| 7   | 56  |  `--spacing-scale-7x`  |
| 8   | 64  |  `--spacing-scale-8x`  |
| 9   | 72  |  `--spacing-scale-9x`  |
| 10  | 80  | `--spacing-scale-10x`  |

#### Ajuste

| em  | px  |   Spacing Scale Token   |
| --- | --- | :---------------------: |
| 0.5 | 4   | `--spacing-scale-half`  |
| 1.5 | 12  | `--spacing-scale-baseh` |
| 2.5 | 20  |  `--spacing-scale-2xh`  |
| 3.5 | 28  |  `--spacing-scale-3xh`  |
| 4.5 | 36  |  `--spacing-scale-4xh`  |
| 5.5 | 44  |  `--spacing-scale-5xh`  |

---

## Superfície

### Borda

#### Style

- `solid`
- `dashed`

#### Width

| px  |      Width Token       |
| --- | :--------------------: |
| 0   | `--surface-width-none` |
| 1   |  `--surface-width-sm`  |
| 2   |  `--surface-width-md`  |
| 4   |  `--surface-width-lg`  |

#### Side

- `top`
- `right`
- `bottom`
- `left`
- `all`

### Tokens de borda

|          Border Token          |  Style   |         Width          |    Color    |
| :----------------------------: | :------: | :--------------------: | :---------: |
| `--surface-border-solid-none`  | `solid`  | `--surface-width-none` | `--gray-40` |
|  `--surface-border-solid-sm`   | `solid`  |  `--surface-width-sm`  | `--gray-40` |
|  `--surface-border-solid-md`   | `solid`  |  `--surface-width-md`  | `--gray-40` |
|  `--surface-border-solid-lg`   | `solid`  |  `--surface-width-lg`  | `--gray-40` |
| `--surface-border-dashed-none` | `dashed` | `--surface-width-none` | `--gray-40` |
|  `--surface-border-dashed-sm`  | `dashed` |  `--surface-width-sm`  | `--gray-40` |
|  `--surface-border-dashed-md`  | `dashed` |  `--surface-width-md`  | `--gray-40` |
|  `--surface-border-dashed-lg`  | `dashed` |  `--surface-width-lg`  | `--gray-40` |

### Cantos

#### Rounder

| px       |      Rounder Token       |
| -------- | :----------------------: |
| 0        | `--surface-rounder-none` |
| 4        |  `--surface-rounder-sm`  |
| 8        |  `--surface-rounder-md`  |
| 16       |  `--surface-rounder-lg`  |
| altura/2 | `--surface-rounder-pill` |

#### Side

- `top/right`
- `bottom/right`
- `bottom/left`
- `bottom/right`
- `all`

### Opacidade

#### Opacity

| Value |        Opacity Token        |
| ----- | :-------------------------: |
| 0     |  `--surface-opacity-none`   |
| 1     | `--surface-opacity-default` |
| 0.16  |   `--surface-opacity-xs`    |
| 0.30  |   `--surface-opacity-sm`    |
| 0.45  |   `--surface-opacity-md`    |
| 0.65  |   `--surface-opacity-lg`    |
| 0.85  |   `--surface-opacity-xl`    |

### Overlay

#### Foco

| Overlay Token             |    Color     |     Color Opacity      |
| ------------------------- | :----------: | :--------------------: |
| `--surface-overlay-scrim` | `--pure-100` | `--surface-opacity-md` |

#### Legibilidade

|      Overlay Token       |     Gradient      |  Angle   |   Color    |      Color Opacity       | Hint |    Color     |        Color Opacity        |  Hint  |
| :----------------------: | :---------------: | :------: | :--------: | :----------------------: | :--: | :----------: | :-------------------------: | :----: |
| `--surface-overlay-text` | `linear-gradient` | `180deg` | `--pure-0` | `--surface-opacity-none` | `0%` | `--pure-100` | `--surface-opacity-default` | `100%` |

---

## Elevação

### Inner Shadow (Sombra Interna)

- `inset`
- `none`

### Offset (Projeção)

| Offset (px) |      Offset Token       | Camada |     Layer Token     |
| :---------: | :---------------------: | :----: | :-----------------: |
|     -9      | `--surface-offset-xl-n` |   4    | `--z-index-layer-4` |
|     -6      | `--surface-offset-lg-n` |   3    | `--z-index-layer-3` |
|     -3      | `--surface-offset-md-n` |   2    | `--z-index-layer-2` |
|     -1      | `--surface-offset-sm-n` |   1    | `--z-index-layer-1` |
|      0      | `--surface-offset-none` |   0    | `--z-index-layer-0` |
|      1      |  `--surface-offset-sm`  |   1    | `--z-index-layer-1` |
|      3      |  `--surface-offset-md`  |   2    | `--z-index-layer-2` |
|      6      |  `--surface-offset-lg`  |   3    | `--z-index-layer-3` |
|      9      |  `--surface-offset-xl`  |   4    | `--z-index-layer-4` |

### Blur (Suavidade)

| Blur (px) |      Blur Token       |
| :-------: | :-------------------: |
|     0     | `--surface-blur-none` |
|     1     |  `--surface-blur-sm`  |
|     3     |  `--surface-blur-md`  |
|     6     |  `--surface-blur-lg`  |
|     9     |  `--surface-blur-xl`  |

---

## Movimento

### Transição

- Opacity (transparência)
- Fade In
- Fade Out
- Color (cor)
- Scale (dimensão)
- Fill (preenchimento)
- Slide (Posição)
- Elevation (elevação)
- Rotate (rotação)
- Corner (arredondamento)
- Form (transformação)

### Easing

| *Easing*      | CSS                               | Easing Token              |
| ------------- | --------------------------------- | ------------------------- |
| *Ease*        | cubic-bezier (0.25, 0.1, 0.25, 1) | `--animation-ease`        |
| *Ease-In*     | cubic-bezier 0.42, 0, 1, 1)       | `--animation-ease-in`     |
| *Ease-Out*    | cubic-bezier (0, 0, 0.58, 1)      | `--animation-ease-out`    |
| *Ease-In-Out* | cubic-bezier (0.42, 0, 0.58, 1)   | `--animation-ease-in-out` |
| Linear        | cubic-bezier (0,0,1,1)            | `--animation-ease-linear` |

### Escala de Tempo

| Tempo (s)   | Tipo         | Área           |
| ----------- | ------------ | -------------- |
| 0,10 - 0,29 | Muito rápido | Micro animação |
| 0,30 - 0,49 | Rápido       | Micro animação |
| 0,50 - 0,79 | Moderado     | Média animação |
| 0,80 - 0,99 | Lento        | Macro animação |
| 1 - \*      | Muito lento  | Macro animação |

### Duração

| Tempo (s) | Tipo         | Duration Token         |
| --------- | ------------ | ---------------------- |
| 0,10      | Muito rápido | `--duration-very-fast` |
| 0,30      | Rápido       | `--duration-fast`      |
| 0,50      | Moderado     | `--duration-moderate`  |
| 0,80      | Lento        | `--duration-slow`      |
| 1         | Muito lento  | `--duration-very-slow` |

---

## Especificação (Design Tokens)

### Outras informacoes

- Caso queira detalhar um elemento referente a um comportamento ou tipo, coloque o nome do tipo ou do comportamento entre parenteses ao lado do nome do elemento. Podendo separar os subníveis por barra "/". Ex: Container (Responsividade / 4 Colunas), Container (Primary/Densidade Alta).
- Caso a dimensão seja variavel (varia de acordo com conteudo) coloque o valor "auto".
- Se alguma celular da tabela existir e nao tem necessidade de preenchimento, coloque um traco "-" para mostrar que nao existe um valor ou nao faz sentido naquele contexto a informação.

### Tipografia

| Name                         | Property    |                    Token/Value                    |
| ---------------------------- | ----------- | :-----------------------------------------------: |
| Nome do Elemento da Anatomia | family      |      Family Token. Ex: `–-font-family-base`       |
| Nome do Elemento da Anatomia | font-weight |      Weight Token. Ex: `–-font-weight-thin`       |
| Nome do Elemento da Anatomia | size        |     Scale Token. Ex: `––font-size-scale-base`     |
| Nome do Elemento da Anatomia | line-height | Line-Height Token. Ex: `-–font-lineheight-medium` |

---

### Iconografia

| Name                      |            Ícone            |               Size                |      Class (Font Awesome)      |
| ------------------------- | :-------------------------: | :-------------------------------: | :----------------------------: |
| Nome do Ícone da Anatomia | <i class="fas fa-user"></i> | Scale Token. Ex: `--icon-size-sm` | Classe do Ícone. Ex: `fa-user` |

---

### Cores

| Name                         | Property   |           Token/Value            |
| ---------------------------- | ---------- | :------------------------------: |
| Nome do Elemento da Anatomia | text/icon  | Color Token. Ex: `--red-cool-90` |
| Nome do Elemento da Anatomia | background |  Color Token. Ex:`--orange-60`   |
| Nome do Elemento da Anatomia | overlay    |   Color Token. Ex:`--green-90`   |

---

### Espaçamento

#### Dimensão

| Name                         | Property |        Token/Value        |
| ---------------------------- | -------- | :-----------------------: |
| Nome do Elemento da Anatomia | height   | Valor+Unidade. Ex: `40px` |
| Nome do Elemento da Anatomia | width    | Valor+Unidade. Ex: `24px` |

#### Alinhamento

| Name                         | Property   |    Token/Value     |
| ---------------------------- | ---------- | :----------------: |
| Nome do Elemento da Anatomia | vertical   |  Valor. Ex: `top`  |
| Nome do Elemento da Anatomia | horizontal | Valor. Ex: `right` |

#### Escala

| Name                         | Type    |                   Top                   |                  Right                  |                 Bottom                  |                  Left                   |
| ---------------------------- | ------- | :-------------------------------------: | :-------------------------------------: | :-------------------------------------: | :-------------------------------------: |
| Nome do Elemento da Anatomia | Interno | Spacing Token. Ex: `--spacing-scale-5x` | Spacing Token. Ex: `--spacing-scale-5x` | Spacing Token. Ex: `--spacing-scale-5x` | Spacing Token. Ex: `--spacing-scale-5x` |
| Nome do Elemento da Anatomia | Externo | Spacing Token. Ex: `--spacing-scale-5x` | Spacing Token. Ex: `--spacing-scale-5x` | Spacing Token. Ex: `--spacing-scale-5x` | Spacing Token. Ex: `--spacing-scale-5x` |

---

### Superfície

#### Borda

| Name                         | Width                                   |        Style        |            Side            |
| ---------------------------- | --------------------------------------- | :-----------------: | :------------------------: |
| Nome do Elemento da Anatomia | Border Tokens. Ex: `--surface-width-sm` | Valor. Ex: `solid`  | Valor. Ex: `top`, `bottom` |
| Nome do Elemento da Anatomia | Border Tokens. Ex: `--surface-width-md` | Valor. Ex: `dashed` |      Valor. Ex: `all`      |

#### Cantos

| Name                         |             Top/Right              |           Bottom/Right           |           Bottom/Left            |            Bottom/Right            |
| ---------------------------- | :--------------------------------: | :------------------------------: | :------------------------------: | :--------------------------------: |
| Nome do Elemento da Anatomia | Token Ex: `--surface-rounder-none` | Token Ex: `--surface-rounder-sm` | Token Ex: `--surface-rounder-sm` | Token Ex: `--surface-rounder-none` |
| Nome do Elemento da Anatomia |  Token Ex: `--surface-rounder-sm`  | Token Ex: `--surface-rounder-sm` | Token Ex: `--surface-rounder-sm` |  Token Ex: `--surface-rounder-sm`  |

#### Opacidade

| Name                         |                     Token                     |
| ---------------------------- | :-------------------------------------------: |
| Nome do Elemento da Anatomia | Opacity Token Ex: `--surface-opacity-default` |
| Nome do Elemento da Anatomia |   Opacity Token Ex: `--surface-opacity-md`    |

---

### Elevação

| Name                         |   Inner Shadow    |               Offset (X)               |               Offset (Y)               |                Blur                |               Camada                |
| ---------------------------- | :---------------: | :------------------------------------: | :------------------------------------: | :--------------------------------: | :---------------------------------: |
| Nome do Elemento da Anatomia | Valor Ex: `inset` | Offset Token Ex: `--surface-offset-md` | Offset Token Ex: `--surface-offset-md` | Blur Token Ex: `--surface-blur-md` | Layer Token Ex: `--z-index-layer-2` |
| Nome do Elemento da Anatomia | Valor Ex: `inset` | Offset Token Ex: `--surface-offset-xl` | Offset Token Ex: `--surface-offset-md` | Blur Token Ex: `--surface-blur-xl` | Layer Token Ex: `--z-index-layer-4` |
