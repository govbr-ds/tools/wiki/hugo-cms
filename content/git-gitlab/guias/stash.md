---
title: Stash
description: Lista de comandos úteis do git stash
---

O stash é útil para armazenar as mudanças sem precisar fazer o commit. O conteúdo fica em uma área reservada e **somente na sua máquina local**.

## Comandos

- Salva as mudanças com uma mensagem (opcional):

```git
git stash push -m "mensagem"
```

- Lista todos os stashes:

```git
git stash list
```

- Aplica as mudanças do stash especificado e não o remove:

```git
git stash apply stash@{0}
```

- Aplica as mudanças do stash mais recente e o remove:

```git
git stash pop
```

- Remove o stash especificado:

```git
git stash drop stash@{0}
```

- Remove todos os stashes:

```git
git stash clear
```

- Cria uma nova branch e aplica as mudanças do stash mais recente nessa nova branch:

```git
git stash branch nova_branch
```

- Mostra as mudanças do stash especificado:

```git
git stash show -p stash@{0}
```
