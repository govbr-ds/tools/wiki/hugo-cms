---
title: Versionamento Semântico
description: Descreve como funciona o versionamento semântico
---

Ao definir a versão do nosso software usamo o chamado versionametno semântico.

Você pode ler mais sobre o assunto [nesse link](https://semver.org/lang/pt-BR/ 'SemVer'), mas vamos fazer um pequeno resumo abaixo para simplificar.

## Versões estáveis

As versões estáveis são numeradas seguindo sequência de 3 números que seguem o seguinte template: {MAJOR}.{MINOR}.{PATCH}.

- 1.0.0 -> {MAJOR: 1} . {MINOR: 0} . {PATCH: 0}
- 1.4.6 -> {MAJOR: 1} . {MINOR: 4} . {PATCH: 6}
- 2.7.4 -> {MAJOR: 2} . {MINOR: 7} . {PATCH: 4}

Cada número tem um significado muito importante que ajuda aos usuários entender o impacto dessa nova versão para seu projeto.

| MAJOR                                        | MINOR                                                        | PATCH                                      |
| -------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------ |
| alteração que pode quebrar outras aplicações | adiciona funcionalidade compatível com as versões anteriores | correção compatível com versões anteriores |
| **breaking** change                          | nova **funcionalidade**                                      | bug **fix**                                |

## Pre-release

Podemos adicionar outros identificadores às versões. Nesses casos é indiciado que a versão é pré-lançamento e pode ser instável e não satisfazer os requisitos pretendidos.

Alguns identificadores usados são:

- rc
- beta
- alpha

O template usado nesses caso é sempre: 1.0.0-{identificador}.{numeração}.

A numeração é uma sequência numérica em ordem crescente que é acrescida a cada nova versão.

- 1.0.0-alpha.1 -> 1.0.0-alpha.2 -> 1.0.0-alpha.3 -> 1.0.0-alpha.4
- 1.4.6-rc.1 -> 1.4.6-rc.2 -> 1.4.6-rc.3 -> 1.4.6-rc.4
- 2.7.4-beta.1 -> 2.7.4-beta.2 -> 2.7.4-beta.3 -> 2.7.4-beta.4

{{% notice info %}}
Observe os guias sobre as nossas releases para saber quais os identificadores estão sendo usados atualmente no projeto.
{{% /notice %}}
