---
title: MR - Aprovações
description: Guia sobre as aprovações de MR
---

Um MR pode exigir algumas aprovações antes que de ser considerado pronto para ser mesclado. Essas aprovações são configuradas **por projeto** pelos mantenedores e em sua maioria são avaliadas automaticamente pelos pipelines.

Caso uma aprovação seja necessária o MR vai estar no estado "Aguardando aprovação". Nesse estado, os revisor podem analisar as alterações, fazer comentários e decidir se aprovam ou não o MR. Quando um revisor aprova o MR, ele deixa um registro de sua aprovação no GitLab.

## Aprovações Manuais

Algumas validações são manuais, como por exemplo:

- revisão do conteúdo (assets, código ou documentação) para garantir que o conteúdo está de acordo com as necessidades do projeto
- aprovações de formato do MR

Esse tipo sempre vai solicitação **sempre** vai travar o MR até que algum usuário com permissão aprove pois não são validadas através de um pipeline.

## Aprovações automáticas

A maioria das validações são feitas automaticamente pelo gitlab usando as ferramentas de CI/CD (caso não conheça o que é isso, sugerimos que consulte a documentação do gitlab sobre esse assunto).

Quando o pipeline identifica algo que pode ser de interesse, de acordo com as configurações do projeto, o gitlab vai incluir uma solicitação de aprovação no MR. Essa solicitação indica que os membros indicados devem analisar, tomar as decisões pertinentes e por fim aprovar o MR.

## Análise

É preciso fazer uma análise das aprovações solicitadas e seguir um de 3 caminhos:

1. Solicitar ao responsável as modificações necessárias para o MR passar nas validações
2. Cria uma issue para que o problema seja corrigido posteriormente e aprovar o item
3. Aprovar o item sem correção caso a validação esteja errada e/ou desnecessária

> Nesse último item é interessante que o aprovador configure o projeto para que as próximas validações não apontem o mesmo problema.
