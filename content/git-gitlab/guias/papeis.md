---
title: Papeis
description: Descreve os papeis para issues e MRs
---

Várias pessoas podem exercer o **mesmo papel ao mesmo tempo no mesmo item** então a comunicação entre o time é essencial.

## Responsável

{{% notice note %}}
Merge Requests e Issues.
{{% /notice %}}

Objetivo: Fazer as modificações necessárias para que o MR/Issue atenda ao requisitos e possa ser integrado ao projeto.

Responsabilidades:

- Executar as tarefas conforme os requisitos do projeto/time
- Criar um MR para revisão com o resultado das tarefas
- Observar e responder às discussões nas issues que é responsável
- Avaliar/Atender as dúvidas ou solicitações dos revisores de MRs

Todos que contribuíram para o conteúdo devem ser "creditados" no campo de responsável.

## Revisor/Aprovador

{{% notice note %}}
Somente para Merge Requests.
{{% /notice %}}

Objetivos: Avaliar o conteúdo para garantir que ele atende a todos aos requisitos e só então aceitar o MR.

Pode solicitar correções:

- de conteúdo (código, documentação, protótipo...)
- forma (assunto, descrições, etc...)

Leia também nossa documentação sobre as [aprovações](../aprovals).
