---
title: Licenças
description: Descreve as licenças para cada tipo de repositórios
---

Os projetos do GOVBR-DS usam 2 tipos de licença:

Para projetos de software usamos a [licença MIT](https://opensource.org/licenses/MIT 'Licença MIT').

Para projetos de design e documentações, usamos [licença CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/ 'Licença CC0 Universal').

> Os projetos do nosso grupo devem seguir a licença conforme descrito acima. Caso algum projeto tenha necessidade de uma licença diferente isso deve ser avaliado pelo time.
