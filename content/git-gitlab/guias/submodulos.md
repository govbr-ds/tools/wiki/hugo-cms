---
title: Submódulos
description: Guia de boas práticas de uso de submódulos
---

Submódulos são uma maneira de incluir um projeto dentro de outro. O projeto principal contem uma referencia a um commit do repositório do submódulo e é essa referencia que indica a versão do submódulo que será incluída no principal.

Não é recomendado alterar o submódulo de dentro do principal, pois isso causa uma alteração da referência. Altere o projeto referente ao submódulo e depois faça a troca da referência (usando git checkout, por exemplo) para a referência desejada.

Antes de versionar, fique atendo à mudanças nas referências dos submódulos. Não commite uma mudança de referência se isso não for o desejado.

## Comandos

### Criar um Submódulo

```bash
git init

git submodule add -b <Branch> <URL do Repositório do Submódulo> <Diretório do Submódulo>

git add .

git commit -m "Adiciona submódulo"
```

### Clonar um Repositório com Submódulos

```bash
git clone --recursive <URL do Repositório Pai>
```

ou

```bash
git clone <URL do Repositório Pai>

git submodule update --init --recursive
```

### Atualizar Submódulos

```bash
git submodule update --init --remote --recursive
```

### Remover um Submódulo

```bash
git submodule deinit -f -- <Diretório do Submódulo>

git rm --cached <Diretório do Submódulo>

rm -rf .git/modules/<Diretório do Submódulo>

git commit -m "Remove submódulo"
```

## Sugestão

Criamos um [hook no Git](https://git-scm.com/docs/githooks#_commit_msg) para avisar quando houver uma alteração na referência de um submódulo. Isso auxilia no trabalho, mas **não substitui** a atenção ao se fazer um commit. Tome **cuidado**!!

Por favor faça sugestões/correções para esse hook caso ache necessário.

### Arquivo .husky/commit-msg

```bash
commit_msg_file=$1

print_message() {
  local color_code=$1
  local message=$2
  echo -e "\e[${color_code}m${message}\e[0m" >&2
}

submodules=$(git submodule | awk '{ print $2 }')
changes_detected=false
for submodule in $submodules; do
  old_commit=$(git rev-parse --short=8 HEAD^:$submodule)
  new_commit=$(git rev-parse --short=8 HEAD:$submodule)

  if [ "$old_commit" != "$new_commit" ]; then
    changes_detected=true
  fi
done

if [ "$changes_detected" = true ]; then
  commit_message=$(cat "$commit_msg_file")
  if ! echo "$commit_message" | grep -iqE '^\s*chore\(submodule\)|^\s*feat\(submodule\)|^\s*fix\(submodule\)|^\s*refactor\(submodule\)|^\s*docs\(submodule\)'; then
    echo -e ""
    print_message "31" "===================================="
    print_message "31" "Sempre que o commit alterar a referência de um submódulo, o escopo de ser 'submodule'."
    print_message "31" "===================================="
    echo -e ""
    exit 1
  fi
fi
exit 0
```
