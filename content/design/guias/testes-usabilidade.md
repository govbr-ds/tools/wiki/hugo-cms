---
title: Testes de Usabilidade
description: Este documento detalha as orientações para a realização de diversos tipos de testes com usuários.
date: 11/04/2022
keywords: testes. usuário, usuários, testes com usuário, teste de usuário, teste de usabilidade
---

## Qual é a importância do teste de usabilidade?

Esses testes têm amplas utilidades para as empresas, uma delas é ajudar a conhecer melhor o comportamento do usuário, identificando seus gostos, interesses, dificuldades, necessidades e outras informações.

Também são encontradas dificuldades de navegação, falhas que geram travamentos, lentidões e demais problemas. Com essas informações, você aplicará as melhorias necessárias para proporcionar uma experiência mais agradável aos clientes ou usuários de uma plataforma. Outra vantagem é a possibilidade de explorar um novo público-alvo e encontrar potenciais usuários.

Por exemplo, se você tiver um fluxo que vende produtos para diferentes perfis de clientes, usando um teste de usabilidade, você identifica que determinado perfil abandona uma compra em determinada etapa. Esse é um dado importante para que você faça as modificações necessárias para conquistar esse perfil. Com isso, você pode aumentar suas vendas e a retenção dos clientes, bem como melhorar a imagem do negócio no mercado.

Neste documento, exploramos diferentes métodos de **teste de usabilidade**, explicando quando você deve usá-lo e porque.

Em três fases do fluxo de processo do GOVBR-DS está previsto um tipo de teste, cada um deles focado em uma necessidade específica. Sinta-se livre, porém, para realizar um teste de usabilidade a qualquer momento que se julgar necessário.

### Teste de Hipótese (*opcional*)

Ocorre na **fase de entendimento** da demanda e é opcional.

Nesta etapa podem ocorrer dúvidas sobre qual caminho a seguir. Talvez seja o momento de realizar um dos testes abaixo para se ter mais convicção de que uma ideia vale mais a pena o esforço do que outra.

Lembre-se que esse teste deve ser realizado de maneira rápida e informal pois, o que está sendo testado é o conceito ou ideia e não uma funcionalidade específica.

**Opções de Testes de Hipótese:**

1. Teste Exploratório;
2. Card Sorting;
3. Teste A/B.

#### 1- Teste Exploratório

Realizados mais frequentemente nas etapas iniciais do projeto do produto, os **Testes Exploratórios** exploram o modelo conceitual em relação aos modelos mentais mais comuns aos usuários e às tarefas que vão realizar.

Ajudam a verificar aspectos críticos para estruturar as linhas-mestras do produto, como:

- A composição dos elementos na tela ou a localização das barras de navegação em relação ao conteúdo, por exemplo;

- O desenvolvimento de mostruário de produtos;

- O valor das ações realizadas para o usuário no ambiente do site ou aplicativo.

Como o layout das telas na maioria dos casos ainda não está pronto, muitos testes são realizados com representações das telas no formato de wire frames. Uma representação da ideia pode ser realizada por meio de uma simulação do protótipo com o layout básico, uma organização simples dos elementos, simulação das funcionalidades principais e das macro operações que os usuários precisam realizar.

Devido aos suportes improvisados, a ambientação não exige preparo especial e o relacionamento entre avaliador e participantes é informal. Há trocas de opiniões e de ideias, para que o avaliador entenda o que o usuário pensa e sente.

#### 2- Card Sorting

A classificação ou ordenação de cartões, comumente conhecida como **Card Sorting**, envolve a colocação de conceitos em cartões. Esse processo permite que os participantes manipulem os cartões em grupos e categorias diferentes. Depois de ordenar os cartões, eles explicam sua lógica em uma sessão de perguntas pensadas por moderadores.

O **Card Sorting** é um ótimo método para sites novos para obter feedback sobre o layout e a estrutura de navegação. Seus resultados mostram aos designers e gerentes de produto como as pessoas e os clientes em potencial organizam naturalmente as informações.

#### 3- Teste A/B

Diferentemente do teste de usabilidade, que investiga o comportamento do usuário, o **Teste A/B** trata de experimentar várias versões de uma página da web para ver qual é a mais eficaz. É uma ferramenta importante para aumentar conversões ou descobrir a preferência dos usuários.

### Teste de Usabilidade

Ocorre necessariamente na **fase de criação/prototipação** da demanda. Pode ocorrer também em qualquer momento que a equipe julgar necessário.

Seus objetivos são detalhar os testes de hipóteses, com a avaliação da usabilidade em um contexto funcional com algumas limitações operacionais e perceptivas.

Por meio da realização de tarefas simples, em que o comportamento do usuário se aproxima do modo como efetivamente usará o produto, permite a verificação inicial da base conceitual e a incidência de problemas de ordem geral.

Há uma imensa variedade de possibilidades de testes que podem ser realizados nesta etapa; uns mais rápidos e simples e outros que exigem maior esforço e tempo. Escolha aquele que melhor valide a demanda utilizando o menor número de recursos possível.

**Opções de Testes de Usabilidade:**

1. Teste de Guerrilha;
2. Teste Não-Moderado;
3. Teste Moderado;
4. Análise Heurística.

#### 1- Teste de Guerrilha

Nos **Testes de Guerrilha**, os participantes são escolhidos aleatoriamente em um local público, geralmente uma cafeteria, shopping ou mesmo colegas da empresa não envolvidos com o projeto. Eles são convidados a realizar um teste rápido de usabilidade, geralmente em troca de algum incentivo.

O **Teste de Guerrilha** é usado para testar uma grande variedade de pessoas que podem não ter histórico com um produto. É uma maneira rápida de coletar grandes quantidades de dados qualitativos que validam certos elementos ou funcionalidades do projeto — mas não é um bom método para testes ou acompanhamento extensivos, pois as pessoas geralmente relutam ou não querem abrir mão de mais de cinco minutos do tempo delas.

#### 2- Teste Não-Moderado

Um **Teste Não-Moderado** é realizado sem supervisão direta: os participantes podem estar em um laboratório, mas é mais provável que estejam em suas próprias casas e/ou usando seus próprios dispositivos para navegar no site que está sendo testado.

O custo de **Testes Não-Moderados** é menor em relação aos moderados, embora as respostas dos participantes possam permanecer superficiais (e, nesse cenário, também não dá para incluir perguntas de acompanhamento, ou de follow-up).

Como regra geral, use **testes não-moderados** para testar uma pergunta muito específica ou observar e medir padrões de comportamento.

#### 3- Teste Moderado

Uma sessão de **Teste Moderado** é administrada pessoal ou remotamente por um pesquisador treinado que apresenta o teste aos participantes, responde às suas dúvidas e faz perguntas de acompanhamento.

O **Teste Moderado** geralmente produz resultados detalhados, graças à interação direta entre pesquisadores e participantes, podendo ser caro para organizar e executar.

Como regra geral, use **Testes Moderados** para investigar o raciocínio por trás do comportamento do usuário.

#### Análise Heurística

Uma **Análise Heurística** (também conhecida como **Avaliação Heurística**) é uma avaliação rápida, barata e prática de um produto, interface ou serviço. Normalmente é feita por um especialista de UX que leva em conta a bagagem e experiência que possui para rapidamente determinar o que está e o que não está funcionando em um sistema.

Uma grande vantagem da **Análise Heurística** é a rapidez. Com poucas horas de trabalho você consegue gerar um bom volume de feedback sobre determinada interface. Também não requer muitas pessoas, ou muitos recursos, ou nenhum software especial.

A segunda vantagem é que ela pode ser feita em diversos momentos do projeto. Se você está trabalhando em um projeto de redesign, por exemplo, pode fazer uma análise logo no começo do projeto para entender onde estão os problemas de design e usabilidade e ajudar o restante do time a não repetir os mesmos erros na nova versão que vocês estão projetado. Pode ser feita também nas etapas mais avançadas, para avaliar um protótipo que está sendo testado, ou ainda para analisar a experiência do usuário em produtos concorrentes ao seu.

O processo é bastante flexível e você pode adaptar alguns dos pontos abaixo para as necessidades específicas do projeto no qual está trabalhando. No geral uma análise heurística é executada na seguinte ordem:

- 1. Entendendo os usuários;
- 2. Definindo as heurísticas de usabilidade;
- 3. Avaliando a experiência;
- 4. Reportando os resultados.

##### 1- Entendendo os usuários

Defina quem eles são e quais os seus objetivos ao interagirem com aquele produto. O especialista que realizará a análise precisa ter em mente o mesmo ponto de vista dos usuários e saber quais são seus objetivos — e os problemas que eles querem resolver ali. Se preciso for, converse com alguns usuários antes de começar a análise.

Defina também quais as tarefas mais comuns que os usuários realizam ali. Isso pode ajudar a definir o escopo da análise; em alguns casos o sistema é tão robusto e complexo que você precisará priorizar os fluxos, telas e tarefas mais importantes para serem avaliados. Nesse processo de definição do escopo, olhe para as métricas do produto (para ver quais delas estão sendo mais acessadas pelo usuário) e converse também com os stakeholders do projeto — eles provavelmente terão uma boa noção de quais os momentos mais importantes ou relevantes da experiência.

##### 2- Definindo as heurísticas de usabilidade

Existem vários modelos online que podem ser seguidos para avaliar a usabilidade do produto — das clássicas [“Heuristics for User Interface Design”](https://www.nngroup.com/articles/ten-usability-heuristics/) do Jakob Nielsen, até guias mais práticos como os [Principles of Interaction Design](https://asktog.com/atc/principles-of-interaction-design/), do Bruce Tognazzini.

##### 3- Avaliando a experiência

Essa é a etapa “mão na massa”, onde você fará a análise propriamente dita da experiência.

Na prática, você vai navegar pelo produto como um usuário navegaria — tentando completar tarefas e passando pelos diferentes momentos da experiência que você e seu time definiram como prioritárias para a análise. À medida em que vai fazendo isso, você vai tirando screenshots (ou gravando vídeos) e anotando os problemas que encontrou no meio do caminho. Documente também a gravidade do problema (crítico > grave > menor > boa prática) para garantir que se consiga priorizar os itens mais importantes.

##### 4- Reportando os resultados

Explique quais os critérios usados para a análise heurística, quem foi o especialista (ou os especialistas) que analisou a experiência, e coloque tudo em um formato apresentável para dividir com o restante do time. Lembre-se também de relatar um resumo dos principais aprendizados depois da análise.

O método só funciona bem quando feito por um profissional de UX que seja realmente considerado um especialista — com vários anos de experiência em desenhar, testar e projetar interfaces daquele tipo ou para aquela plataforma. Um profissional com menos experiência pode acabar fazendo uma análise mais subjetiva do que a recomendada. Para evitar esse problema de subjetividade, o ideal é que você utilize mais de um avaliador — uma avaliação feita por um profissional normalmente pega 20% dos problemas de usabilidade de um sistema. O número recomendado é de 3 a 5 profissionais.

### Acompanhamento/Feedback

São testes e/ou pesquisas que ocorrem no final do processo, após a **etapa de implementação**. Certificam a usabilidade com características formais, informacionais e funcionais já definidas.

Seu objetivo é verificar o desempenho do produto nas referências e padrões de usabilidade e de performance estabelecidos no planejamento da qualidade, podendo coletar feedbacks, descobrir eventuais bugs que tenha passado pelo processo e possibilidades futuras de melhorias.

**Opções de acompanhamentos/feedbacks:**

1. Pesquisa de Avaliação;
2. Observação;
3. Eye-Tracking;
4. Mapa de Calor/Rolagem;
5. Google Analytics.

#### 1- Pesquisa de Avaliação

A **pesquisa de avaliação** é usada para testar a satisfação da pessoa usuária com um produto (e quão bem essa pessoa está sendo capaz de usá-lo). É usada para avaliar a funcionalidade geral do produto.

Como um indicador da experiência do usuário, as pesquisas podem ser usadas em conjunto com os testes de usabilidade como acompanhamento ou método de coleta de feedback.

#### 2- Observação

Uma das maneiras mais fáceis de começar a entender como reagem seus usuários é por meio da **Observação**: essa técnica remota e não moderada ajuda a identificar problemas de usabilidade, apenas observando pessoas reais interagindo com as páginas e elementos do site/aplicativo. Requer pouca configuração e é uma maneira simples de começar a melhorar a funcionalidade e a eficácia do seu site ou app.

#### 3- Eye-Tracking

Durante os testes de rastreamento ocular, (ou como são mais conhecidos, **Eye-Tracking**) pesquisadores observam e estudam os movimentos oculares dos usuários usando um dispositivo especial de rastreamento montado em um computador. Ao analisar para onde os usuários direcionam sua atenção quando solicitados a concluir uma tarefa, o software pode criar mapas de calor ou diagramas de vias de movimento.

Os estudos de **Eye-Tracking** podem ser usados ​​para coletar informações sobre como os usuários interagem com uma página; eles também ajudam a testar os elementos de layout e design e a entender o que pode estar atrapalhando ou afastando o foco de alguém dos elementos da página principal. A desvantagem? Custo: um teste de eye-tracking exige que você alugue um laboratório com equipamento especial e software específico (além de um técnico treinado que pode ajudá-lo a calibrar o dispositivo).

#### 4- Mapa de Calor/Rolagem

**Mapas de Calor** e **Mapas de Rolagem** produzem uma representação visual de como as pessoas se movem pela página, apresentando as partes mais quentes (mais populares) e mais frias (menos populares). Tecnicamente, não são testes de usabilidade porque relatam as ações de forma agregada, mas são uma boa maneira de observar e medir objetivamente o comportamento em seu site.

#### 5- Google Analytics

O **Google Analytics** é o serviço oficial e gratuito de monitoramento de marketing digital do Google. Sua principal função é coletar dados de acesso, comportamento e navegação em sites e aplicativos por meio de códigos de rastreamento e organizar essas informações em relatórios diversificados.

Além de disponibilizar ricas análises de tráfego, a ferramenta auxilia webmasters na otimização de páginas, na condução de testes e na tomada de decisões em negócios e estratégias de marketing.

O **Google Analytics** é considerado uma peça essencial dos sites na web, pois, sem ele, ficamos literalmente no escuro. Como diz a famosa citação do estatístico William Edwards Deming, “não se gerencia o que não se mede”.
