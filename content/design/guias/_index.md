---
title: 'Guias'
chapter: false
pre: '<i class="far fa-copy"></i> '
---

## Introdução

À medida que os times de design começam a crescer em tamanho, o processo de design cresce em complexidade.

Neste contexto, pequenas gestões podem se tornar motivos de insegurança dentro da equipe: como o simples fato de um integrante da equipe se ausentar ou a integração de uma novo membro na equipe.

## Objetivo

Os documentos encontrados nessa sessão possuem o objetivo de instruir a equipe de design a fim de padronizar processos e de manter a consistência do desenvolvimento de artefatos entregáveis delimitados no projeto do GOVBR-DS.

Para tal, são detalhado algumas definições, metodologias, etapas, passos, tecnologias, modelos, que possam auxiliar a equipe de design a entender e aperfeiçoar seu trabalho com outros designer e entre membros de outros times envolvidos (como *Front-ends*). Além disso, essas instruções funcionam como um guia introdutório para novos designers no projeto e para consultas em momentos de criação.

É importante entender que os documentos servem apenas como um instrumento que evolui ao longo do tempo, sempre levando em consideração os cenários encontrados ao longo do trabalho da equipe. Muitas informações contidas servem apenas como uma introdução sobre determinados assuntos. Designers devem, por conta própria, buscar aperfeiçoar o conteúdo existente neste documento, acrescentando e editando o que for pertinente buscando mantê-lo sempre o mais atualizado possível.

{{% notice info %}}
Não hesite em procurar o responsável pelo design do Design System para sugerir assuntos e correções ou simplesmente buscar um maior entendimento.
{{% /notice %}}
