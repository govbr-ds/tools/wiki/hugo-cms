# Changelog
Todas as alterações notáveis neste projeto são documentadas neste arquivo.

## [Released]

## [X.Y.Z] - DD/MM/AAAA

### Adicionado 
-
-

### Modificado
- 
- 

### Obsoleto
- 
- 

### Removido
- 
- 

### Corrigido
- 
- 

### Segurança
- 
- 