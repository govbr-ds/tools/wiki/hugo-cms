---
description: Este documento serve de modelo para facilitar a revisão de qualidade, para qualquer tipo de elemento atômico (EA) criado ou editado pelos designers
date: 17/11/2020
keywords: uikit, adobe XD, figma, componentes, assets, changelog, elemento atômico, tokens, versionamento, fundamentos, comportamentos, template
---

# Revisão

## Como utilizar este documento?

Este documento deve ser utilizado como um facilitador no momento de realizar a revisão de qualidade para qualquer tipo de elemento atômico (EA) criado ou alterado pelos designers.

Este documento foi baseado no [checklist de revisão](/design/checklists/revisao). Portanto, deve ser atualizado sempre que esse checklist sofrer alterações.

O revisor deve estar em posse do modelo de revisão (veja abaixo), onde se encontram listados todos os itens relevantes a serem seguidos para que um EA seja criado/atualizado dentro dos padrões de qualidade estabelecidos pelo Design System.

Analisando item por item, como um checklist, verifique a existência de inconsistências, anotando e detalhando o erro de forma textual. Quando necessário, utilize imagens para melhor detalhar a revisão.

Para cada item listado, o revisor deve fornecer um feedback, seja detalhando o erro ou apenas informando que não existe nenhum problema no item.

---

## Revisão “Elemento Atômico X”

### Padrões de Nomenclatura

#### Geral

- Nome dos arquivos e diretórios?

#### Design Token

- Nome dos tokens dentro da diretriz?

#### Assets

- Nome dos assets/ativos (cor, tipografia, componentes) do arquivo adobe XD?

## Regras de Versionamento

### Fluxo de Versionamento Pré-Release

- Seguindo as fases de pré-release?
- Nome da pré-release está correto? O incremento?

### Criação dos Changelogs

- Foi relatado no lugar e/ou no arquivo correto?
- Texto estão sendo relevantes para criação da release?

### Tipo de mudança

- Foram utilizados os seis tipos de mudança para relatar os changelog (Adicionado, Modificado,Obsoleto, Removido, Corrigido e Segurança)?

## Estrutura Básica de Diretórios e Artefatos

### Diretórios básicos

- Foi criada a estrutura hierárquica básica para criação de um elemento atômico?

### Artefatos básicos

- Foram gerados os arquivos básicos para criação de um elemento atômico (arquivo fonte, diretriz, changelog)?

## Guia de Arquivo Fonte

### Importar o UIkit

- Foi importado o UIkit oficial do Design System para dentro do arquivo fonte?

### Criar e utilizar as pranchetas corretamente

- Nome, tamanho, espaçamento, cor, cabeçalho especiais, regiões (diretriz, elemento atômico, changelog)?

### Criar os elementos gráficos/atômicos

#### Utilize conceito de componentização (Atomic Design)

- Foi utilizado o UIkit oficial do Design System para criação dos elementos na tela?
- Foram utilizados os fundamentos corretamente?
- Foram utilizadas as diretrizes dos componentes já existentes?

#### Utilizar as nomenclaturas para assets

- Todos os elementos foram nomeados utilizando a nomenclatura para assets?
- Elementos genéricos foram nomeados (container, text, icon, title, number, category, fill, metadata, etc.)?

#### Utilizar sempre valores inteiros

- Dimensões, coordenadas e bordas de todos os elementos utilizaram números inteiros?

#### Criar ativos

- O elemento foram criados baseados no conceito de assets/componentes?
- Uso de recursos como “repetição de grade” do adobe XD foi bem empregado?

#### Utilizar cotas e legendas padrões

- Uso de legenda ou cotas foram usado em excesso ou com falta?
- Cotas e legendas seguiram as especificações padrões?

## Guia de Diretriz

- A diretriz faz referência à versão do elemento atômico correspondente?
- Foram seguidas as dicas listadas como padrão de qualquer diretriz?

---

### Fundamento

#### Nome do fundamento

- O nome e o resumo da descrição estão adequados?

#### Princípios

- Os cinco princípios básicos (experiência única, eficiência e clareza, acessibilidade, reutilização e colaboração) estão descrito com clareza?

#### Detalhando o fundamento

- É possível compreender o fundamento e como aplicá-lo na interface e componentes que serão criados no Design System?

#### Design tokens

- Existem tokens do fundamento que possam ser reutilizados dentro dos componentes e na interface?

---

### Componente

#### Nome do componente

- O nome do componente e o resumo da descrição estão adequados?

#### Uso

- É possível compreender como utilizar corretamente o componente?
- Existe algum cuidado com uso de tom e voz?

#### Anatomia

- É possível verificar com clareza quais elementos constituem o componente?
- Existe detalhamento de cada elemento?
- Outros componentes fazem parte deste componente?

#### Tipos

- Os tipos de componentes foram listados e é fácil compreender o uso e o comportamento de cada um deles?
- Existe texto de detalhamento e é fácil compreender as diferenças cada tipo?

#### Comportamentos

- Existe previsão de comportamento por estados, responsividade, rolagem de dados, densidades, etc?

#### Design tokens

- Os tokens estão organizados por fundamentos e tipos?
- É possível criar, visualmente, o componente apenas com uso de tokens?
- Foram detalhados os atributos como cores, dimensões, espaçamento interno e externo, fonte, etc.?

---

### Template

#### Nome do Template

- O nome do template e o resumo da descrição estão adequados?

#### Uso

- É possível compreender como utilizar corretamente o template?
- Existe algum cuidado com uso de tom e voz?
- Foram mencionadas as situações em que não se deve utilizar o template?

#### Estrutura

- Quais componentes fazem parte deste template?
- Foi claramente definida a finalidade de cada região do template?
- O detalhamento dessas regiões são de fácil compreensão?

#### Tipos

- O template possui variações que fazem sentido serem mencionada?
- É possível distinguir claramente o uso e o comportamento de cada variação?

#### Comportamentos

- Existe previsão de comportamento em diferentes responsividades?
- Alguma área do template pode ficar desabilitada em alguma situação específica?
- As áreas podem ser opcionais em distintos dispositivo, tela ou responsividade?

## Guia para Imagens

- As imagens foram exportadas na pasta correta?
- Foi fornecida a lista padrão para criação de imagens para a diretriz (estão no arquivo fonte? As pranchetas estão na região correta? Formato, dimensões, espaçamento, etc.)?

## Palavras-Chaves

Utilize os seguintes termos para realizar a revisão:

- **Não cabe revisão**: não faz sentido revisar o item.
- **Não cabe revisão no momento**: não faz sentido revisar o item devida a alguma pendência na documentação.
- **Nenhum problema encontrado**: após a análise, não foi constatado nenhum problema.
- **Detalhado em “Item - Subitem”**: o item foi analisado porém descrito em outro item do documento.
