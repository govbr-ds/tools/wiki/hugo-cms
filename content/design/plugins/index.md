---
title: 'Plugins'
description: Recursos utilizados pela equipe de design
pre: '<i class="fas fa-plug"></i> '
keywords: ferramentas, plugin, addons, produtividade, design
---

Disponibilizamos os principais **plugins** utilizados pela equipe de design do GOVBR-DS.

## Resize Artboard (GOVBR-DS)

Automatiza o espaçamento interno recomendado nas pranchetas no Adobe XD. Por enquanto esse plugin só está disponível para Adobe XD.

- [Resize Artboard (GOVBR-DS) | Formato XDX - 32,39 KB](adobexd/resize-artboard-govbr-ds.xdx)
