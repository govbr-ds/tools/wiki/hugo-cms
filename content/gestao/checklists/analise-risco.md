---
title: Análise de Risco
description: Checklist para Análise de Risco
date: 25/03/2021
keywords: analise, risco, serviços, apps, componente, atividade
---

A **análise de riscos** deve fazer parte da rotina de planejamento estratégico de todo produto. Por meio dessa prática, é possível evitar diversos tipos de problemas.

- [ ] Existem riscos identificados para o projeto ou serviços? Listar os riscos.
- [ ] Listar todas as possibilidades negativas que a demanda pode causar.
- Qual o grau para cada risco?
- [ ] Baixo
- [ ] Médio
- [ ] Alto
- [ ] Listar as estratégias definidas para cada risco.
