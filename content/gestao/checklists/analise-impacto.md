---
title: Análise de Impacto
description: Checklist para Análise de Impactos
date: 17/11/2020
keywords: analise, impactos, tarefas, correção
---

- [ ] Há impacto visual nos componentes (dependência)? Listar os elementos impactados.
- [ ] Há impacto funcional nos componentes (dependência)? Listar os elementos impactados.
- [ ] Há impacto nas diretrizes? Listar os documentos impactados.
- [ ] Há impacto nos UIKits? Listar os UIKits impactados.
- [ ] Há impacto nos códigos? Listar os documentos impactados.
- [ ] Há impacto no site? Listar as páginas impactadas.

---

- [ ] A atividade possui alguma restrição para sua execução? Listar as restrições ou pendências a serem executadas.
- [ ] A atividade pode trazer algum impacto no visual a ponto de gerar **breaking change**, a possibilidade de modificar a estrutura dos componentes existentes é alta?
- [ ] Quais impactos podem acontecer da não execução?
- [ ] Quais impactos afetam os serviços que consomem o Design System?
- [ ] O que trás de impacto para os apps que consomem o Design System?
- [ ] Comunicar o resultado da análise para aprovação ou rejeição do time.
- [ ] Criar tarefas e issues de correção dos impactos listados com a descrição detalhada da análise.

**Atenção:** caso a atividade traga algum impacto para o Design System ou para serviços/apps existentes o **comitê** deve ser notificado e sua execução dependerá da sua aprovação.
