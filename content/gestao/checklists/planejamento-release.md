---
title: Planejamento da Release
description: Checklist para auxiliar o planejamento da release.
date: 13/04/2022
keywords: planejamento, release
---

- [ ] Definir os objetivos da release.
- [ ] Definir MVP da release.
- [ ] Priorizar as demandas para atendimento.
- [ ] Definir backlogs.
- [ ] Estimar os esforços.
- [ ] Definir a quantidade de sprints (prazo).
- [ ] Formalizar os backlogs no ALM/GitLab.
