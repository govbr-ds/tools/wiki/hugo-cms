---
title: Planejamento de Sprints
description: Checklist para auxiliar o planejamento das sprints
date: 13/04/2022
keywords: planejamento, sprints
---

- [ ] Definir o objetivo da sprint.
- [ ] Definir quais backlogs a serem atendidos.
- [ ] Preparar os backlogs que serão atendidos.
- [ ] Definir os responsáveis pelos backlogs.
- [ ] Criar no ALM/GitLab as tarefas relacionadas aos backlogs.
