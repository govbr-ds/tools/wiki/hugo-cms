---
title: Homologação de Versão para Publicação
description: Checklist para Homologação de Versão para Publicação
date: 09/11/2021
keywords: versão, changelog, repositório, site, govbrds, compreensível, componentes, homologados, assets, documento
---

## Antes de criar a versão

- [ ] Verificar se o repositório da versão do site esteja com mesma versão do GOVBR-DS

## Após criação da versão

- [ ] Changelog não tenham duplicados
- [ ] Changelog sejam simples e Compreensível
- [ ] Todos os componentes que estão homologados estão listados no menu
- [ ] Versão dos assets e se foram criados
- [ ] Versão do documento como começar com a nova Versão
- [ ] Verificar se os links de download não quebre
