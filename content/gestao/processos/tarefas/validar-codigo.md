---
title: Validar código
date: 2022-05-02T11:34:15-03:00
draft: false
---

## Descrição

O desenvolvedor deve ao finalizar a etapa de codificação solicitar que outro desenvolvedor avalie a qualidade do código escrito.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
