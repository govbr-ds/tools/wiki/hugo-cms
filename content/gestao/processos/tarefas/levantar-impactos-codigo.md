---
title: Levantar impactos de código
date: 2022-04-29T19:00:27-03:00
draft: false
---

## Descrição

O levantamento de impactos é um dos quesitos mais importantes da fase de análise de uma demanda. Todos os impactos gerados pela tarefa devem ser listadas e servirão de referência para a priorização da demanda na fase de planejamento da release.

## Processos relacionados

- [Processo de análise da issue](/gestao/processos/analise/analise-demanda/)

## Links úteis

- [](/)
