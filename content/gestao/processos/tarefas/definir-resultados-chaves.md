---
title: Definir resultados chaves
date: 2022-04-29T19:34:17-03:00
draft: false
---

## Descrição

Defina os resultados-chave ao formalizar uma demanda.

## Processos relacionados

- [Processo de formalização da demanda](/gestao/processos/planejamento/formalizacao-demanda)

## Links úteis

- [](/)
