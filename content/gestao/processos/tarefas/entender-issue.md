---
title: Entender issue
date: 2022-04-29T18:59:49-03:00
draft: false
---

## Descrição

Toda nova demanda, ao ser aberta, deve gerar uma issue única. Cabe ao gestor, ou à equipe de desenvolvedores e designer analisar e entender as necessidades da demanda.

## Processos relacionados

- [Processo de análise da issue](/gestao/processos/analise/analise-demanda/)

## Links úteis

- [](/)
