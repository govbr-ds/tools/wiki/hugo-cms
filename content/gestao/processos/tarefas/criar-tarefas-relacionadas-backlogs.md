---
title: Criar tarefas relacionadas aos backlogs
date: 2022-05-02T09:45:34-03:00
draft: false
---

## Descrição

Toda sprint deve possuir suas tarefas criadas no GitLab e no ALM.

## Processos relacionados

- [Processo de planejamento da sprint](/gestao/processos/planejamento/planejamento-sprint)

## Links úteis

- [](/)
