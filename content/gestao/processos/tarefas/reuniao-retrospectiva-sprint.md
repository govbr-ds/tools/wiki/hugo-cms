---
title: Reuniao de retrospectiva da sprint
date: 2022-05-02T09:34:58-03:00
draft: false
---

## Descrição

Todo final de sprint deve contar com o evento de retrospectiva que é o momento ideal para cada membro do time apresentar suas impressões, dificuldades e aprendizagens durante a sprint. É o momento propício também para discutir ajustes nos processos.

## Processos relacionados

- [Processo de condução da sprint](/gestao/processos/execucao/conducao-sprint)

## Links úteis

- [](/)
