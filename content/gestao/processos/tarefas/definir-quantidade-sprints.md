---
title: Definir quantidade de sprints (prazo)
date: 2022-04-29T19:25:34-03:00
draft: false
---

## Descrição

Na etapa de planejamento da release, o time deve determinar a quantidade de sprints suficientes para a conclusão das tarefas priorizadas. Geralmente o DS trabalha com release de quatro sprints.

## Processos relacionados

- [Processo de planejamento da release](/gestao/processos/planejamento/planejamento-release)

## Links úteis

- [](/)
