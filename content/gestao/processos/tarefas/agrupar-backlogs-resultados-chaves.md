---
title: Agrupar backlogs nos resultados chaves
date: 2022-04-29T19:36:55-03:00
draft: false
---

## Descrição

Os itens de backlog devem ser agrupados de acordo com os resultados-chave esperados.

## Processos relacionados

- [Processo de formalização da demanda](/gestao/processos/planejamento/formalizacao-demanda)

## Links úteis

- [](/)
