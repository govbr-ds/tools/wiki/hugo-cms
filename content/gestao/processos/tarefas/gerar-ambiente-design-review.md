---
title: Gerar ambiente de design review
date: 2022-05-02T11:33:29-03:00
draft: false
---

## Descrição

Ao concluir o trabalho de implementação, o desenvolvedor responsável deve gerar o ambiente de **Design Review** para o designer responsável realizar a etapa de validação do artefato implementado.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
