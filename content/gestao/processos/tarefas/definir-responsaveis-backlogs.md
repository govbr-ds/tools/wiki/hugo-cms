---
title: Definir responsaveis pelos backlogs
date: 2022-05-02T09:44:32-03:00
draft: false
---

## Descrição

No processo de planejamento da sprint a equipe define os responsáveis para cada demanda. De forma geral, a demanda padrão deve possuir no mínimo um desenvolvedor e um designer como responsáveis, porém, há demandas específicas que podem ser atendidas unicamente por designers ou por desenvolvedores.

## Processos relacionados

- [Processo de planejamento da sprint](/gestao/processos/planejamento/planejamento-sprint)

## Links úteis

- [](/)
