---
title: Teste intrínseco (heurística)
date: 2022-05-02T10:27:41-03:00
draft: false
---

## Descrição

O designer deve providenciar um teste de heurísticas após concluir a fase de prototipação. As heurísticas serão úteis para garantir que nenhum ponto obrigatório foi esquecido.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
