---
title: Criar sub-épicos dos resultados chaves
date: 2022-04-29T19:35:58-03:00
draft: false
---

## Descrição

Para maior organização das demandas no GitLab, crie sub-épicos relacionados aos épicos existentes.

## Processos relacionados

- [Processo de formalização da demanda](/gestao/processos/planejamento/formalizacao-demanda)

## Links úteis

- [](/)
