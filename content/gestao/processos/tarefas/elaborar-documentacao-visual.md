---
title: Elaborar documentação visual
date: 2022-05-02T10:30:09-03:00
draft: false
---

## Descrição

Ao finalizar a implementação de um artefato validado pelo designer por meio do Design Review, o desenvolvedor deve elaborar a documentação visual.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
