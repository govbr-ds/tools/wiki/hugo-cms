---
title: Discovery
date: 2022-04-29T18:25:28-03:00
draft: false
---

## Descrição

Ao se iniciar a sprint, designer e desenvolvedor responsável deve juntos analisar e discutir possíveis soluções para a demanda. É importante ter em mente que a solução projetada pelo designer e implementada pelo desenvolvedor é o resultado do entendimento dessa etapa sendo, portanto, de responsabilidade das duas partes.

## Processos relacionados

- [Processo de descoberta de necessidade](/gestao/processos/descoberta/descoberta-necessidade)

## Links úteis

- [](/)
