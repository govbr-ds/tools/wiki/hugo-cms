---
title: Publicar no ambiente de produção
date: 2022-05-02T13:27:42-03:00
draft: false
---

## Descrição

Quando uma versão do produto está finalizada e homologada o responsável deve publicá-la no ambiente de produção.

## Processos relacionados

- [Processo de implantação](/gestao/processos/implantacao/implantacao)

## Links úteis

- [](/)
