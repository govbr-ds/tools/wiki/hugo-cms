---
title: Implementar cenário de teste
date: 2022-05-02T11:30:42-03:00
draft: false
---

## Descrição

Para garantir a qualidade das entregas, o desenvolvedor deve implementar o cenário de teste ao finalizar a implementação do artefato.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
