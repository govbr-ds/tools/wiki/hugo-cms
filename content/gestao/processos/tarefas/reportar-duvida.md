---
title: Reportar dúvida
date: 2022-04-29T18:59:57-03:00
draft: false
---

## Descrição

Informar ao autor da issue a respeito de uma dúvida sobre o entendimento da mesma.

## Processos relacionados

- [Processo de análise da issue](/gestao/processos/analise/analise-demanda/)

## Links úteis

- [](/)
