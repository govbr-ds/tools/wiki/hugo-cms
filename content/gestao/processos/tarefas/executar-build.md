---
title: Executar a build
date: 2022-05-02T13:26:25-03:00
draft: false
---

## Descrição

Após criar e enviar uma nova tag para o servidor git o build será realizado automaticamente verificando possíveis falhas no processo.

## Processos relacionados

- [Processo de implantação](/gestao/processos/implantacao/implantacao)

## Links úteis

- [](/)
