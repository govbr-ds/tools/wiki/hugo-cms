---
title: Reportar resultado da validação da diretriz
date: 2022-05-02T11:28:11-03:00
draft: false
---

## Descrição

Após o designer concluir a tarefa de criação das diretrizes, o desenvolvedor responsável deve analisar o documento considerando clareza e objetividade as informações para subsidiar o trabalho de implementação. Caso nao esteja satisfatório, o designer deverá ser comunicado para providenciar os ajustes necessários.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
