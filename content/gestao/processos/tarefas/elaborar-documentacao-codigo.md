---
title: Elaborar documentação de código
date: 2022-05-02T11:31:55-03:00
draft: false
---

## Descrição

Ao implementar um novo artefato, o desenvolvedor deve elaborar a documentação de código.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
