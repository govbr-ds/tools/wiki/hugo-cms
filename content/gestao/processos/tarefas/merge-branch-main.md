---
title: Merge na branch main
date: 2022-05-02T11:34:55-03:00
draft: false
---

## Descrição

O merge na branch principal (main) deve ser executado sempre que o desenvolvedor finaliza um trabalho de modificação de um artefato.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
