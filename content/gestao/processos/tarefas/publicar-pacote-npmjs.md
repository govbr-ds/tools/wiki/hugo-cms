---
title: Publicar pacote no NPMJS
date: 2022-05-02T13:27:01-03:00
draft: false
---

## Descrição

O DevOps deve realizar a publicação do pacote NPM da nova versão.

## Processos relacionados

- [Processo de implantação](/gestao/processos/implantacao/implantacao)

## Links úteis

- [](/)
