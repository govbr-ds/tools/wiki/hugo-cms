---
title: Preparar issue para atendimento
date: 2022-04-29T19:01:05-03:00
draft: false
---

## Descrição

Ao finalizar a tarefa de análise da demanda, caso não seja rejeitada, a issue deve estar pronta para ser trabalhada.

## Processos relacionados

- [Processo de análise da issue](/gestao/processos/analise/analise-demanda)

## Links úteis

- [](/)
