---
title: Teste de hipotese
date: 2022-05-02T10:16:34-03:00
draft: false
---

## Descrição

Caso seja necessário, o designer pode antes de criar o protótipo realizar um teste rápido para confirmar ou rejeitar uma hipótese. Geralmente essa etapa acontece quando o designer e o desenvolvedor não estão convictos de uma possibilidade para a solução. O teste pode ser da maneira que o designer achar mais conveniente e rápido de se executar e o resultado obtido deve orientar a solução proposta.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
