---
title: Atualizar o changelog
date: 2022-05-02T13:24:27-03:00
draft: false
---

## Descrição

Ao finalizar e entregar um artefato, atualize o documento de changelog com as informações necessárias.

## Processos relacionados

- [Processo de implantação](/gestao/processos/implantacao/implantacao)

## Links úteis

- [](/)
