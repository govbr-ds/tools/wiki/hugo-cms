---
title: Propor novo componente
date: 2022-04-29T18:25:39-03:00
draft: false
---

## Descrição

A qualquer momento, qualquer usuário, comitê ou membro da equipe pode abrir uma issue solicitando um novo componente ou evolução de um componente já existente.

## Processos relacionados

- [Processo de descoberta de necessidade](/gestao/processos/descoberta/descoberta-necessidade)

## Links úteis

- [](/)
