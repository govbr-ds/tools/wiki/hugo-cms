---
title: Documentar motivo da rejeição
date: 2022-04-29T19:00:55-03:00
draft: false
---

## Descrição

Caso a análise da abertura de uma demanda conclua que a tarefa não deva ser realizada e, portanto rejeitada, o gestor deve documentar todos os motivos da rejeição ou de parte da rejeição na própria issue de forma clara e objetiva. Uma vez concluída essa etapa, a issue é considerada finalizada.

## Processos relacionados

- [Processo de análise da issue](/gestao/processos/analise/analise-demanda/)

## Links úteis

- [](/)
