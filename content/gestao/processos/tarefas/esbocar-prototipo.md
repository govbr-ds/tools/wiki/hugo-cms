---
title: Esbocar protótipo
date: 2022-05-02T10:28:35-03:00
draft: false
---

## Descrição

Após o alinhamento junto ao desenvolvedor responsável, o designer deve criar o protótipo (Figma ou Adobe XD) com as soluções pensadas em conjunto. Esse protótipo servirá como referência para todas as etapas seguintes do processo.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
