---
title: Definir MVP da release
date: 2022-04-29T19:23:48-03:00
draft: false
---

## Descrição

Ao se planejar uma release, uma das etapas mais importantes é a definição do MVP.

## Processos relacionados

- [Processo de planejamento da release](/gestao/processos/planejamento/planejamento-release)

## Links úteis

- [](/)
