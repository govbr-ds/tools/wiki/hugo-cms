---
title: Reportar resultado do design review
date: 2022-05-02T10:25:40-03:00
draft: false
---

## Descrição

Ao se concluir a tarefa de **Design Review**, o designer deve repostar o resultado ao desenvolvedor responsável detalhando os itens que não estão conformes às descrições das diretrizes.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
