---
title: Entendimento com o designer
date: 2022-05-02T11:28:53-03:00
draft: false
---

## Descrição

O desenvolvedor deve, ao iniciar uma demanda, discutir com o designer responsável as melhores soluções para o atendimento da demanda.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
