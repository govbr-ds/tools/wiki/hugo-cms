---
title: Validar protótipo
date: 2022-05-02T11:26:30-03:00
draft: false
---

## Descrição

O desenvolvedor responsável deve validar o protótipo considerando se a proposta apresentada está de acordo com o que foi previamente discutido pela dupla.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
