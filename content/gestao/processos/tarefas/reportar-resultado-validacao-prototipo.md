---
title: Reportar resultado da validação do protótipo
date: 2022-05-02T11:27:12-03:00
draft: false
---

## Descrição

Desenvolvedor deve avaliar o protótipo criado pelo designer e validar se a proposta apresentada está de acordo com o entendimento da demanda discutido previamente.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
