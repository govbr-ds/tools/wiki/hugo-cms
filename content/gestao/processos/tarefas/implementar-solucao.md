---
title: Implementar solução
date: 2022-05-02T11:30:03-03:00
draft: false
---

## Descrição

Após o designer criar protótipo e elaborar documentos de diretriz, o desenvolvedor está apto a implementar a solução proposta.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
