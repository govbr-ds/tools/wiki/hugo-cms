---
title: Solicitar design review
date: 2022-05-02T11:33:52-03:00
draft: false
---

## Descrição

O desenvolvedor, sempre ao final da atividade de implementação, deve solicitar ao designer responsável a execução do

**Design Review**.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
