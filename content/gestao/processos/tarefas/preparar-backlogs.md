---
title: Preparar backlogs
date: 2022-05-02T09:43:39-03:00
draft: false
---

## Descrição

Ao se planejar a sprint, a equipe deve preparar todos os itens de backlog selecionados para a sprint em questão.

## Processos relacionados

- [Processo de planejamento da sprint](/gestao/processos/planejamento/planejamento-sprint)

## Links úteis

- [](/)
