---
title: Validar diretriz
date: 2022-05-02T11:27:35-03:00
draft: false
---

## Descrição

Um designer designado deve avaliar e validar as diretrizes propostas considerando a clareza e objetividade das informações.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
