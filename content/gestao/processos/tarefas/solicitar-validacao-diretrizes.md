---
title: Solicitar validação de diretrizes
date: 2022-05-02T10:31:42-03:00
draft: false
---

## Descrição

O designer deve solicitar que o desenvolvedor, além de outro designer designado avalie e valide os documentos produzidos.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
