---
title: Sugerir funcionalidade
date: 2022-04-29T18:26:03-03:00
draft: false
---

## Descrição

A qualquer momento, qualquer usuário, comitê ou membro da equipe pode abrir uma issue solicitando uma nova funcionalidade ou a correção de um bug.

## Processos relacionados

- [Processo de descoberta de necessidade](/gestao/processos/descoberta/descoberta-necessidade)

## Links úteis

- [](/)
