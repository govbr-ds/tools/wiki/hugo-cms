---
title: Definir o objetivo da sprint
date: 2022-05-02T09:40:34-03:00
draft: false
---

## Descrição

Definir o objetivo da sprint é o primeiro fator a se considerar na etapa de planejamento da sprint.

## Processos relacionados

- [Processo de planejamento da sprint](/gestao/processos/planejamento/planejamento-sprint)

## Links úteis

- [](/)
