---
title: Estimar esforço
date: 2022-04-29T19:24:50-03:00
draft: false
---

## Descrição

Ao se planejar a release, considere estimar o esforço necessário para a realização de cada tarefa.

## Processos relacionados

- [Processo de planejamento da release](/gestao/processos/planejamento/planejamento-release)

## Links úteis

- [](/)
