---
title: Abrir issue
date: 2022-04-29T18:07:39-03:00
draft: false
---

## Descrição

Toda solicitação de demanda deve ser iniciada por meio de abertura de uma issue no GitLab. Para isso, utilize o template Defaut.

## Processos relacionados

- [Processo de avaliação do produto](/gestao/processos/metricas/avaliacao-produto)
- [Processo de descoberta de necessidade](/gestao/processos/descoberta/descoberta-necessidade)

## Links úteis

- [](/)
