---
title: Elaborar documentação de diretrizes
date: 2022-05-02T10:30:58-03:00
draft: false
---

## Descrição

Ao finalizar a criação de um artefato, o designer deve documentar todas instruções para o consumo pelo usuário e para a implementação por meio dos documentos de diretrizes.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
