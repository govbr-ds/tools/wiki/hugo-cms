---
title: Analisar resultados
date: 2022-04-29T18:48:55-03:00
draft: false
---

## Descrição

Todos os artefatos produzidos pelo DS devem ser monitorados juntos ao usuários. Os feedbacks recebidos devem orientar as futuras melhorias e evolução dos produtos.

## Processos relacionados

- [Processo de avaliação do produto](/gestao/processos/metricas/avaliacao-produto)

## Links úteis

- [](/)
