---
title: Relatar problema
date: 2022-04-29T18:25:52-03:00
draft: false
---

## Descrição

Descrever e detalhar um problema que acontece em algum artefato ou componente.

## Processos relacionados

- [Processo de descoberta de necessidade](/gestao/processos/descoberta/descoberta-necessidade)

## Links úteis

- [](/)
