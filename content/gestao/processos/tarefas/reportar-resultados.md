---
title: Reportar resultados
date: 2022-04-29T18:49:35-03:00
draft: false
---

## Descrição

A equipe do DS deve reportar os resultados levantados pelos feedbacks e análise das métricas para a equipe interna e para os stakeholders.

## Processos relacionados

- [Processo de avaliação do produto](/gestao/processos/metricas/avaliacao-produto)

## Links úteis

- [](/)
