---
title: Atualizar artefatos de design
date: 2022-05-02T13:25:56-03:00
draft: false
---

## Descrição

Quando o desenvolvedor fizer mudanças nos componentes os artefatos de design correspondentes devem ser atualizados no site.

## Processos relacionados

- [Processo de implantação](/gestao/processos/implantacao/implantacao)

## Links úteis

- [](/)
