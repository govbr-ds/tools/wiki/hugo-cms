---
title: Finalizar demanda
date: 2022-04-29T19:17:17-03:00
draft: false
---

## Descrição

Uma demanda é finalizada ao se concluir todas as etapas do processo e o artefato for considerado pronto.

## Processos relacionados

- [Processo de condução da release](/gestao/processos/execucao/conducao-release)

## Links úteis

- [](/)
