---
title: Coletar métricas
date: 2022-04-29T18:49:14-03:00
draft: false
---

## Descrição

É essencial que todos os artefatos entregues tenham seu uso acompanhados pela equipe do DS, de forma automática ou manual, afim de gerar métricas que irão orientar o planejamento estratégico.

## Processos relacionados

- [Processo de avaliação do produto](/gestao/processos/metricas/avaliacao-produto)

## Links úteis

- [](/)
