---
title: Entregar backlog
date: 2022-05-02T10:36:50-03:00
draft: false
---

## Descrição

Verificar se o backlog foi atendido, o que faltou, atualizar as labels e fechar a issue correspondente.

## Processos relacionados

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Links úteis

- [](/)
