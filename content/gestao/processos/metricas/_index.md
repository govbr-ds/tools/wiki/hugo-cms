---
title: 'Fase de Métricas'
chapter: true
pre: '<i class="fas fa-chart-line"></i>'
---

Após implementar, acompanhamos e metrificamos como nossos artefatos estão preformando.

## Fluxo Geral

- [Processo Geral do PDG - Padrão Digital de Governo](/gestao/processos)

## Processos relacionados

- [Processo de avaliação do produto](/gestao/processos/metricas/avaliacao-produto)
