---
title: Implantação
date: 2022-05-02T09:24:28-03:00
draft: false
---

![Processo de implantação](/gestao/processos/imagens/processo-de-implantação.png)

## Descrição

Nesta etapa o desenvolvedor deve implantar a solução criada e aprovada pelo time de maneira fiel às documentações produzidas ao longo da fase de construção do produto.

## Fase

- [Fase de Implantação](/gestao/processos/implantacao)

## Processos pai

- [Processo de condução da release](/gestao/processos/execucao/conducao-release)
- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)

## Tarefas

- [Atualizar o changelog](/gestao/processos/tarefas/atualizar-changelog)
- [Criar tag de versão](/gestao/processos/tarefas/criar-tag-versao)
- [Atualizar os artefatos de design](/gestao/processos/tarefas/atualizar-artefatos-design)
- [Executar a build](/gestao/processos/tarefas/executar-build)
- [Publicar pacote no NPMJS](/gestao/processos/tarefas/publicar-pacote-npmjs)
- [Publicar no ambiente de produção](/gestao/processos/tarefas/publicar-ambiente-producao)
