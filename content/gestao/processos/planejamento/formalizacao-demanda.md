---
title: Formalização da demanda
date: 2022-04-29T19:28:03-03:00
draft: false
---

![Processo de formalização da demanda](/gestao/processos/imagens/processo-de-formalizacao-da-demanda.png)

## Descrição

Todas os artefatos devem estar documentados no GitLab. A partir do momento que uma demanda está analisada e em condições de ser trabalhada é preciso que todas as informações técnicas necessárias para o entendimento do trabalho estejam disponíveis na issue.

## Fase

- [Fase de Planejamento](/gestao/processos/planejamento)

## Processos pai

- [Processo de planejamento da release](/gestao/processos/planejamento/planejamento-release)

## Tarefas

- [Agrupar backlogs nos resultados chaves](/gestao/processos/tarefas/agrupar-backlogs-resultados-chaves)
- [Criar épico da demanda](/gestao/processos/tarefas/criar-epico-demanda)
- [Criar sub-épicos dos resultados chaves](/gestao/processos/tarefas/criar-sub-epicos-resultados-chaves)
- [Definir resultados chaves](/gestao/processos/tarefas/definir-resultados-chaves)
