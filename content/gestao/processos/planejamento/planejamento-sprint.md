---
title: Planejamento da sprint
date: 2022-05-02T09:36:55-03:00
draft: false
---

![Processo de planejamento da sprint](/gestao/processos/imagens/processo-de-planejamento-da-sprint.png)

## Descrição

Todas as etapas da fase de planejamento da sprint devem acontecer no evento **Planejamento da Sprint** e contar com a participação de todos os envolvidos no time. Nesta etapa o time em conjunto executa as seguintes tarefas:

### Definição dos Objetivos

Time define em conjunto os objetivos da sprint.

### Escolha das Demandas

Time discute e decide quais demandas do item de backlog da release serão atendidas na sprint.

### Preparação do Backlog

Time prepara o backlog da sprint baseado nos itens escolhidos na etapa anterior.

### Definição dos Responsáveis

Time decide quais profissionais serão responsáveis pelo atendimento das demandas selecionadas para a sprint.

### Formalização das Demandas

Criação das tarefas no ALM e outras questões de formalização da sprint. A Sprint se encontra preparada e pronta para ser iniciada.

## Fase

- [Fase de Planejamento](/gestao/processos/planejamento)

## Processos pai

- [Processo de condução da sprint](/gestao/processos/execucao/conducao-sprint)

## Tarefas

- [Criar tarefas relacionadas aos backlogs](/gestao/processos/tarefas/criar-tarefas-relacionadas-backlogs)
- [Definir objetivo da sprint](/gestao/processos/tarefas/definir-objetivo-sprint)
- [Definir quais backlogs serão atendidos](/gestao/processos/tarefas/definir-backlogs-atendidos)
- [Definir responsáveis pelos backlogs](/gestao/processos/tarefas/definir-responsaveis-backlogs)
- [Preparar backlogs](/gestao/processos/tarefas/preparar-backlogs)
