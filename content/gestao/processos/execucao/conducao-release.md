---
title: Condução da release
date: 2022-04-29T19:15:41-03:00
draft: false
---

![Processo de condução da release](/gestao/processos/imagens/processo-de-conducao-da-release.png)

## Descrição

Toda release, ao ser iniciada, deve ser planejada pela equipe. Utilize as análises das demandas para determinar quais serão atendidas na release.

## Fase

- [Fase de Execução](/gestao/processos/execucao)

## Processos pai

- [Processo de construção do produto](/gestao/processos/execucao/construcao-produto)

## Sub-processos

- [Processo de condução da sprint](/gestao/processos/execucao/conducao-sprint)
- [Processo de implantação](/gestao/processos/implantacao/implantacao)
- [Processo de planejamento da release](/gestao/processos/planejamento/planejamento-release)

## Tarefas

- [Finalizar demanda](/gestao/processos/tarefas/finalizar-demanda)

## Links Úteis

- [Checklist Release](/design/checklists/release)
