---
title: Condução da sprint
date: 2022-05-02T09:30:12-03:00
draft: false
---

![Processo de condução da sprint](/gestao/processos/imagens/processo-de-conducao-da-sprint.png)

## Descrição

Toda sprint, ao ser iniciada, deve ser planejada pela equipe considerando as prioridades determinadas na etapa de planejamento da release.

Ao final de cada sprint, a equipe deve realizar os eventos de revisão e retrospectiva.

## Fase

- [Fase de Execução](/gestao/processos/execucao)

## Processos pai

- [Processo de condução da release](/gestao/processos/execucao/conducao-release)

## Sub-processos

- [Processo de desenvolvimento dos artefatos](/gestao/processos/execucao/desenvolvimento-artefatos)
- [Processo de planejamento da sprint](/gestao/processos/planejamento/planejamento-sprint)

## Tarefas

- [Reunião de retrospectiva da sprint](/gestao/processos/tarefas/reuniao-retrospectiva-sprint)
- [Reunião de revisão da sprint](/gestao/processos/tarefas/reuniao-revisao-sprint)
