---
title: Desenvolvimento dos artefatos
date: 2022-05-02T09:51:00-03:00
draft: false
---

![Processo de desenvolvimento dos artefatos](/gestao/processos/imagens/processo-de-desenvolvimento-dos-artefatos.png)

## Descrição

O processo de desenvolvimento dos artefatos é quando, de fato, a equipe desenvolve as soluções. Baseados nas análises das demandas realizadas previamente, designers e desenvolvedores trabalham de conjunto para propor a melhor solução que atenda à necessidade dos usuários. A fase de desenvolvimento é dividida em diversos fases que devem ser respeitadas com atenção.

## Fase

- [Fase de Execução](/gestao/processos/execucao)

## Processos pai

- [Processo de condução da sprint](/gestao/processos/execucao/conducao-sprint)

## Sub-processos

- [Processo de implantação](/gestao/processos/implantacao/implantacao)

## Tarefas

### Criação

- [Atualizar UIKIT](/gestao/processos/tarefas/atualizar-uikit)
- [Criar branch no GIT](/gestao/processos/tarefas/criar-branch-git)
- [Design review](/gestao/processos/tarefas/design-review)
- [Elaborar documentação de diretrizes](/gestao/processos/tarefas/elaborar-documentacao-diretrizes)
- [Elaborar documentação visual](/gestao/processos/tarefas/elaborar-documentacao-visual)
- [Entendimento com o desenvolvedor](/gestao/processos/tarefas/entendimento-desenvolvedor)
- [Entregar backlog](/gestao/processos/tarefas/entregar-backlog)
- [Esboçar protótipo](/gestao/processos/tarefas/esbocar-prototipo)
- [Idealizar solução](/gestao/processos/tarefas/idealizar-solucao)
- [Merge request](/gestao/processos/tarefas/merge-request)
- [Reportar resultado do design review](/gestao/processos/tarefas/reportar-resultado-design-review)
- [Solicitar validação de diretrizes](/gestao/processos/tarefas/solicitar-validacao-diretrizes)
- [Solicitar validação do protótipo](/gestao/processos/tarefas/solicitar-validacao-prototipo)
- [Teste de hipótese](/gestao/processos/tarefas/teste-hipotese)
- [Teste intrínseco (heurística)](/gestao/processos/tarefas/teste-intrinseco)
- [Validar documentos](/gestao/processos/tarefas/validar-documentos)
- [Validar UIKIT](/gestao/processos/tarefas/validar-uikit)

### Implementação

- [Criar branch no GIT](/gestao/processos/tarefas/criar-branch-git)
- [Elaborar documentação de código](/gestao/processos/tarefas/elaborar-documentacao-codigo)
- [Entendimento com o designer](/gestao/processos/tarefas/entendimento-designer)
- [Entregar backlog](/gestao/processos/tarefas/entregar-backlog)
- [Executar pipeline](/gestao/processos/tarefas/executar-pipeline)
- [Gerar ambiente de design review](/gestao/processos/tarefas/gerar-ambiente-design-review)
- [Gerar ambiente de pré-producao](/gestao/processos/tarefas/gerar-ambiente-pre-producao)
- [Implementar cenário de teste](/gestao/processos/tarefas/implementar-cenario-teste)
- [Implementar solução](/gestao/processos/tarefas/implementar-solucao)
- [Merge na branch main](/gestao/processos/tarefas/merge-branch-main)
- [Merge request](/gestao/processos/tarefas/merge-request)
- [Reportar resultado da validação da diretriz](/gestao/processos/tarefas/reportar-resultado-validacao-diretriz)
- [Reportar resultado da validação do protótipo](/gestao/processos/tarefas/reportar-resultado-validacao-prototipo)
- [Solicitar design review](/gestao/processos/tarefas/solicitar-design-review)
- [Validar código](/gestao/processos/tarefas/validar-codigo)
- [Validar diretriz](/gestao/processos/tarefas/validar-diretriz)
- [Validar protótipo](/gestao/processos/tarefas/validar-prototipo)
