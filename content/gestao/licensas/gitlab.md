---
title: Gitlab
description: Guia de renovação da licença do gitlab.com
---

- Licença: [GitLab for Open Source](https://about.gitlab.com/solutions/open-source/)
- Período de renovação: Anual
- Renovar antes de: 21 de novembro

## Como renovar

1. Acesse o site <https://about.gitlab.com/solutions/open-source/join/>
2. Clique o botão "Join the Program" e em seguida "Apply Now".
3. Preencha o formulário, lembrando que o texto deve estar em língua inglesa. Sugestões de preenchimento:
   - Campo "Project Description": ""Interface standards that must be followed by designers and developers to ensure standardization of the portals of Brazilian federal public agencies and simplify the information offering interfaces for the citizen."
   - Campo "Additional Details (opcional)": "We are a public company that technically leads the development and maintenance of the interface standard for the entire Brazilian government within an intergovernmental committee, without seeking any kind of profit from this solution."
4. Após preencher todos os campos, clicar no botão "Submit".
5. É solicitado o envio de 3 screenshots do repositório. Siga o [link de orientação](https://docs.gitlab.com/ee/subscriptions/community_programs.html#gitlab-for-open-source) para mais detalhes.
   - Para um dos screenshots, é necessário ser admin no repositório Gitlab para ter acesso às configurações do projeto.

### Fim do prazo

Caso não receba a resposta do GitLab até a data limite informada no formulário, no e-mail <govbr-dssl@serpro.gov.br>, abra um ticket no site <https://support.gitlab.com/hc/en-us> usando a conta do govbr-ds.

Sugere-se verificar os tickets anteriores para ter uma melhor ideia de como preencher e formatar o novo ticket de suporte.
