---
title: 'Licenças'
chapter: true
weight: 1
pre: '<i class="fas fa-certificate"></i> '
---

Nessa seção colocamos alguns guias explicando em passos simples e diretos como renovar as licenças das ferramentas que usamos no projeto.
