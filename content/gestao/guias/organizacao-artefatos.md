---
title: Guia para organização dos conteúdos
description: Guia para organização dos conteúdos do Design System do Governo Federal
date: 02/08/2023
keywords: guia, organização, conteúdos, artefatos, arquivos, design system, repositórios, categorias, classificação
---

Devido a quantidade e variedade de artefatos produzidos pelo time no desenvolvimento do Design System, e também pela dificuldade em identificar o melhor local para armazená-los e apresentá-los, criamos esse guia com as orientações sobre as responsabilidades dos principais repositórios. Isso deve ajudar a entender e escolher onde armazenar e apresentar os novos artefatos do Design System do Governo Federal.

Abaixo, apresentamos como organizamos os conteúdos do Design System, tanto em termos de uso pelo time quanto em termos de entrega/consumo.

Para apoiar na tomada de decisão, criamos algumas categorias para classificar os tipos de arquivos produzidos:

## Categorias de artefatos produzidos

| Categoria                      | Descrição                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| ------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Informação                     | Qualquer conteúdo com o intuito de informar ou dar ciência sobre um determinado assunto.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| Restrição                      | Qualquer conteúdo que possuam regras que devem ser seguidas                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| Arquivos essenciais do projeto | São arquivos obrigatórios que fazem parte da estrutura do projeto e que não são entregas do DS                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| Orientação                     | Conteúdos que visam orientar ou instruir a comunidade.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| Arquivos Fontes                | São arquivos que fazem parte e estão ligados a entregas do produto.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| Ferramentas                    | Qualquer ferramenta utilizada no projeto, seja criada pelo time ou de terceiros.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| Modelos                        | Um modelo é a representação visual de um fundamento, de um componente ou de um padrão, que pode ser utilizado como um exemplo visual ou como um ponto de partida para criação de uma interface contendo uma estrutura predefinida de forma que facilite o desenvolvimento do conteúdo. Em resumo, representa um elemento composto por componentes, uma tela ou fluxo de navegação, e serve de base para construção de uma interfaces para os usuários. Os modelos são construídos levando em consideração todas as regras básicas de uso dos componentes, fundamentos e padrões. Como todas as regras já estão aplicadas, o processo de criação de telas são agilizados. No GOVBR-DS é disponibilizado alguns modelos focados em padrões de tela descrevendo uma funcionalidade especifica, ou um fluxo. Muitas pessoas conhecem esse tipo de modelo como template. Além disso, disponibilizamos modelos de componentes dentro das diretrizes (exemplos interativos) e em bibliotecas de componentes (os UIKits), isto é, um conjunto de modelos organizados em um mesmo artefato. Exemplo de modelos: Tela de sessão de Usuário, Tela de Erro, Fluxo de CRUD, etc. |

## Ferramentas para Distribuição de Conteúdos e suas Responsabilidades

| Ferramenta                       | Descrição                                                                                                                                                                                                                                                                           | Características e Restrições                                                                                                                                                    |
| -------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Site                             | O Site deve ser utilizado para apresentar os conteúdos relativos ao consumo do DS pelos usuários como por exemplo: Informátivos sobre mudanças e novidades, assuntos que impactam a criação de soluções, diretrizes a serem seguidas para o desenvolvimento de soluções de governo. | Maior complexidade na manutenção dos conteúdos e necessidade de versionamento. Deve exibir conteúdos de caráter informativo que estejam relacionados a uma versão do DS.        |
| Biblioteca de Componentes - Core | Arquivos necessários para desenvolvimento do DS nas questões de Frontend como arquivos, JavaScript, css, sass, arquivos que os usuários irão utilizar e outros obrigatórios, a exemplo do package.json, etc...                                                                      | Arquivos versionados no GitLab.com que estão relacionados a uma versão do DS.                                                                                                   |
| Wiki / Hugo                      | A Wiki deverá ser utilizada para compartilhamento de conhecimentos entre o time. Recomendado para conteúdos que podem sofrer mudança constante.                                                                                                                                     | Simplifica a gestão dos conteúdos, Não possui nenhuma influência com determinada versão do DS, Menor complexidade na manutenção dos conteúdos, Não exige a criação de releases. |
| Youtube                          | Qualquer vídeo que não integre a documentação oficial(exemplo o fundamento motion).                                                                                                                                                                                                 | Fácil distribuição, Simplifica a gestão de conteúdos, não exige versionamento.                                                                                                  |
| Medium                           | Deve ser utilizada para publicação de Artigos com o intuito de orientar, que não fazem parte das diretrizes ou processo do Design System.                                                                                                                                           | Fácil distribuição, Simplifica a gestão de conteúdos, não exige versionamento.                                                                                                  |

## Tipos de Conteúdos utilizados nas Ferramentas

Abaixo temos os tipos de conteúdos que podem ser disponibilizados em cada ferramenta de distribuição.

### Site

- Princípio do DS
- Fundamentos de Design
- Padrões de Design
- Artefatos de Design para prototipação
- Artefatos de Design exemplos
- Orientações
- Boas práticas de desenvolvimento
- Guias/Tutoriais
- Legislação/normativos
- Manuais e Guias em PDF para download
- Diretriz de design para componentes
- Diretriz de dev para componentes
- Diretriz de dev para consumo e documentação de utilitários de css
- Diretriz de dev para consumo e documentação de utilitários JS
- Exemplos Implementados Dev, consumo código
- RNF/Orientação
- Tutoriais
- Atalhos para algum conteúdo/ Landing pages
- Config.json

### Core

- HTML
- PUG estrutura
- PUG Variações (Examples)
- SASS
- JS
- CSS
- Imagens ligadas aos componentes
- Estrutura e arquivos obrigatórios
- Licenças
- Arquivos de configuração
- Arquivos de validação Linter, hint
- Arquivos de build
- Arquivos voltados para questões da comunidade
- Templates/Exemplos
- Changelog

### Wiki

- FAQ`s
- Guias
- Guias de Analises de Risco e de Impacto
- Critérios de Analise
- Critérios de Classificação
- Regras
- Problemas e Soluções
- Informativos (*Contém decisões do projeto, versionamento semantico informações sobre arquitetura, etc...*)
- Políticas (*Regras, Normas de Conduta da Comunidade e Contribuição*)
- Links para Lints
- Referências (*Iniciativas*)
- Boas Práticas (*Evolução do Design System*)
- Tutoriais
- Checklists
- Modelos de documentos
- Plugins de Design
- Ferramentas
- Processos
- Fluxo de Trabalho
- Glossário
- Dicionário de Tokens
- Changelog

## Organização de Sub-Grupos e Projetos no GitLab

A seguida temos uma proposta de organização de Sub-Grupos e Projetos para uma melhor organização do GitLab.

- ***GOVBR-DS*** (*Grupo principal do Projeto*)

  - **Configurações Gerais do Projeto** (*Arquivos de configurações Gerais do repositório do Padrão Digital de Governo.*)

    - Commit Config (*Padrão de commits para projetos do GOVBR-DS.*)

    - Configurações de Release (*Padrão de release para projetos do GOVBR-DS.*)

  - **Gitlab Templates** (*Grupo para armazenamento de Templates de Projetos utilizados na criação de novos projetos no Gitlab do GOVBR-DS.*)

    - Design Source Template
    - Developer Template
    - Documentation Template

  - **Padrão Digital de Governo - DS (Base)** (*Núcleo do Padrão Digital de Governo onde concentramos todos os artefatos associados a versão do DS.*)

    - Desenvolvimento (*Projetos de Desenvolvimento do Padrão Digital de Governo.*)

      - Biblioteca de Componentes - Core (*Biblioteca de Componentes Agnóstica do Padrão Digital de Governo.*)
      - Diretrizes de Desenvolvimento (*Projeto contendo as diretrizes de desenvolvimento compiladas para disponibilização no site.*)

    - Design (*Projetos com artefatos de Design do Padrão Digital de Governo.*)

      - Arquivos Fontes de Design (*Projeto contendo os arquivos fontes de design do Padrão Digital de Governo.*)

        - Componentes (*Projeto contendo os arquivos fontes com as especificações dos Componentes do DS.*)
        - Fundamentos (*Projeto com os arquivos fontes dos Fundamentos de Design.*)
        - Padrões (*Projeto contendo os arquivos fontes com os Padrões do DS.*)

      - Diretrizes de Design (*Projeto contendo as diretrizes de design compiladas para disponibilização no site.*)

      - Ilustrações (*Projeto contendo arquivos com ilustrações de Design.*)

      - UIKits para prototipação (*Projeto de uikits do Padrão Digital de Governo.*)

    - Qualidade (*Projetos utilizados na etapa de qualidade do Padrão Digital de Governo.*)

      - Testes Frontend (*Projeto de testes Frontend do Padrão Digital de Governo*)

    - Quickstarts (Início rápido) (*Repositório com projetos básicos de Design e Frontend consumindo o Padrão Digital de Governo.*)

      - Desenvolvimento (*Projetos Básicos de apoio na etapa de Desenvolvimento.*)
        - Angular
      - Design (*Projetos Básicos de apoio na etapa de Design.*)
        - Modelos

    - Acessibilidade (*Orientações e recomendações sobre Acessibilidade*)
    - Writing (*Padrões e Princípios de Writing*)
    - Tokens (*Projeto contendo os Tokens compartilhados no Design System*)

  - **Projetos e Ferramentas de Apoio** (*Projetos e Ferramentas utilizadas como apoio no desenvolvimento e entrega do Padrão Digital de Governo.*)

    - Extensões (*Grupo com extensões de ferramentas utilizadas no desenvolvimento do Padrão Digital de Governo.*)

      - AdobeXD
      - Luminance

    - Site (*Grupo com artefatos relacionados ao Site do DS (solução tecnológica e Landing Pages)*)

      - Conteúdo (*Repositório com as Landing Pages do Site*)
      - Layouts
      - Manuais (*Repositório com os manuais de governo para download.*)
      - Site Angular (*Solução tecnológica utilizada no site do Padrão Digital de Governo - DS.*)

    - Wiki (*Grupo com artefatos relacionados a wiki (solução tecnológica e conteúdo)*)
      - Conteúdo (*Conteúdos da Wiki*)
      - Hugo CMS (*CMS utilizado para disponibilização da Wiki.*)

  - **Protótipos** (*Os repositórios nesse grupo são apenas para testes e NÃO devem ser usados em produção. Usado apenas para testes de ferramentas, tecnologias, etc...*)
  - **React** (*Biblioteca de componentes do DS implementada em React*)

    - Componentes React (*Biblioteca de componentes React baseado no GOVBR-DS.*)

  - **WebComponents** (*Biblioteca de componentes do DS implementada no padrão WebComponents.*)
    - Quickstarts (início rápido) (*Projetos básicos demonstrando o consumo da biblioteca de WebComponents em diversas tecnologias.*)
      - Angular (*Projeto exemplificando o uso da biblioteca de Web Components do GOVBR-DS em projetos Angular.*)
      - JS (*Projeto exemplificando o uso da biblioteca de Web Components do GOVBR-DS em projetos JavaScript.*)
      - React (*Projeto exemplificando o uso da biblioteca de Web Components do GOVBR-DS em projetos React.*)
      - Vue (*Projeto exemplificando o uso da biblioteca de Web Components do GOVBR-DS em projetos Vue.*)
    - Web Components (*Biblioteca de Web Components.*)
  - Backlog
  - Gitlab Config(*Configurações para insights, issues e merge requests.*)
  - GOVBR-DS-ADR (Repositório onde versionamos as decisões arquiteturais do projeto que tragam impactos ao projeto, para que possam ser consultadas sempre que necessários.)

**OBS:** Este documento contém orientações, mas não deve ser a única fonte de verdade. Se surgir uma nova necessidade que não tenha sido identificada durante a análise, pode conversar com o time e tomar uma nova decisão. É importante que, cada vez que uma decisão for tomada, este documento seja atualizado, para que as pessoas que vieram depois saibam onde guardar os novos arquivos.
