---
title: Análise de Impacto
description: Recomendações para Análise de Impacto
date: 13/04/2022
keywords: analise, impacto, serviços, apps, componente, atividade
---

Entendendo que **Design System** é um produto, ele vai servir de base para outros produtos serem gerados. Essa é a função fundamental de um Design System. Tendo em mente as etapas de pesquisa, criação, validação e documentação, é fundamental respeitar cada uma delas. Parte do processo é voltado para criar uma biblioteca de elementos visuais, nessa etapa, o foco é padronizar os elementos, as cores, os textos, os modais, os ícones e tudo aquilo que irá compor a aplicação.

Como é possível perceber, um Design System é um amaranhado de fundamentos e componentes conectados entre si. Cada componente mais complexo utiliza outros componentes mais simples e estes, fundamentos visuais. Desta forma, é muito complexo gerir um Design System muito grande. Por isso é extremamente necessária uma análise de impactos sempre que houver alteração ou evolução de um artefato. É bom possível que, qualquer modificação em um elemento reflita em outros. Para manter o Design System sempre atualizado, é imprescindível realizar uma boa análise de impactos.

Utilize o [Mapa de Classificação de Dependências]({{% relref "/gestao/guias/criterios-analise-classificacao" %}}) para consultar o níveis de dependências entre os elementos. Se for preciso, consulte os documentos específicos de cada componente envolvido para extrair mais detalhes sobre os possíveis impactos gerados.
