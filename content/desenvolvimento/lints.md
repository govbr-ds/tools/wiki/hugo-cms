---
title: Lints
description: Ferramentas Lints
pre: '<i class="fas fa-file-code"></i> '
date: 13/07/2021
keywords: ferramentas, configuração, lints, implementa, implementação, tutoriais, ferramenta
---

Os lints são **obrigatórios** para todos os projetos do GOVBR-DS. Se algum projeto não implementa essas ferramentas ele deverá priorizar a implementação e configuração.

Os projetos são responsáveis por seguir os tutoriais específicos de cada ferramenta para seu uso correto.

## Padrão para TODOS os projetos

Essas ferramentas já tem um modelo de configuração os projetos do grupo GOVBR-DS. As configurações estão na mesma pasta dessa página e podem ser configuradas caso seja necessário, mas o uso dessas ferramentas é **obrigatório**.

1. [lint-staged](https://github.com/okonet/lint-staged)
1. [husky](https://github.com/typicode/husky)
1. [commitizen](https://github.com/commitizen/cz-cli)
1. [prettier](https://github.com/prettier/prettier)

Junto com configurações para as ferramentas acima, colocamos configurações para as IDEs e git.

## Específicos para cada projeto

Essas ferramentas exigem que os projetos façam as configurações específicas as tecnologias utilizadas.

1. [standard-version](https://github.com/conventional-changelog/standard-version)
1. [semantic-release](https://github.com/semantic-release/semantic-release)
1. [stylelint](https://github.com/stylelint/stylelint)
1. [eslint](https://github.com/eslint/eslint)
1. [html-validade](https://gitlab.com/html-validate/html-validate)
