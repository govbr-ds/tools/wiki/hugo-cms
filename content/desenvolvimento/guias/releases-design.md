---
title: Releases de Design
description: Este documento lista as orientações para publicação de releases de design
date: 21/07/2021
keywords: release, publicação, versionamento, changelog, pacote
---

As documentações são publicadas depois de liberadas pela equipe de design. Observe que em alguns casos elas devem ser incluídas em uma branch feat/release-rxsx. Observe a tarefa para saber onde ela deve ser publicada.

O processo é manual e **requer atenção**.

## Releases

Quando alguma release de Design é gerada a equipe envia os documentos para os desenvolvedores publicarem.

As documentações e arquivos de design são hospedados no Serpro Drive e a documentação em formato markdown está na pasta **Diretriz**.

## Substituição

A pasta diretriz contém arquivos markdown e pasta de imagens. Na pasta do projeto correspondente a documentação a ser atualizada, remova tudo exceto a **documentação-dev.md**.

Depois copie o conteúdo da pasta diretriz para dentro da pasta do projeto.

Em uma atualização do componente Button por exemplo, teríamos os seguintes arquivos:

- imagens/
- button.md
- button-dsg.md

## Ajustes

Precisamos agora extrair a descrição do arquivo e realizar os ajustes de acordo com as regras do Markdown.

Componentes e Templates:

1. Renomeie o arquivo **componente.md** para **componente-dsg.md**
2. Extraia a descrição (parte imediatamente abaixo do título) para um arquivo **componente.md**

Para todas as documentações:

1. Mantenha o comentário da primeira linha com a numeração da versão
2. Delete o título (exemplo: # Button)
3. Execute um lint (versão online ou extensão do VSCode) nos arquivos para garantir que eles seguem as recomendações da linguagem Markdown

## Config

Caso seja um arquivo novo ou que teve seu nome alterado, ajuste o arquivo **config.json** na raiz do projeto para que aponte para a pasta correta.

## Atualizar changelog

Atualizar o changelog do govbr-ds com o de design seguindo os [padrões de design](/design/modelos-documento/#changelog)

## Links do UIKit

Os links do UIkit ficam na parte de download dentro do nosso site. É necessário apenas substituir o link da página de Downloads no site para atualizar para o novo UIKit.

O arquivo do UIkit fica hospedado no SERPRO Drive. Para conseguir o link que deve ser colocado no site siga os seguintes passos:

1. Copie o link disponibilizado pela equipe de design
2. Substitua o link na página de Downloads no Site pelo novo link
