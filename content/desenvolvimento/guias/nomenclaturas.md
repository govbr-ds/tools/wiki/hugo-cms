---
title: Nomenclaturas
description: Padrões de nomenclaturas
date: 19/07/2021
keywords: padrão, nomenclaturas, componente, fundamento, html, css, js
---

O padrão de nomenclatura deve ser seguido por todos, indepentente do trabalho a ser feito. Caso haja a necessidade de se desviar do padrão é **obrigatório** que o assunto seja antes discutido com a equipe.

- Fundamento: Em **português**, pode ser no singular ou plural.
- Componente: Em **inglês** no Singular.
- Padrão: Em **inglês ou português** (dependendo de como o termo é mais usado pela comunidade) no singular ou plural.

Os nomes em inglês podem ter o nome do termo em português entre parênteses.

- Collapse (Retrair/Expandir)
- DropDown (Elementos Flutuantes)

As tabelas de *token* devem vir com o par **token: valor**.

Nos códigos HTML, JS e CSS deve ser dada a preferência para nomenclaturas em **inglês**, mas todos os comentários devem ser feitos em **português**.
