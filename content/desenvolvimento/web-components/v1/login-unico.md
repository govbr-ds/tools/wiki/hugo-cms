---
title: Entrar com gov.br
description: Orientações de uso do componente Sign-In (Entrar com gov.br)
---

Essa documentação é um complemento do [Roteiro de Integração do Login Único](https://manual-roteiro-integracao-login-unico.servicos.gov.br/pt/stable/iniciarintegracao.html) em que mostramos como fazer a integração usando os Web Components do GovBR-DS.

O componente Sign-In utilizado como exemplo, foi implementado no padrão Web Components seguindo todas as especificações conforme documentação do componente [Sign-in do GovBR-DS](https://www.gov.br/ds/components/signin?tab=desenvolvedor).

Guia criado conforme versão 1.x dos Web Componentes GovBR-DS. Se estiver utilizando uma versão diferente, por favor consultar a documentação correspondente.

Os Web Components são compatíveis com os principais frameworks de mercado, abaixo listamos algumas vantagens em adotar esse padrão:

- Os estilos visuais que já são aplicados automaticamente, garantindo a consistência visual com o Padrão Digital de Governo.

- Os estilos ficam isolados internamente ao componente o que evita impactos no visual do seu sistema.

- Quantidade de código a copiar é menor.

## Quickstarts

Foram disponibilizados alguns projetos de exemplo, demonstrando como utilizar a biblioteca de Web Components nas tecnologias: Angular, React, Vue e JS. Esses projetos podem ser usados como pontos de partida para novos projetos ou como consulta para entender melhor o uso da biblioteca nessas tecnologias.

Consulte os quickstarts [no repositório do gitlab.com](https://gitlab.com/groups/govbr-ds/wbc/quickstarts/).

## Polyfills

Caso os requisitos do seu projeto incluam navegadores que não suportam os Web Components, consulte a seção sobre [polyfills](https://gitlab.com/govbr-ds/wbc/govbr-ds-wbc#polyfill).

## Como usar o componente Sign-in

Nos passos a seguir demonstramos como usar o componente sign-in de uma forma geral, pois cada framework possui especificidades no consumo dos componentes no padrão Web Components.

Essa seção não elimina a necessidade de uma pesquisa sobre como fazer uso dos webcomponents na tecnologia adotada em seu projeto. A ideia aqui é apenas passar orientações gerais sobre alguns conceitos.

### Passo 1 - Leia o README

No arquivo [README](https://gitlab.com/govbr-ds/wbc/govbr-ds-wbc/-/blob/main/README.md) você encontra o passo a passo sobre como usar a biblioteca e informações do código do projeto.

### Passo 2 - Acesse o Storybook

Ao acessar o [site dos webcomponents](https://gov.br/ds/webcomponents/?path=/story/componentes-signin) na versão interativa do componente sign-in, é possível consultar todas as propriedades e variações disponíveis, com a possibilidade de copiar o código do componente.

### Passo 3 - Use o componente

Avaliar o componente e suas propriedades, copie o HTML para seu projeto e faça as adaptações necessárias conforme definição dos requisitos.

A chamada para autenticação deverá ocorrer pelo componente sign-in com o conteúdo Entrar com GOV.BR. Para mais detalhes sobre o formato do botão, seguir as orientações do [Design System](https://gov.br/ds) e [Web Components](https://gov.br/ds/webcomponents).

Exemplo de como ficará o código do componente:

```html
br-sign-in type="primary" density="middle" label="Entrar com" entity="GOV.BR" >
```

### Passo 4 - Avalie como usar um Web Component no seu projeto

Como dito anteriormente o uso de um Web Component, varia conforme as tecnologias utilizadas. Pesquise como fazer a integração dos webcomponents com seu projeto, para garantir que os componentes estão sendo renderizados corretamente antes de prosseguir para os próximos passos.

### Passo 5 - Use o evento de clique

O componente pode ser usado através do evento de clique, mas a forma como cada framework utiliza esse evento pode variar de projeto para projeto. No componente br-sign-in, você deve capturar o evento de clique de forma que, ao usuário clicar sobre o botão, ele seja direcionado para o fluxo de login no gov.br.

Exemplo:

```html
br-sign-in type="primary" density="middle" label="Entrar com" entity="gov.br" onClick="login()" >
```

### Passo 6 - Siga o Roteiro de Integração do Login Único

Após conseguir adicionar o componente Sign-In ao seu projeto, utilize o [Roteiro de Integração do Login Único[\_ para fazer a integração do gov.br com sua aplicação.

## Observações finais

O objetivo deste documento é fornecer uma orientação geral sobre o uso do componente sign-in em projetos que irão integrar com o gov.br, sem abordar detalhes específicos de cada tecnologia.

Fique à vontade para nos enviar sugestões ou críticas sobre a biblioteca de componentes ou algum dos quickstarts através do [discord](https://discord.gg/NkaVZERAT7).

## Dúvidas e sugestões

Se tiver alguma dúvida sobre como adotar o componente, sugerimos que participe da comunidade no [discord](https://discord.gg/NkaVZERAT7) e envie as dúvidas por lá ou procure as comunidades específicas das tecnologias ou frameworks adotados em seu projeto.
