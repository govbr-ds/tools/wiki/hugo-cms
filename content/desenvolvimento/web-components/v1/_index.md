---
title: 'Versão 1.x (VueJS)'
chapter: true
weight: 1
---

Essa seção é dedicada as documentações do projeto **@govbr-ds/webcomponents**, na versão **V1** construída em VueJS. Essa versão foi descontinuada e não está mais sendo mantida. Para acessar a versão atual do projeto, acesse a documentação da versão **V2** que está sendo desenvolvida utilizando a biblioteca [**Stencil**](https://stenciljs.com/).
