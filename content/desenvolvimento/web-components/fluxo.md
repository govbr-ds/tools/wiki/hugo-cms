---
title: Fluxo de trabalho
description: Explicação sobre nosso fluxo de trabalho dentro dos Web Components
---

```mermaid
graph TD
    A(( Issues )) --> B[Time escolhe e define responsáveis]
    B --> Z[Análise inicial das issues]
    Z -->|Precisa de especificação| C[Cria especificação]
    Z -->|Não precisa de especificação| D[Organizar épicos e issues]
    C --> D[Organizar épicos e issues]
    D --> I
    D --> E[Envia especificação para time]
    E --> F[Notifica time]
    F -->|Prazo de 2 dias| G[Discussão com o time]
    G --> H[Revisar especificação conforme feedback do time]
    H --> I[Definir escopo da sprint]
    I --> J[Executar atividade]
    J --> K[Testar]
    J --> L[Documentar]
    K --> M[Abrir MR]
    L --> M
    M --> N[Responsável notifica revisores]
    N --> O[Revisores avaliam MR]
    O <--> P[Responsável corrige problemas encontrados]
    O --> Q(( MR aceito ou fechado ))
    linkStyle 2 stroke:#1E90FF,stroke-width:2px;
    linkStyle 3 stroke:#32CD32,stroke-width:2px;
    linkStyle 4 stroke:#1E90FF,stroke-width:2px;
    linkStyle 5 stroke:#32CD32,stroke-width:2px;
    linkStyle 6 stroke:#1E90FF,stroke-width:2px;
    linkStyle 7 stroke:#1E90FF,stroke-width:2px;
    linkStyle 8 stroke:#1E90FF,stroke-width:2px;
    linkStyle 9 stroke:#1E90FF,stroke-width:2px;
    linkStyle 10 stroke:#1E90FF,stroke-width:2px;
    click A href "#issues"
    click B href "#time-escolhe-e-define-responsaveis"
    click C href "#cria-especificacao"
    click D href "#organizar-epicos-e-issues"
    click E href "#envia-especificacao-para-time"
    click F href "#notifica-time"
    click G href "#discussao-com-o-time"
    click H href "#revisar-especificacao-conforme-feedback-do-time"
    click I href "#definir-escopo-da-sprint"
    click J href "#executar-atividade"
    click K href "#testar"
    click L href "#documentar"
    click M href "#abrir-mr"
    click N href "#responsavel-notifica-revisores"
    click O href "#revisores-avaliam-mr"
    click P href "#responsavel-corrige-problemas-encontrados"
    click Q href "#mr-aceito-ou-fechado"
    click Z href "#analise-inicial-das-issues"
    subgraph Responsável
        Z
        C
        E
        F
        D
    end
    subgraph Reunião de Planejamento
        G
        H
        I
    end
    subgraph Desenvolvimento
        J
        K
        L
        M
    end
    subgraph Merge Request
        N
        O
        P
        Q
    end
```

## Issues {#issues}

As atividades são organizadas em issues no repositório do projeto. Elas são classificadas usando labels, entre elas as de `prioridade::` para indicar a prioridade de cada issue.

## Time escolhe e define responsáveis {#time-escolhe-e-define-responsaveis}

O time escolhe algumas issues priorizadas e define os responsáveis por cada uma delas. Essa etapa é crucial para garantir que as tarefas sejam distribuídas de acordo com as habilidades e disponibilidade de cada membro do time. A escolha pode ser feita em uma reunião **rápida** ou de forma assíncrona.

## Análise inicial das issues {#analise-inicial-das-issues}

Antes de especificar as issues, é feita uma análise superficial para entender o contexto e a viabilidade das tarefas. Essa análise ajuda a identificar possíveis impedimentos e a planejar melhor as próximas etapas do desenvolvimento. Nessa etapa é decidido se a issue precisa de uma especificação detalhada ou se pode ser feita sem ela.

## Cria especificação {#cria-especificacao}

A especificação é feita em uma issue, com a descrição da tarefa, critérios de aceitação e informações relevantes. Pode ser que a issue seja dividida em issues menores relacionadas entre si. O template de especificação está disponível no formato de issue e pode ser customizado conforme a necessidade.

- Caso a issue seja dividida em issues menores, a issue principal deve conter a descrição geral da tarefa e as issues menores devem conter as especificações detalhadas de cada parte da tarefa.
- Cada subcomponente deve ser separado em uma issue diferente, com a descrição da tarefa, informações relevantes e relacionadas com a issue `pai`.

Para criação de componente use o template `Especificação Componente` do gitlab. Para bug use `Bug Tracking` e para outras como novas features use o template `Default` preenchendo conforme a necessidade da tarefa.

## Organizar épicos e issues {#organizar-epicos-e-issues}

Organiza o backlog em tópicos maiores (épicos) e tarefas menores (issues). Todas as issues devem ser criadas conforme seus templates e os atributos (labels, milestones, assignees, weight, etc) devem ser preenchidos corretamente. Relacione as issues com os épicos correspondentes e entre si. Todas as decisões e definições devem ser documentadas na issue de especificação.

Sempre que uma issue for criada crie também a sua issue de revisão. Ambas vão ser estimadas e relacionadas entre si.

Lista de issues que **podem** ser criadas:

- Issue de especificação
- Issue para revisão do desenvolvedor
- Issue para revisão do design
- Issue para testes de acessibilidade

Ao final da reunião os responsáveis ficam encarregados de organizar as issues e épicos no Gitlab.

## Envia especificação para time {#envia-especificacao-para-time}

O responsável pela especificação envia o documento para leitura do time, com um prazo mínimo de 2 dias antes da reunião de planejamento. Esse prazo é necessário para que o time leia com cuidado a especificação e, caso ache interessante, faça anotações na issue para iniciar a discussão de forma assíncrona.

## Notifica time {#notifica-time}

Como notificar o time?

- Marque o time em um comentário com os `@usernames`;
- Avise na diária assim que possível.

## Discussão com o time {#discussao-com-o-time}

Conversa entre os membros do time sobre a especificação previamente lida para validação e discussão de ideias. Documente as decisões tomadas e ajuste a especificação da atividade conforme necessário.

O responsável por cada issue deve estar disponível para responder dúvidas e esclarecer pontos durante a discussão. A reunião de planejamento é o momento ideal para discutir as prioridades, riscos e dependências entre as atividades.

## Revisar especificação conforme feedback do time {#revisar-especificacao-conforme-feedback-do-time}

Ajusta a proposta de acordo com os feedbacks e sugestões do grupo. Caso nenhuma alteração tenha sido sugerida pelo grupo essa etapa é opcional.

## Definir escopo da sprint {#definir-escopo-da-sprint}

Seleciona as atividades que serão incluídas na Sprint.

Com base nas issues especificadas o time decide quais issues podem ser feitas durante a sprint e quais devem ser adiadas. A decisão deve ser feita com base no tempo disponível de cada membro do time e na prioridade das issues.

Durante essa etapa cada membro do time deve:

- Avaliar o seu tempo disponível para a sprint;
- Escolher quais issues irá realizar;
- Definir uma estimativa de tempo para realizar cada issue;
-

Colocar todas as issues na sprint correta no Gitlab e atribuir a milestone correta.

## Executar atividade {#executar-atividade}

Implementação dos Web Components conforme as especificações e padrões do Design System. Siga as boas práticas de desenvolvimento para garantir a consistência visual e funcional.

## Testar {#testar}

Realiza testes **unitários**, **end-to-end**, **regressão visual** e de **acessibilidade** para garantir a qualidade e o funcionamento correto dos componentes. Utilize ferramentas de teste automatizado para aumentar a cobertura e a confiabilidade dos testes.

## Documentar {#documentar}

Cria e atualiza a documentação dos componentes, incluindo exemplos de uso, propriedades, métodos e eventos. A documentação deve ser clara e acessível para facilitar o uso e a manutenção dos componentes por outros desenvolvedores.

Entre outros, os arquivos que podem ser criados dependendo da necessidade são:

- `README.md`: Descrição geral do componente, instruções de instalação e uso, exemplos de código, etc;
- `Como migrar.md`: Instruções para migrar de uma versão anterior para a versão atual do componente;
- `Exemplos em dev.md`: Exemplos de uso do componente em desenvolvimento;
- `Pagina no site.md`: Descrição do componente na documentação do Design System.

## Abrir MR {#abrir-mr}

Após concluir a implementação, o responsável pela atividade abre um merge request (MR) para que a revisão seja iniciada. Certifique-se de que todos os testes passaram, o build está passando e a documentação está completa antes de abrir o MR.

## Responsável notifica revisores {#responsavel-notifica-revisores}

Após abrir o MR, o responsável pela atividade notifica os revisores para que a revisão seja iniciada.

Como notificar os revisores?

- Marque os revisores em um comentário com os `@usernames`;
- Avise na diária assim que possível.

Quem são os revisores?

- Desenvolvedor: responsável por revisar o código e a funcionalidade do componente;
- Designer: responsável por revisar o design e a usabilidade do componente;
- Especialista em acessibilidade: responsável por revisar a acessibilidade do componente.

Nem todas as atividades vão precisar de todos os revisores, depende da natureza da atividade. O responsável deve analisar cuidadosamente cada atividade e decidir quais revisores são necessários.

## Revisores avaliam MR {#revisores-avaliam-mr}

O merge request pode ser revisado por um (ou mais) desenvolvedor(es), designer(es) e um especialista(s) em acessibilidade para garantir que o código está de acordo com os padrões e requisitos definidos. Essa revisão inclui verificar a qualidade do código, a funcionalidade e a conformidade com as especificações.

Como notificar o responsável?

- Marque o responsável em um comentário com os `@usernames`;
- Avise na diária assim que possível.

Onde registrar as revisões?

- Adicione comentários no MR com feedbacks, sugestões e correções;
- Marque os pontos que precisam ser corrigidos diretamente no código.

## Responsável corrige problemas encontrados {#responsavel-corrige-problemas-encontrados}

O desenvolvedor responsável pelo merge request corrige quaisquer problemas ou melhorias sugeridas durante a revisão. Após as correções, o merge request é revisado novamente até que esteja pronto para ser mesclado.

## MR aceito ou fechado {#mr-aceito-ou-fechado}

O merge request é aceito e mesclado ou é fechado se não puder ser mesclado por algum motivo.
