---
title: Flexibilidade
description: Guia sobre como flexibilizar os web components
---

Cada usuário possui necessidades específicas e requer que os componentes sejam utilizados de maneiras distintas. Para garantir que os componentes atendam a uma ampla gama de requisitos dos usuários, é fundamental torná-los flexíveis.

Essa flexibilidade significa proporcionar aos usuários a liberdade e a responsabilidade de implementar suas necessidades específicas. Embora a implementação padrão que oferecemos esteja sempre em conformidade com o Padrão Digital de Governo, é possível que o usuário precise fazer ajustes que resultem em um desvio desse padrão.

Para flexibilizar os componentes com sucesso, devemos observar os seguintes pontos:

1. Avaliar, durante a especificação, quais aspectos podem ser flexibilizados, como áreas de conteúdo e áreas de ação.
2. Definir se utilizaremos `slots`, `props` ou ambos.
3. Estabelecer conteúdos/valores padrão para os `slots` e `props`.
