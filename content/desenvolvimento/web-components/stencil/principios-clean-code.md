---
title: Princípios do Clean Code
description: Lista os princípios do clean code
---

No desenvolvimento de software, a qualidade do código é fundamental para a manutenção, escalabilidade e eficiência dos sistemas. Um código de alta qualidade não só facilita a correção de bugs e a adição de novas funcionalidades, mas também promove a colaboração eficaz entre desenvolvedores. O conceito de "código limpo" foi amplamente popularizado por Robert C. Martin, também conhecido como Uncle Bob, em seu influente livro "Clean Code: A Handbook of Agile Software Craftsmanship". Este conjunto de diretrizes e boas práticas visa orientar os desenvolvedores a escreverem código que seja legível, manutenível e fácil de entender.

## A Importância do Código Limpo

Escrever código limpo é essencial por várias razões:

- **Manutenção Facilitada**: Código limpo é mais fácil de entender e modificar, reduzindo o tempo e esforço necessários para realizar ajustes e melhorias.
- **Menor Incidência de Erros**: Com uma estrutura clara e lógica, a probabilidade de introduzir erros é reduzida.
- **Facilidade de Colaboração**: Desenvolvedores conseguem trabalhar juntos de maneira mais eficiente quando o código é compreensível.
- **Escalabilidade e Evolução**: Um código bem estruturado pode ser escalado e adaptado às novas necessidades do projeto sem grandes dificuldades.

## Princípios Fundamentais do Código Limpo

Os princípios do código limpo ajudam a alcançar esses objetivos, promovendo práticas que resultam em um código robusto e sustentável. A seguir, detalharemos esses princípios:

1. **Nomes Significativos**:
   - **Descritivos**: Os nomes devem revelar a intenção do código. Em vez de nomes como `d` ou `temp`, use `data` ou `temperature`.
   - **Consistência**: Use nomes consistentes ao longo do código para variáveis semelhantes.
   - **Pronomes e qualificadores**: Use prefixos e sufixos que ajudem a distinguir os papéis das variáveis, como `beginDate` e `endDate`.

2. **Funções Pequenas**:
   - **Tamanho**: Idealmente, uma função deve caber em uma tela sem a necessidade de rolar.
   - **Responsabilidade Única**: Cada função deve realizar uma única tarefa ou ação clara e distinta.
   - **Níveis de Abstração**: Mantenha o mesmo nível de abstração dentro de uma função. Não misture lógica de alto e baixo nível.

3. **Princípio de Responsabilidade Única (SRP)**:
   - **Mudanças Mínimas**: Cada classe deve ter uma única razão para mudar. Isso facilita a manutenção e a atualização do código.
   - **Separação de Responsabilidades**: Divida as funcionalidades em classes distintas, cada uma responsável por um aspecto específico do sistema.

4. **Evitar Comentários Desnecessários**:
   - **Autoexplicativo**: O código deve ser claro o suficiente para que comentários descritivos não sejam necessários.
   - **Comentários Relevantes**: Use comentários para explicar decisões de design, contextos especiais ou razões para uma abordagem específica.
   - **Atualização**: Certifique-se de que os comentários estejam atualizados e reflitam o código real.

5. **Formatação Consistente**:
   - **Indentação**: Use indentação consistente para melhorar a legibilidade.
   - **Espaçamento**: Use espaçamento adequado entre blocos de código, dentro de expressões e ao redor de operadores.
   - **Chaves**: Siga uma convenção clara para o uso de chaves em blocos de código.

6. **Código Simples**:
   - **Clareza e Simplicidade**: Prefira soluções claras e diretas, evitando complexidade desnecessária. Mantenha o código o mais simples possível.
   - **Refatoração**: Simplifique o código continuamente através de refatoração.

7. **Testes Automatizados**:
   - **Cobertura**: Escreva testes que cubram o comportamento esperado do código.
   - **Regressão**: Use testes para detectar e evitar regressões quando o código for alterado.
   - **Tipos de Testes**: Utilize uma variedade de testes, incluindo testes de unidade, integração e aceitação.

8. **Redução de Dependências**:
   - **Injeção de Dependência**: Use a injeção de dependência para desacoplar componentes e facilitar a testabilidade.
   - **Interfaces**: Dependa de interfaces ou abstrações, não de implementações concretas.
   - **Modularidade**: Divida o código em módulos independentes e coesos.

9. **Tratar Erros Adequadamente**:
   - **Exceções Específicas**: Lance e capture exceções específicas para lidar com erros de maneira clara.
   - **Mensagens de Erro**: Forneça mensagens de erro significativas e informativas.
   - **Limpeza de Recursos**: Assegure-se de que recursos sejam liberados adequadamente em caso de erros.

10. **Refatoração Contínua**:
    - **Pequenas Melhorias**: Refatore continuamente em pequenas etapas para melhorar a estrutura do código.
    - **Código Legado**: Aplique refatoração a código legado para torná-lo mais moderno e legível.
    - **Testes**: Garanta que o código esteja coberto por testes antes de refatorar.

11. **Princípio DRY (Don't Repeat Yourself)**:
    - **Reutilização**: Consolidar lógica repetida em funções ou métodos reutilizáveis.
    - **Manutenção**: Facilita a manutenção, pois mudanças precisam ser feitas em apenas um lugar.
    - **Consolidação**: Centralize o conhecimento e a lógica comum em um único local no código.

12. **Coesão e Acoplamento**:
    - **Alta Coesão**: Mantenha os módulos focados em uma única tarefa ou responsabilidade, aumentando a coesão.
    - **Baixo Acoplamento**: Minimize as dependências entre módulos para reduzir o impacto de mudanças.

## Conclusão

Adotar e seguir esses princípios não só ajuda a criar um código funcional, mas também assegura que ele seja sustentável e eficiente a longo prazo. Implementar os princípios do código limpo é um investimento que resulta em um software mais robusto, de fácil manutenção e evolução contínua.
