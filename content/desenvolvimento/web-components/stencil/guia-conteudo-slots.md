---
title: Verificação de Conteúdo em Slots
description: Verificação de Conteúdo em Slots Padrão e Nomeados no Stencil
---

Esse guia contém o passo a passo para verificar se há conteúdo presente em slots, tanto padrão quanto nomeados, em um componente Stencil.

## 1. Importe os decoradores necessários do Stencil core

``` typescript
import { Component, h, State, Element } from '@stencil/core';
```

## 2. Defina o seu componente

``` typescript
@Component({
  tag: 'my-component',
  shadow: true
})
```

## 3. Defina o estado e o elemento

``` typescript
@Element() el: HTMLElement;
@State() hasContent: boolean = false;
```

- `@Element()`: usado para obter uma referência ao elemento host
- `@State()`: utilizado para criar uma variável de estado reativa.

## 4. Verifique o conteúdo do slot no método `componentWillLoad`

### Para slots `default`

``` typescript
componentWillLoad() {
  this.hasContent = this.el.innerHTML.trim() !== '';
}
```

O método componentWillLoad é chamado logo antes da primeira renderização do componente. Neste ponto, `this.el.innerHTML.trim() !== ''` verifica se há qualquer conteúdo, além de espaços em branco, dentro do slot padrão.

### Para slots nomeados

``` typescript
private hasSlotContent(slotName: string): boolean {
  return !!this.el.querySelector(`[slot="${slotName}"]`);
}
```

Para verificar se um slot nomeado contém conteúdo, crie um método (exemplo: hasSlotContent), que busca elementos que utilizam o slot especificado. Ele retorna true se encontrar conteúdo, ou false caso contrário.

## 4. Renderize o conteúdo do slot ou uma mensagem padrão com base no estado

Exemplo para slot `default`:

``` typescript
render() {
  return (
    <div>
      {this.hasContent ? 
        <slot /> : 
        <span>Nenhum conteúdo fornecido no slot.</span>
      }
    </div>
  );
}
```

O método render usa o estado hasContent para renderizar o conteúdo do slot ou uma mensagem padrão.

Exemplo para slot nomeado:

``` typescript
render() {
  return (
    <div>
      {this.hasSlotContent('action') ? 
        <slot /> : 
        <span>Nenhum conteúdo fornecido no slot.</span>
      }
    </div>
  );
}
```

### Nota

Essa abordagem assume que o conteúdo do slot é estático e não é carregado ou alterado dinamicamente após a inicialização do componente. Se o conteúdo do slot puder mudar dinamicamente, você pode precisar usar um método de ciclo de vida diferente ou um MutationObserver para monitorar as mudanças no conteúdo do slot.

Para mais informações sobre métodos de ciclo de vida de componentes Stencil, consulte a [Documentação do Stencil](https://stenciljs.com/docs/component-lifecycle).
