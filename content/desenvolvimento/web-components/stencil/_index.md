---
title: 'Versão 2.x (Stencil)'
chapter: true
weight: 10
---

Essa seção é dedicada as documentações do projeto **@govbr-ds/webcomponents**, na versão **V2**, que está sendo desenvolvido utilizando a biblioteca [**Stencil**](https://stenciljs.com/). O Stencil é um compilador que gera Web Components nativos e eficientes. Ele é uma ferramenta para construir componentes web reutilizáveis e de alto desempenho usando JavaScript moderno.
