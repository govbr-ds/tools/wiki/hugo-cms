---
title: Testes com Playwright
description: Uso do Playwright para testes E2E e de acessibilidade
---

Este documento é resultado de uma POC (Proof of Concept) sobre o uso do **Playwright** na automação de testes de aplicações web. Foram abordados testes unitários, de ponta a ponta (E2E) e de acessibilidade, utilizando exemplos práticos para demonstrar a eficácia da ferramenta.

O estudo permitiu identificar as principais funcionalidades e benefícios do Playwright, além de destacar boas práticas para a implementação de testes automatizados.

## Configuração Inicial

### 1. Instalação do Node.js

Para começar a utilizar o Playwright, é necessário ter o **Node.js** instalado no seu sistema.

1. **Passo:**
   - Acesse [nodejs.org](https://nodejs.org/) e faça o download da versão LTS recomendada.
   - Siga as instruções de instalação para o seu sistema operacional.

### 2. Inicializando um Novo Projeto Node.js

Após instalar o Node.js, é necessário inicializar um novo projeto.

1. **Passo:**
   - Abra o terminal e navegue até o diretório onde deseja criar seu projeto.
   - Execute o comando:

     ```bash
     npm init -y
     ```

   - Isso criará um arquivo `package.json` com as configurações padrão.

### 3. Instalando o Playwright

Com o projeto inicializado, prossiga para a instalação do Playwright.

1. **Passo:**
   - No terminal, ainda dentro do diretório do projeto, execute:

     ```bash
     npm install @playwright/test
     ```

   - Isso instalará o Playwright como uma dependência do seu projeto.

### 4. Configurando o Playwright

Após a instalação, configure o Playwright para baixar os navegadores necessários e preparar o ambiente de testes.

1. **Passo:**
   - Execute o comando:

     ```bash
     npx playwright install
     ```

   - O Playwright baixará os navegadores (Chromium, Firefox, WebKit) e criará os seguintes arquivos e diretórios no seu projeto:
     - `playwright.config.ts`: Arquivo de configuração onde você pode ajustar as configurações do Playwright, como os navegadores a serem utilizados nos testes.
     - `package.json` e `package-lock.json`: Arquivos de configuração do projeto que gerenciam as dependências.
     - `tests/`: Pasta contendo um exemplo básico de teste para ajudar você a começar.
     - `tests-examples/`: Pasta com exemplos mais detalhados, incluindo um teste de uma aplicação de lista de tarefas (todo app).

## Testes Unitários com Playwright

Os testes unitários são essenciais para garantir que as funções individuais da sua aplicação funcionem conforme o esperado.

### 1. Escrevendo um Teste Unitário Simples

1. **Passo:**
   - Crie um arquivo de teste, por exemplo `tests/unit.spec.js`, com o seguinte conteúdo:

     ```javascript
     const { test, expect } = require('@playwright/test');

     test('verifica se a função soma funciona corretamente', () => {
       function soma(a, b) {
         return a + b;
       }

       // Teste da função soma
       expect(soma(2, 3)).toBe(5);
       expect(soma(-1, 1)).toBe(0);
     });
     ```

### 2. Executando os Testes Unitários

1. **Passo:**
   - Para rodar o teste, execute no terminal:

     ```bash
     npx playwright test
     ```

   - O Playwright executará o teste e fornecerá um relatório no terminal.

## Testes de Ponta a Ponta (E2E) com Playwright

Os testes E2E verificam o fluxo completo da aplicação, garantindo que todas as partes interajam corretamente.

### 1. Escrevendo um Teste E2E Simples

1. **Passo:**
   - Crie um arquivo de teste, por exemplo `tests/e2e.spec.js`, com o seguinte conteúdo:

     ```javascript
     const { test, expect } = require('@playwright/test');

     test('verifica o fluxo de login', async ({ page }) => {
       // Acessa a página de login
       await page.goto('https://example.com/login');

       // Preenche o campo de usuário
       await page.fill('input[name="username"]', 'usuarioTeste');

       // Preenche o campo de senha
       await page.fill('input[name="password"]', 'senhaTeste');

       // Clica no botão de login
       await page.click('button[type="submit"]');

       // Verifica se o usuário foi redirecionado para a página inicial
       await expect(page).toHaveURL('https://example.com/home');

       // Verifica se o texto "Bem-vindo" está visível
       await expect(page.locator('text=Bem-vindo')).toBeVisible();
     });
     ```

### 2. Executando os Testes E2E

1. **Passo:**
   - Para rodar o teste, execute no terminal:

     ```bash
     npx playwright test
     ```

   - O Playwright executará o teste em cada um dos navegadores instalados e fornecerá um relatório no terminal.

## Testes de Acessibilidade com Playwright e Axe

Garantir a acessibilidade da sua aplicação é fundamental para atender a todos os usuários.

### 1. Instalando a Biblioteca Axe

1. **Passo:**
   - Para realizar testes de acessibilidade, instale a biblioteca Axe junto com o Playwright:

     ```bash
     npm install axe-core @axe-core/playwright
     ```

### 2. Escrevendo um Teste de Acessibilidade

1. **Passo:**
   - Crie um arquivo de teste, por exemplo `tests/accessibility.spec.js`, com o seguinte conteúdo:

     ```javascript
     const { test, expect } = require('@playwright/test');
     const AxeBuilder = require('@axe-core/playwright');

     test('deve não ter violações detectáveis automaticamente de WCAG A ou AA', async ({ page }) => {
       await page.goto('https://example.com/avatar');

       const accessibilityScanResults = await new AxeBuilder({ page })
         .withTags(['wcag2a', 'wcag2aa', 'wcag21a', 'wcag21aa'])
         .disableRules(['color-contrast'])
         .analyze();

       expect(accessibilityScanResults.violations).toEqual([]);
     });
     ```

### 3. Executando os Testes de Acessibilidade

1. **Passo:**
   - Para rodar o teste de acessibilidade, execute no terminal:

     ```bash
     npx playwright test
     ```

   - O Playwright executará o teste e fornecerá um relatório no terminal, indicando se foram encontradas violações de acessibilidade.

## Gerando Relatórios de Testes

Gerar relatórios detalhados facilita a análise e a comunicação dos resultados dos testes.

1. **Passo:**
   - Para gerar um relatório em HTML, execute:

     ```bash
     npx playwright show-report
     ```

   - Isso abrirá um relatório detalhado dos testes no navegador, onde você pode visualizar os resultados e screenshots de cada etapa dos testes.

## Conclusão

Considerando os tópicos abordados neste guia, pode-se concluir que o **Playwright** é uma ferramenta poderosa e versátil para a automação de testes em aplicações web. Ele suporta diferentes tipos de testes, como unitários, E2E e de acessibilidade, oferecendo flexibilidade e eficiência no processo de desenvolvimento.

A configuração inicial é simples e permite a rápida implementação de testes. Além disso, a integração com bibliotecas como o **Axe** potencializa a capacidade de identificar e corrigir problemas de acessibilidade, garantindo uma experiência mais inclusiva para todos os usuários.

Ao adotar o Playwright em seu fluxo de trabalho, você estará investindo em qualidade e robustez para sua aplicação, facilitando a detecção de bugs e a manutenção contínua do sistema.
