---
title: Padrão de Nomenclatura
description: Especifica um padrão de nomenclatura para os webcomponents
---

O padrão de nomenclatura para os Web Components tem como base os [princípios do clean code](/desenvolvimento/web-components/stencil/principios-clean-code), e o uso da biblioteca [Stencil.js](https://stenciljs.com/). Dessa forma é proposto um padrão que promova clareza, consistência e expressividade.

## Diretrizes para Nomenclatura de Componentes

- Use PascalCase: Nomes de componentes devem user o PascalCase, onde a primeira letra de cada palavra é maiúscula.

  - `Button`
  - `MagicButton`
  - `ShoppingCart`

- Seja Descritivo e Intencional: Escolha nomes que revelem a intenção e a funcionalidade do componente.

  - `UserProfileCard`
  - `ProductList`
  - `NotificationBanner`

- Evite Abreviações e Siglas: Use nomes completos e evite abreviações, a menos que sejam amplamente reconhecidas.

  - Em vez de `UsrProfile`, use `UserProfile`

## Diretrizes para a Nomenclatura da Tag

- Use kebab-case: Nomes de tags devem ser escritos em kebab-case, onde as palabras são separadas por hifens e todas a letras são minúsculas.

  - `br-button`
  - `br-magic-button`
  - `br-shopping-cart`

- Use Prefixos Descritivos: Utilize um prefixo comum e descritivo para todas as tags, garantindo que os componentes possam ser facilmente identificados e que os nomes não entrem em conflito com elementos HTML nativos ou outras bibliotecas.

  > O prefixo utilizado no projeto govbr-ds-wbc é: br

- Utilizar o Nome do Componente: O nome da tag deve conter o nome do componente com a adição do prefixo.

  - `br-button`
  - `br-magic-button`
  - `br-shopping-cart`

## Diretrizes para a Nomenclatura de Props e States

- Use camelCase: As propriedades devem ser escritas em camelCase, onde a primeira letra é minúscula e a primeira letra de cada palavra subsequente é maiúscula.
- Seja Descritivo e Funcional: Escolha nomes que revelem claramente o propósito da propriedade.
- Consistência e Clareza: As propriedades devem ser nomeadas de maneira que seu uso seja óbvio, sem a necessidade de explicações detalhadas.

1. Props nativas: Nos casos que a prop já exista nativamente no HTML, use o mesmo nome.

   - `disabled`
   - `submit`
   - `placeholder`

1. String: Use nomes que sejam descritivos do conteúdo ou propósito da string.

   - `title`
   - `description`
   - `label`

1. Number: Use nomes que indiquem a função ou o que o número representa.

   - `maxItems`
   - `fontSize`
   - `age`

1. Boolean: Use nomes que sejam perguntas de "*sim ou não*". Use prefixos com `is`, `has`, `can`, `should` para indicar estados ou condições booleanas.

   - `isHidden`
   - `hasError`
   - `canType`
   - `shouldShow`

1. Arrays: Use nomes que descrevam o conteúdo da lista o coleção. Use o plural.

   - `items`
   - `users`
   - `products`
   - `tags`

1. Objeto: Use nomes que indiquem a natureza ou função do objeto.

   - `user`
   - `avatar`

## Diretrizes para Nomenclatura de Métodos

- Use camelCase: Os métodos devem ser escritos em camelCase, onde a primeira letra é minúscula e a primeira letra de cada palavra subsequente é maiúscula.
- Seja Descritivo e Intencional: Escolha nomes que revelam claramente a ação o propósito do método.
- Use Prefixos Indicativos: Utilize prefixos comuns que indiquem a ação ou tipo de método.

1. Métodos para Manipulação de Eventos (Handler): Use o prefixo `on` para indicar que o método está lidando com eventos.

   - `onClick`
   - `onSubmit`
   - `onChange`
   - `onConnected`

2. Métodos de Acesso (Getters e Setters): Use prefixos como `get` ou `set` para indicar métodos que acessam ou modificam valores.

   - `getUserProfile`
   - `getConfig`
   - `setUserProfile`
   - `setConfig`

3. Métodos de Ação: Use verbos descritivos que indiquem claramente a ação realizada pelo método.

   - `fetchData`
   - `updateProfile`
   - `calculateProfit`
   - `initializeComponent`

4. Métodos Utilitários: Use nomes que descrevam a função utilitária do método.

   - `formateDate`
   - `parseJSON`
   - `generateID`
   - `validateInput`

## Diretrizes para Nomenclatura de Eventos Customizados

- Use camelCase: Os nomes dos eventos customizados devem ser escritos em camelCase.
- Seja Descritivo e Intencional: Escolha nomes que revelem claramente o propósito do event.
- Adicione o prefixo `br`. Isso ajuda a evitar conflitos de nome com outras bibliotecas ou partes do sistema.

1. Eventos Nativos do Browser: Mantenha o nome original.

- `click`
- `submit`
- `load`

1. Eventos Customizados:

   - Utilize `Did` se o evento é disparado depois de uma determinada ação.
     - `brDidOpenModal`
     - `brDidCloseModal`
   - Utilize `Will` se o evento é disparado antes de uma determinada ação.
     - `brWillOpenModal`
     - `brWillCloseModal`
