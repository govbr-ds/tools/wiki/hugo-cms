---
title: Danger Bot
description: Explicação sobre nossa configuração do Danger-Bot
---

No projeto de Web Components estamos usando o `danger-bot` para ajudar nas revisões de Merge Requests.

Nessa documentação não vamos explicar o que é nem como configurar o `danger-bot`, para isso consulte as documentações:

- [Danger Bot](https://danger.systems)
- [Gitlab Dangerfiles](https://gitlab.com/gitlab-org/ruby/gems/gitlab-dangerfiles)

## Configuração do projeto

Como as configurações são dinâmicas e é provável que evoluam constantemente, vamos atualizar essa documentação com os pontos mais importantes, mas não vamos explicar cada validação que é feita.

### Gemfile

Nesse arquivo ficam as dependências do bot. Plugins, extensões, etc.

## Run:Danger

Esse é o estágio do Gitlab CI que executa o bot durante os pipelines. Ele vai instalar as dependências e executar o bot em modo CI.

## gitlab.dismiss_out_of_range_messages

Faz com que o `danger-bot` só escreva mensagens relacionadas a conteúdos modificados no MR.

## Gitlab Helper

Usamos o `gitlab-dangerfiles` para importar utilitários para facilitar o uso da API do gitlab.

Colocamos o código a seguir no início do arquivo para usá-los:

```ruby
require 'gitlab-dangerfiles'

Gitlab::Dangerfiles.for_project(self) do |dangerfiles|
  dangerfiles.import_plugins
end
```

[Exemplo do Gitlab Helper](https://gitlab.com/gitlab-org/ruby/gems/gitlab-dangerfiles/-/blob/master/lib/danger/plugins/internal/helper.rb).

## Gitlab

Também fazemos uso da API do gitlab. Para isso instalamos a gem `danger-gitlab` que faz uso do `gitlab client` e com ela podemos fazer uso da API do gitlab.

Por exemplo o código `gitlab.api.merge_request(helper.mr_target_project_id, helper.mr_iid).to_hash` nos retorna os dados do MR:

```json
{
  "id": "number",
  "iid": "number",
  "project_id": "number",
  "title": "string",
  "description": "string",
  "state": "string",
  "created_at": "string (date)",
  "updated_at": "string (date)",
  "merged_by": "null",
  "merge_user": "null",
  "merged_at": "null",
  "closed_by": "null",
  "closed_at": "null",
  "target_branch": "string",
  "source_branch": "string",
  "user_notes_count": "number",
  "upvotes": "number",
  "downvotes": "number",
  "author": {
    "id": "number",
    "username": "string",
    "name": "string",
    "state": "string",
    "locked": "boolean",
    "avatar_url": "string (URL)",
    "web_url": "string (URL)"
  },
  "assignees": "array",
  "assignee": "null",
  "reviewers": "array",
  "source_project_id": "number",
  "target_project_id": "number",
  "labels": "array",
  "draft": "boolean",
  "imported": "boolean",
  "imported_from": "string",
  "work_in_progress": "boolean",
  "milestone": "null",
  "merge_when_pipeline_succeeds": "boolean",
  "merge_status": "string",
  "detailed_merge_status": "string",
  "sha": "string",
  "merge_commit_sha": "null",
  "squash_commit_sha": "null",
  "discussion_locked": "null",
  "should_remove_source_branch": "null",
  "force_remove_source_branch": "boolean",
  "prepared_at": "string (date)",
  "reference": "string",
  "references": {
    "short": "string",
    "relative": "string",
    "full": "string"
  },
  "web_url": "string (URL)",
  "time_stats": {
    "time_estimate": "number",
    "total_time_spent": "number",
    "human_time_estimate": "null",
    "human_total_time_spent": "null"
  },
  "squash": "boolean",
  "squash_on_merge": "boolean",
  "task_completion_status": {
    "count": "number",
    "completed_count": "number"
  },
  "has_conflicts": "boolean",
  "blocking_discussions_resolved": "boolean",
  "approvals_before_merge": "null",
  "subscribed": "boolean",
  "changes_count": "string",
  "latest_build_started_at": "string (date)",
  "latest_build_finished_at": "null",
  "first_deployed_to_production_at": "null",
  "pipeline": {
    "id": "number",
    "iid": "number",
    "project_id": "number",
    "sha": "string",
    "ref": "string",
    "status": "string",
    "source": "string",
    "created_at": "string (date)",
    "updated_at": "string (date)",
    "web_url": "string (URL)"
  },
  "head_pipeline": {
    "id": "number",
    "iid": "number",
    "project_id": "number",
    "sha": "string",
    "ref": "string",
    "status": "string",
    "source": "string",
    "created_at": "string (date)",
    "updated_at": "string (date)",
    "web_url": "string (URL)",
    "before_sha": "string",
    "tag": "boolean",
    "yaml_errors": "null",
    "user": {
      "id": "number",
      "username": "string",
      "name": "string",
      "state": "string",
      "locked": "boolean",
      "avatar_url": "string (URL)",
      "web_url": "string (URL)"
    },
    "started_at": "string (date)",
    "finished_at": "null",
    "committed_at": "null",
    "duration": "null",
    "queued_duration": "number",
    "coverage": "string",
    "detailed_status": {
      "icon": "string",
      "text": "string",
      "label": "string",
      "group": "string",
      "tooltip": "string",
      "has_details": "boolean",
      "details_path": "string",
      "illustration": "null",
      "favicon": "string (URL)"
    }
  },
  "diff_refs": {
    "base_sha": "string",
    "head_sha": "string",
    "start_sha": "string"
  },
  "merge_error": "null",
  "first_contribution": "boolean",
  "user": {
    "can_merge": "boolean"
  }
}
```

Podemos pegar os dados do autor do post fazendo assim:

```ruby
merge_request = gitlab.api.merge_request(helper.mr_target_project_id, helper.mr_iid).to_hash
author = merge_request['author']
```

Consulte as documentações abaixo para mais informações sobre a API do gitlab e o `danger-gitlab`:

- [Gitlab API](https://docs.gitlab.com/ee/api/merge_requests.html#get-single-mr)
- [GItlab Client](https://www.rubydoc.info/gems/gitlab/Gitlab/Client)
- [Danger-gitlab](https://rubygems.org/gems/danger-gitlab)
