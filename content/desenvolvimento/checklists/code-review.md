---
title: Code Review
description: Requisitos usados durante o code review
keywords: checklist, revisão
---

O processo de code review é uma atividade que tem o objetivo garantir a **qualidade** do projeto e que pode ser realizada por 1 ou mais membros do projeto.

O revisor não pode:

- Ser o autor do MR
- Ter enviado pelo menos um commit do MR

## Como fazer?

Ao fazer um code review, avalie:

1. Se o MR atende ao {{< newtablink href="../../../git-gitlab/guias/mr-criacao" title="guia de criação de MRs" >}}
1. Se o conteúdo segue todos os requisitos do {{< newtablink href="../implementacao" title="checklist de implementação" >}}

{{% notice tip %}}
Além dos requisitos acima, observe também se o MR atende aos requisitos descritos na página de {{< newtablink href="../../../git-gitlab/guias/mr-revisao" title="revisão por Merge Request" >}}
{{% /notice %}}
