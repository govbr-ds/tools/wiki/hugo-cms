---
title: WSL2
description: Como instalar e configurar o WSl2
date: 08/10/2020
keywords: windows, wsl, máquina, wsl2, script, linux
---

O Windows provê uma ferramente para desenvolvedores usarem o GNU/Linux dentro do próprio windows sem causar uma sobrecarga nos recursos da máquina como uma máquina virtual tradicional faria.

Vocês podem ler mais sobre o WSL (Subsistema do Windows para Linux) no [site oficial](https://docs.microsoft.com/pt-br/windows/wsl/about).

Descrição retirada do [site oficial](https://docs.microsoft.com/pt-br/windows/wsl/about):
O Subsistema do Windows para Linux permite que os desenvolvedores executem um ambiente GNU/Linux, incluindo a maioria das ferramentas de linha de comando, utilitários e aplicativos, diretamente no Windows, sem modificações e sem a sobrecarga de uma máquina virtual tradicional ou instalação dualboot.

O WSL 2 é uma nova versão da arquitetura do Subsistema do Windows para Linux que capacita o Subsistema do Windows para Linux a executar binários ELF64 Linux no Windows. As metas principais dele são aumentar o desempenho do sistema de arquivos e adicionar compatibilidade completa com a chamada do sistema.

## instalação

Para instalar, siga os passos da [documentação oficial](https://docs.microsoft.com/pt-br/windows/wsl/install-win10). Note que a documentação tem um passo dedicado ao WSL2, mas você pode manter e usar os dois.

## Configuração

O WSL permite fazer configurações específicas para cada distribuição do linux, mas para fazer uma configuração global:

1. Crie um arquivo .wslconfig na sua pasta de usuário do Windows (ex: c:/Users/rafael/)
1. Coloque o conteúdo abaixo nesse arquivo e configure do jeito que achar melhor:

```editorconfig
[wsl2]
memory=              # How much memory to assign to the WSL2 VM.
processors=          # How many processors to assign to the WSL2 VM.
swap=                # How much swap space to add to the WSL2 VM. 0 for no swap file.
swapFile=            # An absolute Windows path to the swap vhd.
localhostForwarding= # Boolean specifying if ports bound to wildcard or localhost in the WSL2 VM should be connectable from the host via localhost:port (default true).

#  entries must be absolute Windows paths with escaped backslashes, for example C:\\Users\\Ben\\kernel
#  entries must be size followed by unit, for example 8GB or 512MB

```

## Arquivos

Os arquivos da distribuição podem ser acessados pelo Windows Explorer usando o caminho: **\\wsl\$**

## Acessar servidor pela LAN

O WSL2 faz uma virtualização completa da rede da distribuição e isso pode deixar mais complicado para acessar algum servidor rodando. Para facilitar o acesso, no [github do WSL](https://github.com/microsoft/WSL) algumas pessoas contribuíram e criaram [várias maneiras de contornar o problema](https://github.com/microsoft/WSL/issues/4150).

A que eu achei mais fácil é usando um script que é executado toda vez que o usuário entra no Windows. Vou descrever os passos necessários para usar o script, mas na [issue do github](https://github.com/microsoft/WSL/issues/4150) é possível que outras maneiras apareçam e sejam aperfeiçoadas.

Esse script basicamente cria as regras no firewall para permitir o redirecionamento. É necessário executar o script toda vez que o Windows reinicia, pois o WSL no modo padrão altera o IP toda vez que a máquina é reiniciada.

1. Crie o script a seguir na sua pasta de usuário do Windows (ex: c:/Users/rafael) e salve com a extensão "**.ps1**":

```powershell
$wsl_ip = bash.exe -c "ip addr | grep -Ee 'inet.*eth0'"
$found = $wsl_ip -match '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}';

if( $found ){
   $wsl_ip = $matches[0];
} else{
   echo "The Script Exited, the ip address of WSL 2 cannot be found";
   exit;
}

#[Ports]
#All the ports you want to forward separated by coma
$ports=@(80,443,4200,9000,10000,3000,5000);

   #[Static ip]
   #You can change the addr to your ip config to listen to a specific address
   $addr='0.0.0.0';
   $ports_a = $ports -join ",";

   #Remove Firewall Exception Rules
   iex "Remove-NetFireWallRule -DisplayName 'WSL 2 Firewall Unlock' ";

   #adding Exception Rules for inbound and outbound Rules
   iex "New-NetFireWallRule -DisplayName 'WSL 2 Firewall Unlock' -Direction Outbound -LocalPort $ports_a -Action Allow -Protocol TCP";
   iex "New-NetFireWallRule -DisplayName 'WSL 2 Firewall Unlock' -Direction Inbound -LocalPort $ports_a -Action Allow -Protocol TCP";

   for( $i = 0; $i -lt $ports.length; $i++ ){
   $port = $ports[$i];
   iex "netsh interface portproxy delete v4tov4 listenport=$port listenaddress=$addr";
   iex "netsh interface portproxy add v4tov4 listenport=$port listenaddress=$addr connectport=$port connectaddress=$wsl_ip";
   }
```

1. A lista das portas que devem ser redirecionadas fica na variável \$ports.
1. Abra o Agendador de Tarefas do Windows

![Agendador de Tarefas](../imagens/agendador_de_tarefas.png)

1. Selecione a opção "Criar Tarefa Básica"

![Criar tarefa básica](../imagens/criar_tarefa_basica.png)

1. Coloque um nome para a tarefa

![Nome da tarefa](../imagens/nome.png)

1. Selecione a opção "Ao fazer logon"

![Ao fazer login](../imagens/logon.png)

1. Selecione a opção "Iniciar um programa"

![Iniciar](../imagens/iniciar.png)

1. No programa coloque "Powershell.exe" e nos argumentos coloque o endereço entre aspas para o script criado

![Powershell](../imagens/powershell.png)

1. Revise as opções preenchidas e marque a opção de "Abrir a caixa de Propriedades da tarefa depois de clicar em Concluir"

![Resumo](../imagens/resumo.png)

1. Na tela que aparece marque a opção "Executar com privilégios mais altos"

![Administrador](../imagens/adm.png)

1. Na aba disparadores clique na opção "Editar..." e coloque um atraso de **10 segundos**

![Atraso](../imagens/atraso.png)

1. Caso esteja usando um laptop desmarque a opção "Iniciar a tarefa somente se o computador estiver ligado na rede elétrica" na aba Condições

![Condições](../imagens/condicoes.png)

## Roteador

Um problema que pode acontecer é do roteador bloquear o redirecionamento de portas. Nesse caso existem duas opções:

1. Liberar as portas individuais que precisa para a máquina
1. Colocar a máquina na DMZ da rede

Caso resolva colocar a máquina na DMZ da rede, saiba que problemas de segurança podem ocorrer. Pesquise os potências problemas e perigos antes de decidir seguir por esse caminho.
