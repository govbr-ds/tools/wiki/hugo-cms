---
title: 'Criação de release de CORE '
description: Guia de criação de release de CORE
---

## Processo de criação de Release do core

1. Verificar se tem problema de responsividade, funcionalidade e integração
1. criar branch de release com a versão que vai ser gerada como release/v1.0.0
1. Rodar teste de funcionalidade em [teste frontend](https://gitlab.com/govbr-ds/dev/govbr-ds-dev-frontend-testing)
1. Rodar o comando 'npx standard-version --dry-run' e verifique o changelog no terminal
1. Verificar se teve alterações na release de design
1. Alterar o config.json nas referencia de download com essa versão gerada, os links ficam disponível depois desse processo
1. Alterar a pagina [novidades](https://www.gov.br/ds/o-que-ha-de-novo) no arquivo docs/o-que-ha-de-novo/o-que-ha-de-novo.html com as mudanças
1. Rodar o comando 'npm run build'
1. Verificar se os arquivos core.min.js e core-init.min.js estão minificados e sem problema
1. Rodar o comando 'npx standard-version'
1. Mandar a branch gerada para o git
1. No git criar uma tag ,a partir da branch que foi enviada,com o nome da versão gerada e colocar em **release notes** o changelog gerado
1. Fazer publicação no npmjs usando o comando 'npm publish' conforme documentação em [publicação npm](/desenvolvimento/devops/publicacao-npm/)
1. Fazer o release do site
