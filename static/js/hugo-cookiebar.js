class HugoCookiebar {
  static _instanceCache

  static instance() {
    if (!this._instanceCache) {
      this._instanceCache = new this()
    }

    return this._instanceCache
  }

  constructor() {
    this.data = null
    this.outputJson = null
    this.setup()
  }

  addContent() {
    document.querySelector('br-cookiebar').setAttribute('content', JSON.stringify(this.data))
  }

  closeCookiebar() {
    document.getElementById('cookiebar').removeAttribute('open')
  }

  createCookie(name, value, days) {
    let expires = ''
    if (days) {
      let date = new Date()
      date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000)
      expires = '; expires=' + date.toUTCString()
    }
    document.cookie = name + '=' + value + expires + '; path=/'
  }

  createGoogleAnalyticsCookie(trackingID) {
    // Cria o elemento script
    let gaScript = document.createElement('script')
    gaScript.setAttribute('name', 'ga-script')

    // Define o código do Google Analytics
    gaScript.innerHTML = `
                (function (w, d, s, l, i) {
                w[l] = w[l] || []; w[l].push({
                    'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
                }); var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
                })(window, document, 'script', 'dataLayer', 'GTM-PBK2JXM');
            `

    // Adiciona o elemento script ao final do elemento head
    document.head.appendChild(gaScript)
  }

  eraseCookie(name) {
    this.createCookie(name, '', -1)
  }

  // Ativa quando clicar em aceitar/rejeitar
  handleReport(event) {
    this.outputJson = event.detail[0]
    this.eraseCookie('lgpd-cookie-v2')
    this.createCookie('lgpd-cookie-v2', this.outputJson, 365)
    this.setCookieC2(JSON.parse(this.outputJson))
    this.closeCookiebar()
    this.hideCookiebar()
  }

  hideCookiebar() {
    document.getElementById('cookiebar').setAttribute('class', 'd-none')
  }

  openCookiebar() {
    document.getElementById('cookiebar').setAttribute('open', 'true')
  }

  readCookie(name) {
    let nameEQ = name + '='
    let ca = document.cookie.split(';')

    for (const element of ca) {
      let c = element
      while (c.charAt(0) == ' ') c = c.substring(1, c.length)
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length)
    }
    return null
  }

  removeGoogleAnalyticsCookie() {
    document.querySelector('script[name="ga-script"]')?.remove()
  }

  resetCookies() {
    if (document.querySelector('br-cookiebar').classList.contains('d-none')) {
      let categories = JSON.parse(this.readCookie('lgpd-cookie-v2'))
      for (let category of categories) {
        if (category.c2) {
          const { k5: _ga } = category.c2
          this.data['pt-br'].categories[1].enable = Boolean(_ga)
        }
      }
      this.showCookiebar()
      this.openCookiebar()
    }
  }

  setCookieC2(categories) {
    for (let category of categories) {
      if (category.c2) {
        const { k5: _ga } = category.c2
        if (_ga == '1') {
          this.createGoogleAnalyticsCookie()
        } else {
          this.removeGoogleAnalyticsCookie()
          this.eraseCookie('_ga')
          this.eraseCookie('_ga_VM3VC6PZVH')
          window.addEventListener('unload', () => this.eraseCookie('_ga_VM3VC6PZVH'))
        }
      }
    }
  }

  setData(data) {
    this.data = data
    this.addContent()
  }

  setReport() {
    document.querySelector('br-cookiebar').addEventListener('report', this.handleReport.bind(this))
  }

  setup() {
    this.setReport()
    window.addEventListener('load', this.setCookieC2.bind(this, JSON.parse(this.readCookie('lgpd-cookie-v2') || '[]')))
  }

  showCookiebar() {
    document.getElementById('cookiebar').removeAttribute('class', 'd-none')
  }
}
const hugoCookiebar = HugoCookiebar.instance()
if (!hugoCookiebar.readCookie('lgpd-cookie-v2')) {
  hugoCookiebar.showCookiebar()
}
hugoCookiebar.setData({
  'pt-br': {
    labels: {
      close: 'Fechar o cookiebar',
      update: 'Última atualização',
      categories: 'Classes de cookies',
      enableAll: 'Selecionar tudo',
      disableAll: 'Desselecionar tudo',
      enableCategory: 'Selecionar toda classe',
      disableCategory: 'Desselecionar toda classe',
      enableCookie: 'Selecionar',
      disableCookie: 'Desselecionar',
      awaysEnabled: 'Sempre ativo',
      cookie: 'Cookies',
      expiry: 'Vencimento',
      domain: 'Domínio',
      company: 'Empresa',
      purpose: 'Finalidade',
      description: 'Descrição',
      acceptButton: 'Aceitar',
      rejectButton: 'Rejeitar cookies',
      optInButton: 'Ver Políticas de Cookies',
      optOutButton: 'Definir Cookies',
      notes: 'Aviso sobre cookies',
    },
    intro: {
      observation:
        'Usamos cookies para armazenar informações sobre como você usa o nosso site e as páginas que visita. Tudo para tornar sua experiência a mais agradável possível. Para entender os tipos de cookies que utilizamos e customizá-los, clique em &apos;Definir Cookies&apos;. Ao clicar em &apos;Aceitar&apos;, você consente com a utilização de cookies.',
      title: 'Respeitamos a sua privacidade',
      update: '21/06/2023',
      foreword:
        'O Serpro preza pela sua privacidade e garante a proteção de seus dados pessoais! Destacamos que utilizamos cookies para melhorar sua experiência no site, conforme a nossa Política de Privacidade. Fique à vontade para customizar os cookies que não são estritamente necessários.',
    },
    all: {
      optIn: false,
      enable: true,
      alert: 'Ao desabilitar os cookies, alguns serviços podem não funcionar corretamente.',
    },
    categories: [
      {
        id: 'c1',
        name: 'Estritamente Necessários',
        optIn: true,
        description:
          'Esses cookies são indispensáveis para o funcionamento do site e não podem ser desligados em nossos sistemas por usarem recursos essenciais. Normalmente, eles são definidos apenas em resposta a ações feitas por você que equivalem a uma solicitação de serviços, como definir suas preferências de privacidade, fazer login ou preencher formulários. Você pode configurar seu navegador para bloquear ou alertá-lo sobre esses cookies, mas algumas partes do site não funcionarão. Esses cookies não armazenam qualquer informação pessoalmente identificável.',
      },
      {
        id: 'c2',
        name: 'Desempenho',
        enable: true,
        alert: 'Ao desligar, não irá funcionar o Google Analytics, ou seja, as estatísticas de acesso ao portal.',
        description:
          'Cookies de desempenho são cookies usados especificamente para coletar dados sobre como os visitantes usam o site, incluindo as páginas visitadas com mais frequência e as mensagens de erro recebidas durante a navegação. Esses cookies monitoram apenas o desempenho do site, à medida que o usuário interage com ele, e não coletam informações identificáveis sobre os visitantes. Isso significa que todos os dados coletados são anônimos e usados apenas para melhorar a funcionalidade do site. Se você não permitir esses cookies, sua navegação não fará parte da medição para melhoria de desempenho.',
      },
    ],
    cookies: [
      {
        id: 'k1',
        categoryId: 'c1',
        name: 'lgpd-cookie-v2',
        optIn: true,
        expiry: '365 dias',
        domain: 'serpro.gov.br',
        company: 'Serpro',
        purpose: 'Funcional / Preferências',
        description: 'Cookie da Barra LGPD v2',
      },
      {
        id: 'k2',
        categoryId: 'c1',
        name: 'gitlab-pages',
        optIn: true,
        expiry: '10 minutos',
        domain: 'govbr-ds.gitlab.io',
        company: 'Gitlab',
        purpose: 'Funcional / Preferências',
        description: 'Cookie do Gitlab Pages',
      },
      {
        id: 'k3',
        categoryId: 'c1',
        name: '_gitlab_session',
        optIn: true,
        expiry: 'Sessão',
        domain: 'gitlab.com',
        company: 'Gitlab',
        purpose: 'Funcional / Preferências',
        description: 'Cookie de Sessão do Gitlab Pages',
      },
      {
        id: 'k4',
        categoryId: 'c1',
        name: '_cfuvid',
        optIn: true,
        expiry: 'Sessão',
        domain: '.gitlab.com',
        company: 'Gitlab',
        purpose: 'Funcional / Preferências',
        description: 'Cookie do Gitlab Pages',
      },
      {
        id: 'k5',
        categoryId: 'c2',
        name: '_ga',
        optIn: true,
        expiry: '2 anos',
        domain: 'serpro.gov.br',
        company: 'Google',
        purpose: 'Estatística',
        description:
          'Usado para distinguir usuários por Google Analytics (cookies analíticos de terceiros: google analytics para fins estatísticos). Tipo de dados processados: endereços IP. Para maximizar a privacidade dos visitantes, utilizamos o recurso &apos;mascaramento de IP&apos;, que impõe ao sistema o escurecimento dos últimos 8 dígitos do endereço IP do visitante antes que qualquer tipo de processamento seja realizado. Objetivo analisado: recolher informação, de forma agregada, sobre o número de utilizadores e a forma como visitam o nosso site, para tratamento de estatísticas.',
        alert: 'Se você não permitir esses cookies, esses serviços podem não funcionar corretamente.',
      },
      {
        id: 'k6',
        categoryId: 'c2',
        name: '_ga_VM3VC6PZVH',
        optIn: true,
        expiry: '1 dia',
        domain: 'serpro.gov.br',
        company: 'Google',
        purpose: 'Estatística',
        description: 'Usado para distinguir usuários por Google Analytics',
        alert: 'Se você não permitir esses cookies, esses serviços podem não funcionar corretamente.',
      },
    ],
    notes: [
      {
        question: 'O que são cookies?',
        answer:
          'Os cookies são pequenos arquivos criados por sites visitados e que são salvos no computador do usuário, por meio do navegador (browser).',
      },
      {
        question: 'Qual a durabilidade?',
        answer:
          'Todos os cookies armazenados têm uma data de vencimento e são eliminados depois dela. Geralmente, esta data é bem extensa e não possui um prazo padrão. A eliminação antes do vencimento pode ser feita manualmente pelo usuário. O armazenamento e a remoção, no entanto, são diferentes para cada navegador. Neste site, os cookies ficam registrados após a ação de clicar no botão &apos;Aceitar&apos;. Você pode escolher customizá-los a qualquer momento no botão &apos;Redefinir Cookies&apos;.',
      },
      {
        question: 'Qual a proveniência?',
        answer:
          'Os cookies foram lançados na internet, em 1994, no navegador Netscape. Embora não armazenem dados de contas bancárias, por exemplo, eles podem guardar informações particulares como endereço pessoal, login e senhas de e-mail. Os cookies não estão apenas nos computadores, já que são armazenados em todos os dispositivos com um navegador, como smartphones, tablets etc.',
      },
      {
        question: 'Qual a finalidade?',
        answer:
          'Os cookies são utilizados pelos sites principalmente para identificar e armazenar informações no navegador (browser) sobre os visitantes. Servem também para personalizar a página de acordo com o perfil, registrar dados estatísticos e de acompanhamento ou melhorar a performance da aplicação, facilitando o transporte de dados entre as páginas de um mesmo site.',
      },
    ],
    links: [
      {
        name: 'Declaração de Conformidade com os Princípios de Proteção de Dados',
        url: 'https://www.serpro.gov.br/privacidade-protecao-dados',
      },
    ],
  },
})

function resetCookies() {
  const hugoCookiebar = HugoCookiebar.instance()
  hugoCookiebar.resetCookies()
}
